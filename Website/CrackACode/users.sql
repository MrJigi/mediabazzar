-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2020 at 12:48 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `users`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(80) NOT NULL,
  `name` varchar(80) NOT NULL,
  `password` varchar(80) NOT NULL,
  `family_name` varchar(80) NOT NULL,
  `date_of_birth` varchar(80) NOT NULL,
  `birthplace` varchar(80) NOT NULL,
  `nationality` varchar(80) NOT NULL,
  `languages` varchar(80) NOT NULL,
  `bsn` varchar(80) NOT NULL,
  `home_address` varchar(80) NOT NULL,
  `zip-code` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `phone` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `password`, `family_name`, `date_of_birth`, `birthplace`, `nationality`, `languages`, `bsn`, `home_address`, `zip-code`, `email`, `phone`) VALUES
(1, 'test', 'testname', '12345', 'name', '21-04-2002', 'Eindhoven', 'Dutch', 'Dutch,English', '34532', 'teststreet 12', '3456EH', 'test@gmail.com', '+31 625887451');
INSERT INTO `users` (`id`, `username`, `name`, `password`, `family_name`, `date_of_birth`, `birthplace`, `nationality`, `languages`, `bsn`, `home_address`, `zip-code`, `email`, `phone`) VALUES
(2, 'testt', 'testtname', '123456', 'names', '21-04-2005', 'Eindhoven', 'Dutch', 'English', '34532', 'teststreet 12', '3456EH', 'test@gmail.com', '+31 625887451');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
