﻿using MediaBazzar.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MediaBazzar.Views
{
    /// <summary>
    /// Interaction logic for Depo.xaml
    /// </summary>
    public partial class Depo : Window
    {
        public Depo()
        {
            InitializeComponent();
            DataContext = new StockViewModel();
        }
    }
}
