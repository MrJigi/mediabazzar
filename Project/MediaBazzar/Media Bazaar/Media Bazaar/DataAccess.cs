﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Media_Bazaar
{
    class DataAccess
    {
        string connStr = "SERVER=localhost;DATABASE=media_bazzar; UID=root; PASSWORD=";
        public List<stock> GetStock()
        {
            List<stock> returnedStock = new List<stock>();
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "SELECT * FROM stocks_s";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                conn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    stock s = new stock(Convert.ToInt32(dr[0]), dr[1].ToString(), Convert.ToInt32(dr[2]), dr[3].ToString(), Convert.ToSingle(dr[4]), dr[5].ToString(), dr[6].ToString());
                    returnedStock.Add(s);
                }
                conn.Close();
                return returnedStock;
            }
        }

        public List<stock> GetStockBasedOnType(string type)
        {
            List<stock> returnedStock = new List<stock>();
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "SELECT * FROM stocks_s WHERE type=@t";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@t", type);
                conn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    stock s = new stock(Convert.ToInt32(dr[0]), dr[1].ToString(), Convert.ToInt32(dr[2]), dr[3].ToString(), Convert.ToSingle(dr[4]), dr[5].ToString(), dr[6].ToString());
                    returnedStock.Add(s);
                }
                conn.Close();
                return returnedStock;
            }
        }

        public List<stock> GetActiveStock()
        {
            List<stock> returnedActiveStock = new List<stock>();
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string active = "active";
                string sql = "SELECT * FROM stocks_s WHERE state=@state";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@state", active);
                conn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    stock s = new stock(Convert.ToInt32(dr[0]), dr[1].ToString(), Convert.ToInt32(dr[2]), dr[3].ToString(), Convert.ToSingle(dr[4]), dr[5].ToString(), dr[6].ToString());
                    returnedActiveStock.Add(s);
                }
                conn.Close();
                return returnedActiveStock;
            }
        }

        public List<stock> GetInactiveStock()
        {
            List<stock> returnedInactiveStock = new List<stock>();
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string inactive = "inactive";
                string sql = "SELECT * FROM stocks_s WHERE state=@state";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@state", inactive);
                conn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    stock s = new stock(Convert.ToInt32(dr[0]), dr[1].ToString(), Convert.ToInt32(dr[2]), dr[3].ToString(), Convert.ToSingle(dr[4]), dr[5].ToString(), dr[6].ToString());
                    returnedInactiveStock.Add(s);
                }
                conn.Close();
                return returnedInactiveStock;
            }
        }

        public void RemoveStock(int sId)
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string inactive = "inactive";
                string sql = "UPDATE stocks_s SET state=@inActiveState WHERE id=@sId";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@sId", sId);
                cmd.Parameters.AddWithValue("@inActiveState", inactive);


                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void InsertStock(string name, int quantity, string description, float purchasev, string state, string type)
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "INSERT INTO stocks_s (name, quantity, description, purchase_value, state, type) VALUES (@nameValue, @quantityValue, @descriptionValue, @purchaseValue, @stateValue, @type)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@nameValue", name);
                cmd.Parameters.AddWithValue("@quantityValue", quantity);
                cmd.Parameters.AddWithValue("@descriptionValue", description);
                cmd.Parameters.AddWithValue("@purchaseValue", purchasev);
                cmd.Parameters.AddWithValue("@stateValue", state);
                cmd.Parameters.AddWithValue("@type", type);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

        }

        public int CalculateQuantity(int id, int quantity)
        {
            int result = 0;
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {

                string sql1 = "SELECT quantity FROM stocks_s WHERE id=@sid";
                MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                cmd1.Parameters.AddWithValue("@sId", id);
                conn.Open();
                MySqlDataReader dr = cmd1.ExecuteReader();
                while (dr.Read())
                {
                    int Getquantity = Convert.ToInt32(dr[0]);
                    result = quantity + Getquantity;

                }
                conn.Close();
                return result;
            }
        }

        public void RefillStock(int id, int quantity)
        {

            using (MySqlConnection conn = new MySqlConnection(connStr))
            {


                string sql2 = "UPDATE stocks_s SET quantity=@qty WHERE id=@sId";
                MySqlCommand cmd2 = new MySqlCommand(sql2, conn);
                cmd2.Parameters.AddWithValue("@sId", id);
                cmd2.Parameters.AddWithValue("@qty", quantity);

                conn.Open();
                cmd2.ExecuteNonQuery();
                conn.Close();

            }
        }

        public void ModifyStock(int id, string name, int quantity, string description, float purchasev, string state, string type)
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {

                string sql = "UPDATE stocks_s SET name=@name,quantity=@qty,description=@desc,purchase_value=@pv,state=@state,type=@type WHERE id=@sId";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@sId", id);
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@qty", quantity);
                cmd.Parameters.AddWithValue("@desc", description);
                cmd.Parameters.AddWithValue("@pv", purchasev);
                cmd.Parameters.AddWithValue("@state", state);
                cmd.Parameters.AddWithValue("@type", type);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void ModifyStockRequest(int id, string name, string description, float purchasev, string state, string type)
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {

                string sql = "UPDATE stocks_s SET name=@name,description=@desc,purchase_value=@pv,state=@state,type=@t WHERE id=@sId";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@sId", id);
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@desc", description);
                cmd.Parameters.AddWithValue("@pv", purchasev);
                cmd.Parameters.AddWithValue("@state", state);
                cmd.Parameters.AddWithValue("@t", type);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public int CountTotalActiveStocks()
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "SELECT COUNT(id) from stocks_s WHERE state=@ps";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@ps", "active");
                conn.Open();
                Object result = cmd.ExecuteScalar();
                int returnedCount = Convert.ToInt32(result);
                conn.Close();
                return returnedCount;
            }
        }

        public int CountTotalInactiveStocks()
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "SELECT COUNT(id) from stocks_s WHERE state=@ps";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@ps", "inactive");
                conn.Open();
                Object result = cmd.ExecuteScalar();
                int returnedCount = Convert.ToInt32(result);
                conn.Close();
                return returnedCount;
            }
        }

        public int CountTotalStocks()
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "SELECT COUNT(id) from stocks_s";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                conn.Open();
                Object result = cmd.ExecuteScalar();
                int returnedCount = Convert.ToInt32(result);
                conn.Close();
                return returnedCount;
            }
        }

    }
}

