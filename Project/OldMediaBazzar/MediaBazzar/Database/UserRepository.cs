﻿using MediaBazzar.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBazzar.Database
{
    public class UserRepository : BaseRepository<User>
    {
        private readonly string table = "Users";
        public UserRepository(string connectionString)
            : base(connectionString)
        {
        }

        public override IEnumerable<User> GetAll()
        {
            using (var command = new MySqlCommand("SELECT * FROM " + table))
            { 
                return GetAll(command);
            }
        }
        public override User GetById(string id)
        {
            using (var command = new MySqlCommand("SELECT * FROM " + table + " WHERE username=@id"))
            {
                command.Parameters.AddWithValue("@id", id);
                return GetById(command);
            }
        }

        public override void Add(User record)
        {
            using (var command = new MySqlCommand("INSERT INTO " + table + " (username, password, position, fired, toBeFired) " +
                "VALUES(@username, @password, @position, @fired, @toBeFired)"))
            {
                Fill(command, record);
                Save(command);
            }
        }

        public override void Update(User record)
        {
            using (var command = new MySqlCommand("UPDATE " + table + " SET username=@username, " +
                "password=@password, position=@position, fired=@fired, toBeFired=@toBeFired " +
                "WHERE Id=@id"))
            {
                Fill(command, record);
                command.Parameters.AddWithValue("@id", record.Id);
                Save(command);
            }
        }

        public override void Delete(string id)
        {
            Delete(table, id);
        }
       
        public override User Populate(MySqlDataReader reader)
        {
            Position pos;
            Enum.TryParse(reader.GetString(3), out pos);
            return new User
            {
                Id = reader.GetInt32(0),
                Username = reader.GetString(1),
                PasswordHash = reader.GetString(2),
                CurrPosition = pos,
                Fired = reader.GetBoolean(4),
                ToBeFired = reader.GetBoolean(5)
            };
        }

        public override void Fill(MySqlCommand command, User record)
        {
            command.Parameters.AddWithValue("@username", record.Username);
            command.Parameters.AddWithValue("@password", record.PasswordHash);
            command.Parameters.AddWithValue("@position", record.CurrPosition.ToString("G"));
            command.Parameters.AddWithValue("@fired", record.Fired);
            command.Parameters.AddWithValue("@toBeFired", record.ToBeFired);
        }


    }
}
