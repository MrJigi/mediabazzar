﻿using MediaBazzar.Database;
using MediaBazzar.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MediaBazzar.ViewModels
{
    public class AdministrationViewModel : ViewModelBase
    {
        public UserListViewModel UserList { get; set; }
        public ICommand CreateCommand { get; }
        public ICommand EditCommand { get; }
        public ICommand DeleteCommand { get; }
        private BaseRepository<User> _userRepo;
        private User _newUser;
        public User NewUser
        {
            get => _newUser;
            set 
            { 
                _newUser = value;
                NotifyPropertyChanged("NewUser");
            }
        }
        public IEnumerable<Position> PositionEnum
        {
            get
            {
                return Enum.GetValues(typeof(Position))
                    .Cast<Position>();
            }
        }
        public AdministrationViewModel()
        {
            NewUser = new User();
            _userRepo = new UserRepository(ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
            CreateCommand = new RelayCommand(param => CreateUser());
            UserList = new UserListViewModel();
            EditCommand = new RelayCommand(param => Edit());
            DeleteCommand = new RelayCommand(param => Delete());
        }

        private void CreateUser()
        {
            UserList.Users.Add(NewUser);
            _userRepo.Add(NewUser);
            NewUser = new User();
        }

        

        private void Edit()
        {
            _userRepo.Update(UserList.SelectedUser);
        }

        private void Delete()
        {
            _userRepo.Delete(UserList.SelectedUser.Id.ToString());
            UserList.Users.Remove(UserList.SelectedUser);
        }
    }
}
