﻿using MediaBazzar.Database;
using MediaBazzar.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MediaBazzar.ViewModels
{
    public class UserListViewModel : ViewModelBase
    {
        public ICommand RefreshCommand { get; }
        private BaseRepository<User> _userRepo;
        private ObservableCollection<User> _users;
        public ObservableCollection<User> Users
        {
            get => _users;
            set
            {
                _users = value;
                NotifyPropertyChanged("Users");
            }
        }
        private User _selectedUser;
        public User SelectedUser
        {
            get => _selectedUser;
            set
            {
                _selectedUser = value;
                NotifyPropertyChanged("SelectedUser");
            }
        }
        public IEnumerable<Position> PositionEnum
        {
            get
            {
                return Enum.GetValues(typeof(Position))
                    .Cast<Position>();
            }
        }
        public UserListViewModel()
        {
            _userRepo = new UserRepository(ConfigurationManager.ConnectionStrings["MySql"].ConnectionString); ;
            Users = new ObservableCollection<User>();
            SelectedUser = new User();
            RefreshCommand = new RelayCommand(param => Refresh());
        }
        private void Refresh()
        {
            Users = new ObservableCollection<User>(_userRepo.GetAll());
        }
    }
}
