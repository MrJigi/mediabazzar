﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Media_Bazaar
{
    class DataAccesEmployee
    {
        public string id;
        public string name;
        public string surName;
        public string birthDate;
        public string birthPlace;
        public string nationality;
        public string languages;
        public string bsn;
        public string homeAddress;
        public string zipCode;

        private MySqlConnection conn = new MySqlConnection("server=localhost;database=media_bazzar;uid=root;password=;");
        private string sql = "SELECT name FROM employees;";

        public DataAccesEmployee()
        {
            MySqlCommand cmd1 = new MySqlCommand(sql, conn);
        }

        public void GetEmployee(string searching, string name)
        {
            conn.Open();
            string query = "SELECT * FROM employees WHERE " + searching + " = '" + name + "' ";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            MySqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                this.id = (dr["id"].ToString());
                this.name = (dr["name"].ToString());
                this.surName = (dr["surname"].ToString());
                this.birthDate = (dr["birthDate"].ToString());
                this.birthPlace = (dr["birthPlace"].ToString());
                this.nationality = (dr["nationality"].ToString());
                this.languages = (dr["languages"].ToString());
                this.bsn = (dr["bsn"].ToString());
                this.homeAddress = (dr["homeAddress"].ToString());
                this.zipCode = (dr["zipCode"].ToString());
            }
            conn.Close();
        }

        public double GetEmployeePresentAbsentSick(string query)
        {
            conn.Open();
            MySqlCommand cmd2 = new MySqlCommand(query, conn);
            Int32 count = Convert.ToInt32(cmd2.ExecuteScalar());
            conn.Close();
            return count;
        }

        public string GetFirstLastShiftDate(string query)
        {
            conn.Open();
            MySqlCommand cmd4 = new MySqlCommand(query, conn);
            string firstLastShiftDate = cmd4.ExecuteScalar().ToString();
            conn.Close();
            return firstLastShiftDate;
        }
    }
}
