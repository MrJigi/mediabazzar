﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Media_Bazaar
{
    class User
    {
        private string MySQLConnectionString = "SERVER=localhost;DATABASE=media_bazzar; UID=root; PASSWORD=";
        private int id;
        private string name;
        private string surname;
        private string birthplace;
        private string birthDate; //Idk need to set how to retrieve that date
        private string nationality;
        private string languages;
        private int bsn;
        private string rank;
        private string homeAddress;
        private string zipCode;
        private bool isFired;
        private string username;
        private string password;
        public int intexOfEmp;
        List<User> newEmployee;
        MySqlConnection databaseConnection;
        MySqlCommand commandDatabase;



        public int Id
        {
            get
            {
                return id;
            }
            set
            {

                id = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string Surname
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
            }
        }
        public string BirthPlace
        {
            get
            {
                return birthplace;
            }
            set
            {
                birthplace = value;
            }
        }
        public string Nationality
        {
            get
            {
                return nationality;
            }
            set
            {
                nationality = value;
            }
        }
        public string Languages
        {
            get
            {
                return languages;
            }
            set
            {
                languages = value;
            }
        }
        public int Bsn
        {
            get
            {
                return bsn;
            }
            set
            {
                bsn = value;
            }
        }
        public string HomeAdress
        {
            get
            {
                return homeAddress;
            }
            set
            {
                homeAddress = value;
            }
        }
        public string ZipCode
        {
            get
            {
                return zipCode;
            }
            set
            {
                zipCode = value;
            }
        }
        public string Rank
        {
            get
            {
                return rank;
            }
            set
            {
                rank = value;
            }
        }
        public bool Isfired
        {
            get
            {
                return isFired;
            }
            set
            {
                isFired = value;
            }
        }
        public string BirthDate
        {
            get
            {
                return birthDate;
            }
            set
            {
                birthDate = value;
            }
        }
        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
            }
        }
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }
        public int IndexOfEmp
        {
            get
            {
                return intexOfEmp;
            }
            set
            {
                intexOfEmp = value + 1;
            }
        }
        //List<User> returnAllEmployees;
        List<User> userList;

        User employeeName;

        public User()
        {
            Id = id;
            Name = name;
            Surname = surname;
            Isfired = isFired;
        }
        public User(int id, string name, string surname, string birthDate, string birthPlace, string nationality, string languages, int bsn, string homeAdress, string zipCode, string rank, bool isFired, string username, string password)
        {
            Id = id;
            Name = name;
            Surname = surname;
            BirthDate = birthDate;
            BirthPlace = birthPlace;
            Nationality = nationality;
            Languages = languages;
            Rank = rank;
            Bsn = bsn;
            HomeAdress = homeAdress;
            ZipCode = zipCode;
            Isfired = isFired;
            Username = username;
            Password = password;
            

        }
        public User(string name, string surname,string birthDate, string birthPlace, string nationality, string languages, int bsn, string homeAdress, string zipCode, string rank, string username, string password)
        {
            Name = name;
            Surname = surname;
            BirthPlace = birthPlace;
            BirthDate = birthDate;
            Nationality = nationality;
            Languages = languages;
            Rank = rank;
            Bsn = bsn;
            HomeAdress = homeAdress;
            ZipCode = zipCode;
            isFired = false;
            Username = username;
            Password = password;
        }
        public User(int id, bool isFired)
        {
            Id = id;
            Isfired = isFired;
        }

        public User(int id, string name, string surname)
        {
            Id = id;
            Name = name;
            Surname = surname;
        }
        public void CreateUser()
        {
            var command = "INSERT INTO employees (name,surname,birthDate,birthPlace,nationality,languages,bsn,homeAddress,zipCode,rank,fired,username,password)" +
                " VALUES (@name,@surname,@birthDate,@birthPlace,@nationality,@languages,@bsn,@homeAddress,@zipCode,@rank,@fired,@username,@password)";
            databaseConnection = new MySqlConnection(MySQLConnectionString);
            commandDatabase = new MySqlCommand(command, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            //commandDatabase.Parameters.AddWithValue("@id" , Id);
            commandDatabase.Parameters.AddWithValue("@name", Name);
            commandDatabase.Parameters.AddWithValue("@surname", Surname);
            commandDatabase.Parameters.AddWithValue("@birthDate", BirthDate);
            commandDatabase.Parameters.AddWithValue("@birthPlace", BirthPlace);
            commandDatabase.Parameters.AddWithValue("@nationality", Nationality);
            commandDatabase.Parameters.AddWithValue("@languages", Languages);
            commandDatabase.Parameters.AddWithValue("@bsn", Bsn);
            commandDatabase.Parameters.AddWithValue("@homeAddress", HomeAdress);
            commandDatabase.Parameters.AddWithValue("@zipCode", ZipCode);
            commandDatabase.Parameters.AddWithValue("@rank", Rank);
            commandDatabase.Parameters.AddWithValue("@fired", isFired);
            commandDatabase.Parameters.AddWithValue("@username", Username);
            commandDatabase.Parameters.AddWithValue("@password", Password);
            /*userList.Add("");*/
            databaseConnection.Open();
            commandDatabase.ExecuteNonQuery();
            databaseConnection.Close();
        }
        public enum pos
        {
            Administrator,
            Manager,
            Depo
        }

        public List<User> GetAllEmployees() //Gets all employees with their name and surname
        {
            List<User> returnAllEmployees = new List<User>();
            using (MySqlConnection databaseConnections = new MySqlConnection(MySQLConnectionString))
            {
                var command = "Select id,name,surname from employees";
                MySqlConnection databaseConnection = new MySqlConnection(MySQLConnectionString);
                MySqlCommand commandDatabase = new MySqlCommand(command, databaseConnection);
                commandDatabase.CommandTimeout = 60;
                returnAllEmployees = new List<User>();

                databaseConnection.Open();
                MySqlDataReader employees = commandDatabase.ExecuteReader();

                while (employees.Read())
                {
                    User employeeInfo = new User(Convert.ToInt32(employees["id"]), employees["name"].ToString(), employees["surname"].ToString());
                    //Product p = new Product(Convert.ToInt32(dr[0]), dr[1].ToString(), Convert.ToInt32(dr[2]), dr[3].ToString(), Convert.ToSingle(dr[4]), dr[5].ToString());
                    returnAllEmployees.Add(employeeInfo);//(employees[1].ToString(), employees[2].ToString());
                }

                databaseConnection.Close();
                return returnAllEmployees;
            }
        }

        public List<User> GetAllUserInfo() //Gets all employees with their name and surname
        {
            List<User> returnEmployeeInfo = new List<User>();
            using (MySqlConnection databaseConnections = new MySqlConnection(MySQLConnectionString))
            {
                var command = "Select * from employees where id =  '" + IndexOfEmp + "' ";
                MySqlConnection databaseConnection = new MySqlConnection(MySQLConnectionString);
                MySqlCommand commandDatabase = new MySqlCommand(command, databaseConnection);
                commandDatabase.CommandTimeout = 60;
                returnEmployeeInfo = new List<User>();

                databaseConnection.Open();
                MySqlDataReader employees = commandDatabase.ExecuteReader();

                while (employees.Read())
                {
                    User employeeInfo = new User(Convert.ToInt32(employees["id"]), employees["name"].ToString(), employees["surname"].ToString(), employees["birthDate"].ToString(), employees["birthPlace"].ToString(), employees["nationality"].ToString(), employees["languages"].ToString(), Convert.ToInt32(employees["bsn"]), employees["homeAddress"].ToString(), employees["zipCode"].ToString(), employees["rank"].ToString(), Convert.ToBoolean(employees["fired"]), employees["username"].ToString(), employees["password"].ToString());
                    //Product p = new Product(Convert.ToInt32(dr[0]), dr[1].ToString(), Convert.ToInt32(dr[2]), dr[3].ToString(), Convert.ToSingle(dr[4]), dr[5].ToString());
                    returnEmployeeInfo.Add(employeeInfo);//(employees[1].ToString(), employees[2].ToString());
                }

                databaseConnection.Close();
                return returnEmployeeInfo;
            }
        }

        public void SaveAllUserChanges()
        {
            //List<User> returnEditedUserInfo = new List<User>();
            using (MySqlConnection databaseConnections = new MySqlConnection(MySQLConnectionString))
            {
                var command = "UPDATE employees Set name = @Name , surname = @Surname , birthDate = @BirthDate , birthPlace = @Birthplace , nationality = @Nationality , languages = @Languages, bsn = @Bsn , homeAddress = @HomeAdress , zipCode = @zip, fired = @IsFired , username = @Username , password = @Password  " +
                    " where id = '" + IndexOfEmp + " '";

                databaseConnection = new MySqlConnection(MySQLConnectionString);
                commandDatabase = new MySqlCommand(command, databaseConnection);

                commandDatabase.Parameters.AddWithValue("@Name", Name);
                commandDatabase.Parameters.AddWithValue("@Surname", Surname);
                commandDatabase.Parameters.AddWithValue("@BirthDate", BirthDate);
                commandDatabase.Parameters.AddWithValue("@BirthPlace", BirthPlace);
                commandDatabase.Parameters.AddWithValue("@Nationality", Nationality);
                commandDatabase.Parameters.AddWithValue("@Languages", Languages);
                commandDatabase.Parameters.AddWithValue("@Bsn", Bsn);
                commandDatabase.Parameters.AddWithValue("@HomeAdress", HomeAdress);
                commandDatabase.Parameters.AddWithValue("@Zip", ZipCode);
                commandDatabase.Parameters.AddWithValue("@Rank", Rank);
                commandDatabase.Parameters.AddWithValue("@Isfired", Isfired);
                commandDatabase.Parameters.AddWithValue("@Username", Username);
                commandDatabase.Parameters.AddWithValue("@Password", Password);
                /*userList.Add("");*/
                databaseConnection.Open();
                commandDatabase.ExecuteNonQuery();
                databaseConnection.Close();

            }
        }

        public void changeToFired()
        {
            using (MySqlConnection databaseConnections = new MySqlConnection(MySQLConnectionString))
            {
                var command = "UPDATE employees Set fired = @IsFired where id = '" + IndexOfEmp + " ' ";

                databaseConnection = new MySqlConnection(MySQLConnectionString);
                commandDatabase = new MySqlCommand(command, databaseConnection);

                if (Isfired == true)
                {
                    Isfired = false;
                }
                else 
                {
                    Isfired = true;
                }
                commandDatabase.Parameters.AddWithValue("@IsFired", Isfired);

                databaseConnection.Open();
                commandDatabase.ExecuteNonQuery();
                databaseConnection.Close();
            }

        }

        public bool checkIfPasswordExists(string Username, string Password)
        {

            var command = "Select username,password From employees Where (username = '" + Username + "') AND (password = '" + Password + "')";
            MySqlConnection databaseConnection = new MySqlConnection(MySQLConnectionString);
            MySqlCommand commandDatabase = new MySqlCommand(command, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            string message;
            try
            {
                databaseConnection.Open();

                MySqlDataReader myReader = commandDatabase.ExecuteReader();

                if (myReader.HasRows)
                {
                    databaseConnection.Close();
                    return false;
                }
                else
                {
                    databaseConnection.Close();
                    return true;
                }

            }
            catch (Exception e)
            {
                message = "user pass" + e.Message;
                databaseConnection.Close();
                return false;
            }
        }
        //User Statistics
        public string checkAllUsers()
        {

            using (MySqlConnection databaseConnections = new MySqlConnection(MySQLConnectionString))
            {
                string all;
                var command = "select count(id) from employees";
                MySqlConnection databaseConnection = new MySqlConnection(MySQLConnectionString);
                MySqlCommand commandDatabase = new MySqlCommand(command, databaseConnection);
                commandDatabase.CommandTimeout = 60;

                databaseConnection.Open();

                all = commandDatabase.ExecuteScalar().ToString();
                databaseConnection.Close();
                return all;

            }
        }
        public string checkInactiveUsers()
        {

            using (MySqlConnection databaseConnections = new MySqlConnection(MySQLConnectionString))
            {
                string inactive;
                var command = "select count(id) from employees where fired = 1";
                MySqlConnection databaseConnection = new MySqlConnection(MySQLConnectionString);
                MySqlCommand commandDatabase = new MySqlCommand(command, databaseConnection);
                commandDatabase.CommandTimeout = 60;

                databaseConnection.Open();

                inactive = commandDatabase.ExecuteScalar().ToString();
                databaseConnection.Close();
                return inactive;

            }
        }
        public string checkActiveUsers()
        {

            using (MySqlConnection databaseConnections = new MySqlConnection(MySQLConnectionString))
            {
                string active;
                var command = "select count(id) from employees where fired = 0";
                MySqlConnection databaseConnection = new MySqlConnection(MySQLConnectionString);
                MySqlCommand commandDatabase = new MySqlCommand(command, databaseConnection);
                commandDatabase.CommandTimeout = 60;

                databaseConnection.Open();

                active = commandDatabase.ExecuteScalar().ToString();
                databaseConnection.Close();
                return active;

            }
        }

        public string checkAllAdministrators()
        {

            using (MySqlConnection databaseConnections = new MySqlConnection(MySQLConnectionString))
            {
                string Administrator;
                var command = "select count(id) from employees where rank = 'Administrator' and fired = 0";
                MySqlConnection databaseConnection = new MySqlConnection(MySQLConnectionString);
                MySqlCommand commandDatabase = new MySqlCommand(command, databaseConnection);
                commandDatabase.CommandTimeout = 60;

                databaseConnection.Open();

                Administrator = commandDatabase.ExecuteScalar().ToString();
                databaseConnection.Close();
                return Administrator;
            }

        }

        public string checkAllManagers()
        {

            using (MySqlConnection databaseConnections = new MySqlConnection(MySQLConnectionString))
            {
                string Manager;
                var command = "select count(id) from employees where rank = 'Manager' and fired = 0";
                MySqlConnection databaseConnection = new MySqlConnection(MySQLConnectionString);
                MySqlCommand commandDatabase = new MySqlCommand(command, databaseConnection);
                commandDatabase.CommandTimeout = 60;

                databaseConnection.Open();

                Manager = commandDatabase.ExecuteScalar().ToString();
                databaseConnection.Close();
                return Manager;
            }

        }
        public string checkAllDepo()
        {

            using (MySqlConnection databaseConnections = new MySqlConnection(MySQLConnectionString))
            {
                string Depo;
                var command = "select count(id) from employees where rank = 'Depo' and fired = 0";
                MySqlConnection databaseConnection = new MySqlConnection(MySQLConnectionString);
                MySqlCommand commandDatabase = new MySqlCommand(command, databaseConnection);
                commandDatabase.CommandTimeout = 60;

                databaseConnection.Open();

                Depo = commandDatabase.ExecuteScalar().ToString();
                databaseConnection.Close();
                return Depo;
            }

        }


    }
}
