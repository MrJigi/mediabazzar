﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Media_Bazaar
{
    class DataAccessSR
    {
        string connStr = "SERVER=localhost;DATABASE=media_bazzar; UID=root; PASSWORD=";
        public List<stock_request> GetAllStockReq()
        {
            List<stock_request> returnedOpenStockReq = new List<stock_request>();
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "SELECT * FROM stocks_request_s";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                conn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    stock_request sr = new stock_request(Convert.ToInt32(dr[0]), Convert.ToInt32(dr[1]), dr[2].ToString(), Convert.ToInt32(dr[3]), dr[4].ToString(), Convert.ToSingle(dr[5]), dr[6].ToString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString());
                    sr.Date = dr[10].ToString();
                    returnedOpenStockReq.Add(sr);
                }
                conn.Close();
                return returnedOpenStockReq;
            }
        }

        public List<stock_request> GetOpenStockReq()
        {
            List<stock_request> returnedOpenStockReq = new List<stock_request>();
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "SELECT * FROM stocks_request_s WHERE request_state=@openstate";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@openstate", "open");
                conn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    stock_request sr = new stock_request(Convert.ToInt32(dr[0]), Convert.ToInt32(dr[1]), dr[2].ToString(), Convert.ToInt32(dr[3]), dr[4].ToString(), Convert.ToSingle(dr[5]), dr[6].ToString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString());
                    sr.Date = dr[10].ToString();
                    returnedOpenStockReq.Add(sr);
                }
                conn.Close();
                return returnedOpenStockReq;
            }
        }

        public List<stock_request> GetPendingStockReq()
        {
            List<stock_request> returnedOpenStockReq = new List<stock_request>();
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "SELECT * FROM stocks_request_s WHERE request_state=@pstate";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@pstate", "pending");
                conn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    stock_request sr = new stock_request(Convert.ToInt32(dr[0]), Convert.ToInt32(dr[1]), dr[2].ToString(), Convert.ToInt32(dr[3]), dr[4].ToString(), Convert.ToSingle(dr[5]), dr[6].ToString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString());
                    sr.Date = dr[10].ToString();
                    returnedOpenStockReq.Add(sr);
                }
                conn.Close();
                return returnedOpenStockReq;
            }
        }

        public List<stock_request> GetClosedStockReq()
        {
            List<stock_request> returnedOpenStockReq = new List<stock_request>();
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "SELECT * FROM stocks_request_s WHERE request_state=@cstate";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@cstate", "closed");
                conn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    stock_request sr = new stock_request(Convert.ToInt32(dr[0]), Convert.ToInt32(dr[1]), dr[2].ToString(), Convert.ToInt32(dr[3]), dr[4].ToString(), Convert.ToSingle(dr[5]), dr[6].ToString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString());
                    sr.Date = dr[10].ToString();
                    returnedOpenStockReq.Add(sr);
                }
                conn.Close();
                return returnedOpenStockReq;
            }
        }

        public void MarkRequestAsPending(int rid)
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {

                string sql = "UPDATE stocks_request_s SET request_state=@state WHERE Id=@sId";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@sId", rid);
                cmd.Parameters.AddWithValue("@state", "pending");


                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void MarkRequestAsClosed(int rid)
        {

            using (MySqlConnection conn = new MySqlConnection(connStr))
            {

                string sql = "UPDATE stocks_request_s SET request_state=@state WHERE Id=@sId";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@sId", rid);
                cmd.Parameters.AddWithValue("@state", "closed");


                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void MarkRefillRequestAsClosed(int rid)
        {
            DateTime date = DateTime.UtcNow.Date;
            string InputdbDate = date.ToString("yyyy-MM-dd");
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {

                string sql = "UPDATE stocks_request_s SET request_state=@state,arrival_date=@av WHERE Id=@sId";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@sId", rid);
                cmd.Parameters.AddWithValue("@state", "closed");
                cmd.Parameters.AddWithValue("@av", InputdbDate);


                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        public int CountRequest()
        {

            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "SELECT COUNT(id) from stocks_request_s WHERE request_state=@rs";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@rs", "open");
                conn.Open();
                Object result = cmd.ExecuteScalar();
                int returnedCount = -1;
                if (result != null)
                {
                    returnedCount = Convert.ToInt32(result);
                    return returnedCount;

                }
                else
                {
                    returnedCount = 0;
                    return returnedCount;
                }
            }
        }

        public void AddRequestForNewStock(string name, int quantity, string description, float purchasev, string type)
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "INSERT INTO stocks_request_s (pid, name, quantity, description, purchase_value, state, type, request_type, request_state) VALUES (@productid, @nameValue, @quantityValue, @descriptionValue, @purchaseValue, @stateValue,@typeValue, @requestype, @requeststate)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@productid", 0);
                cmd.Parameters.AddWithValue("@nameValue", name);
                cmd.Parameters.AddWithValue("@quantityValue", quantity);
                cmd.Parameters.AddWithValue("@descriptionValue", description);
                cmd.Parameters.AddWithValue("@purchaseValue", purchasev);
                cmd.Parameters.AddWithValue("@stateValue", "active");
                cmd.Parameters.AddWithValue("@requestype", "new");
                cmd.Parameters.AddWithValue("@requeststate", "open");
                cmd.Parameters.AddWithValue("@typeValue", type);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void AddRequestForStockRefill(int pi, string name, int quantity, string description, float purchasev, string state, string type)
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "INSERT INTO stocks_request_s (pid, name, quantity, description, purchase_value, state, type, request_type, request_state) VALUES (@productid, @nameValue, @quantityValue, @descriptionValue, @purchaseValue, @stateValue, @typeValue, @requestype, @requeststate)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@productid", pi);
                cmd.Parameters.AddWithValue("@nameValue", name);
                cmd.Parameters.AddWithValue("@quantityValue", quantity);
                cmd.Parameters.AddWithValue("@descriptionValue", description);
                cmd.Parameters.AddWithValue("@purchaseValue", purchasev);
                cmd.Parameters.AddWithValue("@stateValue", state);
                cmd.Parameters.AddWithValue("@requestype", "ref");
                cmd.Parameters.AddWithValue("@requeststate", "open");
                cmd.Parameters.AddWithValue("@typeValue", type);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void AddRequestForStockModif(int pi, string name, string description, float purchasev, string state, string type)
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                string sql = "INSERT INTO stocks_request_s (pid, name, description, purchase_value, state, type, request_type, request_state) VALUES (@productid, @nameValue, @descriptionValue, @purchaseValue, @stateValue,@typeValue, @requestype, @requeststate)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@productid", pi);
                cmd.Parameters.AddWithValue("@nameValue", name);
                cmd.Parameters.AddWithValue("@descriptionValue", description);
                cmd.Parameters.AddWithValue("@purchaseValue", purchasev);
                cmd.Parameters.AddWithValue("@stateValue", state);
                cmd.Parameters.AddWithValue("@requestype", "mod");
                cmd.Parameters.AddWithValue("@requeststate", "open");
                cmd.Parameters.AddWithValue("@typeValue", type);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public int GetNrStockRequestMonthly(int id)
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                var myDate = DateTime.Now;
                var startOfMonth = new DateTime(myDate.Year, myDate.Month, 1);
                var endOfMonth = startOfMonth.AddMonths(1).AddDays(-1);
                string startDate = startOfMonth.ToString("yyyy-MM-dd");
                string endDate = endOfMonth.ToString("yyyy-MM-dd");
                string sql = "SELECT COUNT(id) from stocks_request_s WHERE pid=@id AND request_type=@rt AND arrival_date BETWEEN @monthstart AND @monthend ";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@rt", "ref");
                cmd.Parameters.AddWithValue("@monthstart", startDate);
                cmd.Parameters.AddWithValue("@monthend", endDate);

                conn.Open();
                Object result = cmd.ExecuteScalar();
                int returnedCount = -1;
                if (result != null)
                {
                    returnedCount = Convert.ToInt32(result);
                    return returnedCount;

                }
                else
                {
                    returnedCount = 0;
                    return returnedCount;
                }
            }
        }

        public List<stock_request> GetStockRefillReq(int id)
        {
            List<stock_request> returnedOpenStockReq = new List<stock_request>();
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                var myDate = DateTime.Now;
                var startOfMonth = new DateTime(myDate.Year, myDate.Month, 1);
                var endOfMonth = startOfMonth.AddMonths(1).AddDays(-1);
                string startDate = startOfMonth.ToString("yyyy-MM-dd");
                string endDate = endOfMonth.ToString("yyyy-MM-dd");
                string sql = "SELECT * FROM stocks_request_s WHERE pid=@id AND request_type=@rt AND arrival_date BETWEEN @monthstart AND @monthend;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@rt", "ref");
                cmd.Parameters.AddWithValue("@monthstart", startDate);
                cmd.Parameters.AddWithValue("@monthend", endDate);
                conn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    stock_request sr = new stock_request(Convert.ToInt32(dr[0]), Convert.ToInt32(dr[1]), dr[2].ToString(), Convert.ToInt32(dr[3]), dr[4].ToString(), Convert.ToSingle(dr[5]), dr[6].ToString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString());
                    sr.Date = dr[10].ToString();
                    returnedOpenStockReq.Add(sr);
                }
                conn.Close();
                return returnedOpenStockReq;
            }
        }
    }
}
