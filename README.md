Content of client questions, minutes and project plan is located in a doc file, link below.
https://docs.google.com/document/d/1MyZD30j8YkpJc6Upf0ZHQdCkcumLlpENJwS4VVDgxKg/edit?usp=sharing
Project Plan:
https://drive.google.com/file/d/1qLqv-1CxcNdd6Y2y9XkOcUxHuyt2eZeQ/view?usp=sharing
URS:
https://docs.google.com/document/d/1YVeuVl2_OX3qNlchLDssv-Qv70yRcFQ52Y6f8HvTPmY/edit?usp=sharing
Mobile friendly interactive wireframe for the client website, mobile view:
https://xd.adobe.com/view/6f64e829-e33e-469b-5fab-55d5b4b4a0a4-1955/
Interactive wireframe for the client website, desktop view:
https://xd.adobe.com/view/438c51b4-4b52-47c0-4eee-c5763b76ec3c-35bd/