﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media_Bazaar
{
    public class SortByQty : IComparer<stock>
    {

        public int Compare(stock x, stock y)
        {
            return x.getQuantity.CompareTo(y.getQuantity);
        }
    }

}
