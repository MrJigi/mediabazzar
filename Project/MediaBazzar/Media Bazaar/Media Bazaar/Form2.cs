﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Media_Bazaar
{
    public partial class Form2 : Form
    {
        List<stock> stocks = new List<stock>();
        List<stock_request> req_stocks = new List<stock_request>();
        public Form2()
        {
            InitializeComponent();
            CustomizeLayout();
            timerCheckNotif.Start();
            //timerCheckNotif.Stop();

        }

        private void UpdateBind()
        {
            listBoxProducts.DataSource = stocks;
            listBoxProducts.DisplayMember = "FullInfo";
            listBoxProducts.ValueMember = "getId";
        }

        private void UpdateBindProductReq()
        {
            listBoxProductReq.DataSource = req_stocks;
            listBoxProductReq.DisplayMember = "RequestInfo";
            listBoxProductReq.ValueMember = "getId";
        }

        private void timerCheckNotif_Tick(object sender, EventArgs e)
        {
            DataAccessSR db = new DataAccessSR();
            int notificationNr = db.CountRequest();
            if (notificationNr == 0)
            {
                panelNotificationCount.Visible = false;
                labelNotificationPopText.Text = "You have no open product request";
            }
            else
            {
                //panelInfoRequestNotification.Visible = true;
                panelNotificationCount.Visible = true;
                labelActiveNotif.Text = notificationNr.ToString();
                labelNotificationPopText.Text = $"You have {notificationNr} open product requests";
            }
        }

        private void btnCloseNotificationPop_Click(object sender, EventArgs e)
        {
            panelInfoRequestNotification.Visible = false;
        }

        private void btnNotificationReq_Click(object sender, EventArgs e)
        {
            panelInfoRequestNotification.Visible = true;
            HideProductDashSubMenu();
            panelProductDash.Visible = true;
            panelAddProduct.Visible = false;
            panelModifyProduct.Visible = false;
            panelProductRequests.Visible = true;
            panelProductRequests.Dock = DockStyle.Fill;

        }

        #region SideMenu
        private void CustomizeLayout()
        {
            panelProductDashboardSubMenu.Visible = false;
            listBoxProducts.HorizontalScrollbar = true;
            listBoxProductReq.HorizontalScrollbar = true;

        }

        private void btnProductReq_Click(object sender, EventArgs e)
        {
            HideProductDashSubMenu();
            panelProductDash.Visible = true;
            panelAddProduct.Visible = false;
            panelModifyProduct.Visible = false;
            panelProductRequests.Visible = true;
            panelSearchSpecificProduct.Visible = false;
            panelProductRequests.Dock = DockStyle.Fill;
        }
        private void HideProductDashSubMenu()
        {
            if (panelProductDashboardSubMenu.Visible == true)
            {
                panelProductDashboardSubMenu.Visible = false;
            }
        }

        private void ShowDashSubMenu(Panel submenu)
        {
            if (submenu.Visible == false)
            {
                HideProductDashSubMenu();
                submenu.Visible = true;
            }
            else
            {
                submenu.Visible = false;
            }
        }

        private void btnProductDashboard_Click(object sender, EventArgs e)
        {

            ShowDashSubMenu(panelProductDashboardSubMenu);
            panelProductDash.Visible = true;
            panelAddProduct.Visible = false;
            panelModifyProduct.Visible = false;
            panelProductRequests.Visible = false;
            panelSearchSpecificProduct.Visible = false;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            HideProductDashSubMenu();
            panelMainContent.Visible = true;
            panelProductDash.Visible = false;
            panelAddProduct.Visible = false;
            panelModifyProduct.Visible = false;
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            timerCheckNotif.Stop();
            this.Close();
        }

        #endregion



        #region ProductDashboard
        private void btnNavAddProduct_Click(object sender, EventArgs e)
        {
            btnMenuAddProduct.BackColor = Color.FromArgb(20, 20, 60);
            btnMenuModifyProduct.BackColor = Color.FromArgb(20, 20, 30);
            //btnMenuRemoveProduct.BackColor = Color.FromArgb(20, 20, 30);

            panelAddProduct.Visible = true;
            panelAddProduct.Dock = DockStyle.Fill;
        }

        private void btnNavModifyProduct_Click(object sender, EventArgs e)
        {
            btnMenuAddProduct.BackColor = Color.FromArgb(20, 20, 30);
            btnMenuModifyProduct.BackColor = Color.FromArgb(20, 20, 60);
            //btnMenuRemoveProduct.BackColor = Color.FromArgb(20, 20, 30);
            panelModifyProduct.Visible = true;
            panelModifyProduct.Dock = DockStyle.Fill;
            int selectedValue = Convert.ToInt32(listBoxProducts.SelectedValue);
            foreach (stock s in stocks)
            {
                if (s.getId == selectedValue)
                {
                    textBoxModifyProductName.Text = s.getName;
                    textBoxModifyProductQuantity.Text = s.getQuantity.ToString();
                    textBoxModifyProductDescription.Text = s.getDescription;
                    textBoxModifyProductValue.Text = s.getpurchaseValue.ToString();
                    textBoxModifyProductType.Text = s.getType;
                    if (s.getState == "active")
                    {
                        radioButtonActiveProduct.Checked = true;
                    }
                    if (s.getState == "inactive")
                    {
                        radioButtonInactiveProduct.Checked = true;
                    }
                }
            }
        }

        private void CleanListbox()
        {
            listBoxProducts.DataSource = null;
            listBoxProducts.Items.Clear();
        }
        private void btnNavRemoveProduct_Click(object sender, EventArgs e)
        {
            btnMenuAddProduct.BackColor = Color.FromArgb(20, 20, 30);
            btnMenuModifyProduct.BackColor = Color.FromArgb(20, 20, 30);
            //btnMenuRemoveProduct.BackColor = Color.FromArgb(20, 20, 60);
            DataAccess db = new DataAccess();
            int selectedValue = Convert.ToInt32(listBoxProducts.SelectedValue);
            db.RemoveStock(selectedValue);
            CleanListbox();
            //Remove the product from the products list
            for (int i = stocks.Count - 1; i >= 0; i--)
            {
                if (stocks[i].getId == selectedValue)
                {
                    stocks.RemoveAt(i);
                }
            }

            UpdateBind();

        }
        private void btnViewAvailableProducts_Click(object sender, EventArgs e)
        {
           // btnNavModifyProduct.Visible = true;
            btnNavRemoveProduct.Visible = true;
            CleanListbox();
            DataAccess db = new DataAccess();
            stocks = db.GetActiveStock();
            UpdateBind();
        }

        private void btnViewUnavailableProducts_Click(object sender, EventArgs e)
        {
           // btnNavModifyProduct.Visible = true;
            btnNavRemoveProduct.Visible = false;
            CleanListbox();
            DataAccess db = new DataAccess();
            stocks = db.GetInactiveStock();
            UpdateBind();
        }
        private void btnViewAllProducts_Click(object sender, EventArgs e)
        {
           // btnNavModifyProduct.Visible = false;
            btnNavRemoveProduct.Visible = true;
            CleanListbox();
            DataAccess db = new DataAccess();
            stocks = db.GetStock();
            UpdateBind();

        }

        private void btnViewStockDescription_Click(object sender, EventArgs e)
        {
            int selectedValue = Convert.ToInt32(listBoxProducts.SelectedValue);
            foreach(stock s in stocks)
            {
                if (s.getId == selectedValue)
                {
                    MessageBox.Show($"Name: {s.getName}{Environment.NewLine}Type: {s.getType}{Environment.NewLine}Description:{Environment.NewLine}{s.getDescription}{Environment.NewLine}");
                }
            }
        }
        #endregion

        #region AddProduct
        private void btnBackFromAddProduct_Click(object sender, EventArgs e)
        {
            panelAddProduct.Visible = false;
            panelSearchSpecificProduct.Visible = false;
            panelProductDash.Visible = true;
        }
        private void btnAddProductClearFields_Click(object sender, EventArgs e)
        {
            textBoxAddProductName.Text = "";
            textBoxAddProductDescription.Text = "";
            textBoxAddProductQuantity.Text = "";
            textBoxAddProductValue.Text = "";
            textBoxAddProductType.Text = "";
        }

        private void btnAddProductToDb_Click(object sender, EventArgs e)
        {

            panelAddProduct.Visible = false;
            panelProductDash.Visible = true;
            string getInputPName = textBoxAddProductName.Text;
            string getInputPQuantity = textBoxAddProductQuantity.Text;
            string getInputPDescription = textBoxAddProductDescription.Text;
            string getInputPPurchaseValue = textBoxAddProductValue.Text;
            string getInputPType = textBoxAddProductType.Text;
            string InputState = "active";
            if (string.IsNullOrEmpty(getInputPName) || string.IsNullOrEmpty(getInputPType) || string.IsNullOrEmpty(getInputPQuantity) || string.IsNullOrEmpty(getInputPDescription) || string.IsNullOrEmpty(getInputPPurchaseValue))
            {
                MessageBox.Show("All textboxs must be filled");
            }
            else
            {
                int number1 = 0;
                bool canConvertInt = int.TryParse(getInputPQuantity, out number1);
                if (canConvertInt == true && Convert.ToInt32(getInputPQuantity) != 0)
                {
                    float number2 = 0;
                    bool canConvertFloat = float.TryParse(getInputPPurchaseValue, out number2);
                    if (canConvertFloat == true && Convert.ToSingle(getInputPPurchaseValue) != 0 && Convert.ToSingle(getInputPPurchaseValue) >= 1)
                    {
                        DataAccess db = new DataAccess();
                        db.InsertStock(getInputPName, Convert.ToInt32(getInputPQuantity), getInputPDescription, Convert.ToSingle(getInputPPurchaseValue), InputState, getInputPType);
                    }
                    else
                    {
                        MessageBox.Show($"Purchase value is not valid.{Environment.NewLine}Try entering a valid number");
                    }

                }
                else
                {
                    MessageBox.Show($"Quantity is not vaild.{Environment.NewLine}Try entering a valid number");
                }
            }


        }
        #endregion


        #region ModifyProduct
        private void btnModifyProductToDb_Click(object sender, EventArgs e)
        {
            panelModifyProduct.Visible = false;
            panelSearchSpecificProduct.Visible = false;
            panelProductDash.Visible = true;
            string getInputPName = textBoxModifyProductName.Text;
            string getInputPQuantity = textBoxModifyProductQuantity.Text;
            string getInputPDescription = textBoxModifyProductDescription.Text;
            string getInputPPurchaseValue = textBoxModifyProductValue.Text;
            int selectedValue = Convert.ToInt32(listBoxProducts.SelectedValue);
            string getInputPType = textBoxModifyProductType.Text;
            string InputState = "";
            if (radioButtonActiveProduct.Checked == true)
            {
                radioButtonInactiveProduct.Checked = false;
                InputState = "active";
            }
            if (radioButtonInactiveProduct.Checked == true)
            {
                radioButtonActiveProduct.Checked = false;
                InputState = "inactive";
            }

            if (string.IsNullOrEmpty(getInputPName) || string.IsNullOrEmpty(getInputPType) || string.IsNullOrEmpty(getInputPQuantity) || string.IsNullOrEmpty(getInputPDescription) || string.IsNullOrEmpty(getInputPPurchaseValue))
            {
                MessageBox.Show("All textboxs must be filled");
            }
            else
            {
                int number1 = 0;
                bool canConvertInt = int.TryParse(getInputPQuantity, out number1);
                if (canConvertInt == true && Convert.ToInt32(getInputPQuantity) != 0)
                {
                    float number2 = 0;
                    bool canConvertFloat = float.TryParse(getInputPPurchaseValue, out number2);
                    if (canConvertFloat == true && Convert.ToSingle(getInputPPurchaseValue) != 0 && Convert.ToSingle(getInputPPurchaseValue) >= 1)
                    {
                        // CleanListbox();
                        DataAccess db = new DataAccess();
                        db.ModifyStock(selectedValue, getInputPName, Convert.ToInt32(getInputPQuantity), getInputPDescription, Convert.ToSingle(getInputPPurchaseValue), InputState, getInputPType);
                        //UpdateBind();
                    }
                    else
                    {
                        MessageBox.Show($"Purchase value is not valid.{Environment.NewLine}Try entering a valid number");
                    }

                }
                else
                {
                    MessageBox.Show($"Quantity is not vaild.{Environment.NewLine}Try entering a valid number");
                }
            }
        }

        private void btnBackFromModifyProduct_Click(object sender, EventArgs e)
        {
            panelModifyProduct.Visible = false;
            panelAddProduct.Visible = false;
            panelProductDash.Visible = true;
        }

        private void btnClearModifyProductFields_Click(object sender, EventArgs e)
        {
            textBoxModifyProductName.Text = "";
            textBoxModifyProductDescription.Text = "";
            textBoxModifyProductQuantity.Text = "";
            textBoxModifyProductValue.Text = "";
            textBoxModifyProductType.Text = "";
        }




        #endregion

        #region ReqProduct
        private void btnViewProductReq_Click(object sender, EventArgs e)
        {
            btnMarkPending.Visible = true;
            btnMarkPending.Location = btnCloseProductReq.Location;
            btnCloseProductReq.Visible = false;
            DataAccessSR db = new DataAccessSR();
            req_stocks = db.GetOpenStockReq();

            UpdateBindProductReq();
        }
        private void btnViewPendingReq_Click(object sender, EventArgs e)
        {
            btnMarkPending.Visible = false;
            btnCloseProductReq.Visible = true;
            DataAccessSR db = new DataAccessSR();
            req_stocks = db.GetPendingStockReq();

            UpdateBindProductReq();
        }

        private void btnViewClosedReq_Click(object sender, EventArgs e)
        {
            btnMarkPending.Visible = false;
            btnCloseProductReq.Visible = false;
            DataAccessSR db = new DataAccessSR();
            req_stocks = db.GetClosedStockReq();

            UpdateBindProductReq();
        }

        private void btnMarkPending_Click(object sender, EventArgs e)
        {
            DataAccessSR db = new DataAccessSR();
            int selectedValue = Convert.ToInt32(listBoxProductReq.SelectedValue);
            foreach (stock_request sr in req_stocks)
            {
                if (sr.getId == selectedValue)
                {
                    
                        db.MarkRequestAsPending(selectedValue);
                        req_stocks = db.GetOpenStockReq();
                        UpdateBindProductReq();
                    
                }
                
            }
        }

        private void btnConfirmProductReq_Click(object sender, EventArgs e)
        {

            
            btnMenuAddProduct.BackColor = Color.FromArgb(20, 20, 60);
            btnMenuModifyProduct.BackColor = Color.FromArgb(20, 20, 30);
            DataAccess dbs = new DataAccess();
            DataAccessSR db = new DataAccessSR();
            int selectedValue = Convert.ToInt32(listBoxProductReq.SelectedValue);

            
            foreach (stock_request sr in req_stocks)
            {
                if (sr.getId == selectedValue&&sr.getTypeRequest == "new")
                {
                        panelProductRequests.Visible = false;
                        panelAddProduct.Visible = true;
                        panelAddProduct.Dock = DockStyle.Fill;
                        textBoxAddProductName.Text = sr.getName;
                        textBoxAddProductQuantity.Text = sr.getQuantity.ToString();
                        textBoxAddProductDescription.Text = sr.getDescription;
                        textBoxAddProductValue.Text = sr.getpurchaseValue.ToString();
                        textBoxAddProductType.Text = sr.getType;
                        db.MarkRequestAsClosed(selectedValue);
                        req_stocks = db.GetOpenStockReq();
                        UpdateBindProductReq();
                    
                }
                if (sr.getId == selectedValue && sr.getTypeRequest == "ref")
                {
                    DateTime date = DateTime.UtcNow.Date;
                    string InputdbDate = date.ToString("yyyy-MM-dd");
                    int qty = dbs.CalculateQuantity(sr.getPId, sr.getQuantity);
                    dbs.RefillStock(sr.getPId, qty);
                    db.MarkRefillRequestAsClosed(selectedValue);
                    req_stocks = db.GetPendingStockReq();
                    UpdateBindProductReq();

                }
                if (sr.getId == selectedValue && sr.getTypeRequest == "mod")
                {
                    dbs.ModifyStockRequest(sr.getPId, sr.getName, sr.getDescription, sr.getpurchaseValue, sr.getState, sr.getType);
                    db.MarkRequestAsClosed(selectedValue);
                    req_stocks = db.GetPendingStockReq();
                    UpdateBindProductReq();
                }


            }

            btnCloseProductReq.Visible = false;
        }

        #endregion

        #region SearchSpecificProduct
        private void BtnSearchProduct_Click(object sender, EventArgs e)
        {
            panelSearchSpecificProduct.Visible = true;
            panelSearchSpecificProduct.Dock = DockStyle.Fill;
        }

        private void BtnSearchSpecificProduct_Click(object sender, EventArgs e)
        {
            LbMultipleProducts.Items.Clear();
            conn.Open();
            string searching = "name";
            string toSearch = CbxToSearch.Text;
            if (toSearch == "Name")
            {
                searching = "name";
            }
            else if (toSearch == "Quantity")
            {
                searching = "quantity";
            }
            string name = " ";
            name = TbProductnq.Text;
            string query = "SELECT * FROM stocks WHERE " + searching + " = '" + name + "' ";
            MySqlCommand cmd2 = new MySqlCommand(query, conn);
            MySqlDataReader dr = cmd2.ExecuteReader();
            if (dr.Read())
            {
                LbMultipleProducts.Items.Add(dr["name"]);
                foreach (var item in dr)
                {
                    LbMultipleProducts.Items.Add(dr["name"]);
                }
                TbxId.Text = (dr["Id"].ToString());
                TbxName.Text = (dr["name"].ToString());
                TbxQuantity.Text = (dr["quantity"].ToString());
                TbxDescription.Text = (dr["description"].ToString());
                TbxPurchaseValue.Text = (dr["purchase_value"].ToString());
                TbxState.Text = (dr["state"].ToString());
            }
            conn.Close();
        }

        MySqlConnection conn = new MySqlConnection("server=localhost;database=media_bazzar;uid=root;password=;");
        string sql = "SELECT name FROM stocks;";

        private void DbConnect(string string1, MySqlConnection conn1)
        {
            MySqlCommand cmd1 = new MySqlCommand(string1, conn1);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            DbConnect(sql, conn);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            conn.Open();
            string name = " ";
            if (LbMultipleProducts.SelectedItem == null)
            {
                name = " ";
            }
            else
            {
                name = LbMultipleProducts.SelectedItem.ToString();
            }
            string query = "SELECT * FROM stocks WHERE name = '" + name + "' ";
            MySqlCommand cmd2 = new MySqlCommand(query, conn);
            MySqlDataReader dr = cmd2.ExecuteReader();
            if (dr.Read())
            {
                TbxId.Text = (dr["Id"].ToString());
                TbxName.Text = (dr["name"].ToString());
                TbxQuantity.Text = (dr["quantity"].ToString());
                TbxDescription.Text = (dr["description"].ToString());
                TbxPurchaseValue.Text = (dr["purchase_value"].ToString());
                TbxState.Text = (dr["state"].ToString());
            }
            conn.Close();
        }







        #endregion

        
    }
}
