﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media_Bazaar
{
    class stock_request
    {
        private int rid;
        private int pid;
        private string pname;
        private int pquantity;
        private string pdescription;
        private float ppurchaseValue;
        private string pstate;
        private string ptype;
        private string rtype;
        private string rstate;
        private string arrival_date;
        public int getId { get { return this.rid; } }
        public int getPId { get { return this.pid; } }
        public string getName { get { return this.pname; } }
        public int getQuantity { get { return this.pquantity; } }
        public string getDescription { get { return this.pdescription; } }

        public float getpurchaseValue { get { return this.ppurchaseValue; } }

        public string getState { get { return this.pstate; } }
        public string getTypeRequest { get { return this.rtype; } }
        public string getStateRequest { get { return this.rstate; } }
        public string Date { get { return this.arrival_date; } set { this.arrival_date = value; } }
        public string getType { get { return this.ptype; } }

        public stock_request(int id, int pid, string name, int quantity, string description, float purchaseValue, string state, string type, string reqtype, string reqstate)
        {
            this.rid = id;
            this.pid = pid;
            this.pname = name;
            this.pquantity = quantity;
            this.pdescription = description;
            this.ppurchaseValue = purchaseValue;
            this.pstate = state;
            this.rstate = reqstate;
            this.rtype = reqtype;
            this.ptype = type;
        }
        public string RequestInfo
        {
            get { return $"Req Id:{rid} | Name: {pname} | Qty:{pquantity} | Purchase Value:{ppurchaseValue}€ | RState:{rstate} | RType:{rtype} | AV:-{arrival_date}"; }
        }

        public string RequestRefill
        {
            get { return $"Name: {pname} | Qty:{pquantity} AV:-{arrival_date.Substring(0, 10)}"; }
        }
    }
}
