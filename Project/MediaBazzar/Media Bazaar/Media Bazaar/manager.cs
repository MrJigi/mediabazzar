﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Media_Bazaar
{
    public partial class manager : Form
    {
        bool requestMode=false;
        bool requestView = false;
        List<stock> stocks = new List<stock>();
        List<stock_request> req_stocks = new List<stock_request>();
        List<stock> stocks_stats = new List<stock>();
        List<stock_request> req_stocks_ref = new List<stock_request>();
        public manager()
        {
            InitializeComponent();
            panelMainContent.Visible = true;
            panelMainContent.Dock = DockStyle.Fill;
            listBoxProducts.HorizontalScrollbar = true;
            listBoxStockStats.HorizontalScrollbar = true;
            listBoxRefillLog.HorizontalScrollbar = true;
        }
        private void CleanListbox()
        {
            listBoxProducts.DataSource = null;
            listBoxProducts.Items.Clear();
        }
        private void CleanListboxStats()
        {
            listBoxStockStats.DataSource = null;
            listBoxStockStats.Items.Clear();
        }
        private void CleanListboxStatsRefill()
        {
            listBoxRefillLog.DataSource = null;
            listBoxRefillLog.Items.Clear();
        }
        private void UpdateBindStock()
        {
            listBoxProducts.DataSource = stocks;
            listBoxProducts.DisplayMember = "FullInfo";
            listBoxProducts.ValueMember = "getId";
        }

        private void UpdateBindStockRequest()
        {
            listBoxProducts.DataSource = req_stocks;
            listBoxProducts.DisplayMember = "RequestInfo";
            listBoxProducts.ValueMember = "getId";
        }

        private void UpdateBindStockStatisticsRefill()
        {
            listBoxRefillLog.DataSource = req_stocks_ref;
            listBoxRefillLog.DisplayMember = "RequestRefill";
            listBoxRefillLog.ValueMember = "getId";
        }

        private void UpdateBindStockStatistics()
        {
            listBoxStockStats.DataSource = stocks_stats;
            listBoxStockStats.DisplayMember = "StatisticsInfo";
            listBoxStockStats.ValueMember = "getId";
        }
        #region SideMenu
        private void labelLogo_Click(object sender, EventArgs e)
        {
            PanelSpecificEmployeeStatistics.Visible = false;
            PanelChoosSpecificOrAllEmployees.Visible = false;
            panelMainContent.Visible = true;
            panelMainContent.Dock = DockStyle.Fill;
            panelProductDash.Visible = false;
            panelAddProduct.Visible = false;
            panelStockStatistics.Visible = false;
        }

        private void btnProductReq_Click(object sender, EventArgs e)
        {
            PanelSpecificEmployeeStatistics.Visible = false;
            PanelChoosSpecificOrAllEmployees.Visible = false;
            panelProductDash.Visible = true;
            panelProductDash.Dock = DockStyle.Fill;
            panelAddProduct.Visible = false;
            panelMainContent.Visible = false;
            panelStockStatistics.Visible = false;
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnStockStats_Click(object sender, EventArgs e)
        {
            PanelSpecificEmployeeStatistics.Visible = false;
            PanelChoosSpecificOrAllEmployees.Visible = false;
            panelStockStatistics.Visible = true;
            panelStockStatistics.Dock = DockStyle.Fill;
            panelProductDash.Visible = false;
            panelAddProduct.Visible = false;
            panelMainContent.Visible = false;
        }
        private void BtnEmployeeStatistics_Click(object sender, EventArgs e)
        {
            PanelChoosSpecificOrAllEmployees.Visible = true;
            PanelChoosSpecificOrAllEmployees.Dock = DockStyle.Fill;
            panelProductDash.Visible = false;
            panelAddProduct.Visible = false;
            panelMainContent.Visible = false;
            panelStockStatistics.Visible = false;
            PanelSpecificEmployeeStatistics.Visible = false;
            PanelAllEmployeeStatistics.Visible = false;
        }

        private void BtnSpecificEmployee_Click(object sender, EventArgs e)
        {
            PanelSpecificEmployeeStatistics.Visible = true;
            PanelSpecificEmployeeStatistics.Dock = DockStyle.Fill;
            PanelChoosSpecificOrAllEmployees.Visible = false;
            pnl_employeesStatistics.Visible = false;
        }

        private void BtnAllEmployees_Click(object sender, EventArgs e)
        {
            PanelAllEmployeeStatistics.Visible = true;
            PanelAllEmployeeStatistics.Dock = DockStyle.Fill;
            PanelChoosSpecificOrAllEmployees.Visible = false;
            pnl_employeesStatistics.Visible = false;
        }

        private void bt_employeesStatistics_Click(object sender, EventArgs e)
        {
            pnl_employeesStatistics.Visible = true;
            pnl_employeesStatistics.Dock = DockStyle.Fill;
            PanelChoosSpecificOrAllEmployees.Visible = false;
            PanelAllEmployeeStatistics.Visible = false;
        }
        #endregion

        #region ProductReq

        private void btnViewStockDescription_Click(object sender, EventArgs e)
        {
            if (requestView == false)
            {
                int selectedValue = Convert.ToInt32(listBoxProducts.SelectedValue);
                foreach (stock s in stocks)
                {
                    if (s.getId == selectedValue)
                    {
                        MessageBox.Show($"Name: {s.getName}{Environment.NewLine}Type:{s.getType}{Environment.NewLine}Description:{Environment.NewLine}{s.getDescription}{Environment.NewLine}{Environment.NewLine}");
                    }
                }
            }
            else
            {
                int selectedValue = Convert.ToInt32(listBoxProducts.SelectedValue);
                foreach (stock_request s in req_stocks)
                {
                    if (s.getId == selectedValue)
                    {
                        MessageBox.Show($"Name: {s.getName}{Environment.NewLine}Type:{s.getType}{Environment.NewLine}Description:{Environment.NewLine}{s.getDescription}{Environment.NewLine}Arrival Date: {s.Date}");
                    }
                }
            }
            
        }

        private void btnViewOpenRequests_Click(object sender, EventArgs e)
        {
            btnViewStockDescription.Visible = true;
            requestView = true;
            if (requestMode == false)
            {
                CleanListbox();
                DataAccessSR db = new DataAccessSR();
                req_stocks = db.GetOpenStockReq();

                UpdateBindStockRequest();
            }
            
        }

        private void btnViewPendingRequests_Click(object sender, EventArgs e)
        {
            btnViewStockDescription.Visible = true;
            requestView = true;
            if (requestMode == false)
            {
                CleanListbox();
                DataAccessSR db = new DataAccessSR();
                req_stocks = db.GetPendingStockReq();

                UpdateBindStockRequest();
            }
            
        }

        private void btnViewClosedRequests_Click(object sender, EventArgs e)
        {
            btnViewStockDescription.Visible = true;
            requestView = true;
            if (requestMode == false)
            {
                CleanListbox();
                DataAccessSR db = new DataAccessSR();
                req_stocks = db.GetClosedStockReq();

                UpdateBindStockRequest();
            }
            
        }
        private void btnViewAllProducts_Click(object sender, EventArgs e)
        {
            btnStockRefillRequest.Visible = true;
            btnStockModRequest.Visible = true;
            btnViewStockDescription.Visible = true;
            requestView = false;
            CleanListbox();
            DataAccess db = new DataAccess();
            stocks = db.GetStock();

            UpdateBindStock();
        }

        private void btnViewAvailableProducts_Click(object sender, EventArgs e)
        {
            btnStockRefillRequest.Visible = true;
            btnStockModRequest.Visible = true;
            btnViewStockDescription.Visible = true;
            requestView = false;
            CleanListbox();
            DataAccess db = new DataAccess();
            stocks = db.GetActiveStock();
            UpdateBindStock();
        }

        private void btnViewUnavailableProducts_Click(object sender, EventArgs e)
        {
            btnStockRefillRequest.Visible = false;
            btnStockModRequest.Visible = true;
            btnViewStockDescription.Visible = true;
            requestView = false;
            CleanListbox();
            DataAccess db = new DataAccess();
            stocks = db.GetInactiveStock();
            UpdateBindStock();
        }

        private void btnAddProductRequest_Click(object sender, EventArgs e)
        {
            panelProductDash.Visible = false;
            panelMainContent.Visible = false;
            panelAddProduct.Visible = true;
            panelAddProduct.Dock = DockStyle.Fill;


        }
        private void btnViewStock_Click(object sender, EventArgs e)
        {
            requestView = false;
            panelStockSelectView.Visible = true;
            panelStockRequestSelectView.Visible = false;
            btnAddProductRequest.Visible = true;
            btnStockModRequest.Visible = true;
            btnStockRefillRequest.Visible = true;
            btnViewStockDescription.Visible = false;
        }

        private void btnViewStockRequests_Click(object sender, EventArgs e)
        {
            requestView = true;
            panelStockSelectView.Visible = false;
            panelStockRequestSelectView.Visible = true;
            btnAddProductRequest.Visible = false;
            btnStockModRequest.Visible = false;
            btnStockRefillRequest.Visible = false;
            btnViewStockDescription.Visible = false;
           // btnViewStockDescription.Visible = false;
        }
        #endregion

        #region AddNewProductRequest
        private void btnBackFromAddProduct_Click(object sender, EventArgs e)
        {
            panelAddProduct.Visible = false;
            panelProductDash.Visible = true;
        }

        private void btnAddProductClearFields_Click(object sender, EventArgs e)
        {
            textBoxAddProductName.Text = "";
            textBoxAddProductDescription.Text = "";
            textBoxAddProductQuantity.Text = "";
            textBoxAddProductValue.Text = "";
            textBoxAddProductType.Text = "";
        }

        private void btnAddProductToDb_Click(object sender, EventArgs e)
        {
            panelAddProduct.Visible = false;
            panelProductDash.Visible = true;
            string getInputPName = textBoxAddProductName.Text;
            string getInputPQuantity = textBoxAddProductQuantity.Text;
            string getInputPDescription = textBoxAddProductDescription.Text;
            string getInputPPurchaseValue = textBoxAddProductValue.Text;
            string getInputPType = textBoxAddProductType.Text;
            if (string.IsNullOrEmpty(getInputPName) || string.IsNullOrEmpty(getInputPType) || string.IsNullOrEmpty(getInputPQuantity) || string.IsNullOrEmpty(getInputPDescription) || string.IsNullOrEmpty(getInputPPurchaseValue))
            {
                MessageBox.Show("All textboxs must be filled");
            }
            else
            {
                int number1 = 0;
                bool canConvertInt = int.TryParse(getInputPQuantity, out number1);
                if (canConvertInt == true && Convert.ToInt32(getInputPQuantity) != 0)
                {
                    float number2 = 0;
                    bool canConvertFloat = float.TryParse(getInputPPurchaseValue, out number2);
                    if (canConvertFloat == true && Convert.ToSingle(getInputPPurchaseValue) != 0 && Convert.ToSingle(getInputPPurchaseValue)>=1)
                    {
                        DataAccessSR db = new DataAccessSR();
                        db.AddRequestForNewStock(getInputPName, Convert.ToInt32(getInputPQuantity), getInputPDescription, Convert.ToSingle(getInputPPurchaseValue), getInputPType);
                    }
                    else
                    {
                        MessageBox.Show($"Purchase value is not valid.{Environment.NewLine}Try entering a valid value");
                    }

                }
                else
                {
                    MessageBox.Show($"Quantity is not vaild.{Environment.NewLine}Try entering a valid number");
                }
            }
        }




        #endregion
        #region StockRefill
        private void btnCloseStockRefill_Click(object sender, EventArgs e)
        {
            panelStockRefillRequest.Visible = false;
            requestMode = false;
        }

        private void btnSendStockRefill_Click(object sender, EventArgs e)
        {
            requestMode = false;
            int selectedValue = Convert.ToInt32(listBoxProducts.SelectedValue);
            string inputQuantity = textBoxStockRefill.Text;
            if (string.IsNullOrEmpty(inputQuantity))
            {
                MessageBox.Show("Field can not be empty");
            }
            else
            {
                int number1 = 0;
                bool canConvertInt = int.TryParse(inputQuantity, out number1);
                if (canConvertInt == true && Convert.ToInt32(inputQuantity) != 0)
                {
                    foreach (stock s in stocks)
                    {
                        DataAccessSR dbsr = new DataAccessSR();
                        DataAccess db = new DataAccess();
                        if (s.getId == selectedValue)
                        {


                            dbsr.AddRequestForStockRefill(s.getId, s.getName, Convert.ToInt32(inputQuantity),s.getDescription,s.getpurchaseValue,s.getState,s.getType);
                            stocks = db.GetActiveStock();
                            UpdateBindStock();

                        }

                    } 
                }
                else
                {
                    MessageBox.Show($"Quantity is not vaild.{Environment.NewLine}Try entering a valid number");
                }
            }
            panelStockRefillRequest.Visible = false;
        }

        private void btnStockRefillRequest_Click(object sender, EventArgs e)
        {
            panelStockRefillRequest.Visible = true;
            requestMode = true;
        }
        #endregion

        #region StockMod
        private void btnStockModRequest_Click(object sender, EventArgs e)
        {

            btnViewStock.Visible = false;
            btnViewStockRequests.Visible = false;
            btnViewStockDescription.Visible = false;
            panelStockSelectView.Visible = false;
            btnStockModRequest.Visible = false;
            btnAddProductRequest.Visible = false;
            btnStockRefillRequest.Visible = false;
            panelModifyProduct.Visible = true;
            int selectedValue = Convert.ToInt32(listBoxProducts.SelectedValue);
            foreach (stock s in stocks)
            {
                if (s.getId == selectedValue)
                {
                    textBoxModifyProductName.Text = s.getName;
                    //textBoxModifyProductQuantity.Text = s.getQuantity.ToString();
                    textBoxModifyProductDescription.Text = s.getDescription;
                    textBoxModifyProductValue.Text = s.getpurchaseValue.ToString();
                    textBoxModifyProductType.Text = s.getType;
                    if (s.getState == "active")
                    {
                        radioButtonActiveProduct.Checked = true;
                    }
                    if (s.getState == "inactive")
                    {
                        radioButtonInactiveProduct.Checked = true;
                    }
                }
            }
            //requestMode = true;
        }

        private void btnModifyProductToDb_Click(object sender, EventArgs e)
        {
           // requestMode = false;
            string getInputPName = textBoxModifyProductName.Text;
            string getInputPDescription = textBoxModifyProductDescription.Text;
            string getInputPPurchaseValue = textBoxModifyProductValue.Text;
            int selectedValue = Convert.ToInt32(listBoxProducts.SelectedValue);
            string getInputPType = textBoxModifyProductType.Text;
            string InputState = "";
            if (radioButtonActiveProduct.Checked == true)
            {
                radioButtonInactiveProduct.Checked = false;
                InputState = "active";
            }
            if (radioButtonInactiveProduct.Checked == true)
            {
                radioButtonActiveProduct.Checked = false;
                InputState = "inactive";
            }

            if (string.IsNullOrEmpty(getInputPName)|| string.IsNullOrEmpty(getInputPType) || string.IsNullOrEmpty(getInputPDescription) || string.IsNullOrEmpty(getInputPPurchaseValue))
            {
                MessageBox.Show("All textboxs must be filled");
            }
            else
            {

                    float number2 = 0;
                    bool canConvertFloat = float.TryParse(getInputPPurchaseValue, out number2);
                    if (canConvertFloat == true && Convert.ToSingle(getInputPPurchaseValue) != 0 && Convert.ToSingle(getInputPPurchaseValue) >= 1)
                    {

                        DataAccessSR db = new DataAccessSR();
                        db.AddRequestForStockModif(selectedValue, getInputPName, getInputPDescription, Convert.ToSingle(getInputPPurchaseValue), InputState, getInputPType);

                    }
                    else
                    {
                        MessageBox.Show($"Purchase value is not valid.{Environment.NewLine}Try entering a valid number");
                    }

                
                
                panelModifyProduct.Visible = false;
                btnAddProductRequest.Visible = true;
                btnStockRefillRequest.Visible = true;
                btnStockModRequest.Visible = true;
                btnViewStock.Visible = true;
                btnViewStockDescription.Visible = true;
                btnViewStockRequests.Visible = true;
            }
        }

        private void btnClearModifyProductFields_Click(object sender, EventArgs e)
        {
            textBoxModifyProductName.Text = "";
            textBoxModifyProductDescription.Text = "";
            //textBoxModifyProductQuantity.Text = "";
            textBoxModifyProductValue.Text = "";
            textBoxModifyProductType.Text = "";
        }

        private void btnBackFromModifyProduct_Click(object sender, EventArgs e)
        {
            //requestMode = false;
            btnAddProductRequest.Visible = true;
            btnStockRefillRequest.Visible = true;
            panelModifyProduct.Visible = false;
            btnStockModRequest.Visible = true;
            btnViewStockDescription.Visible = true;
            btnViewStock.Visible = true;
            btnViewStockRequests.Visible = true;
        }
        #endregion

        #region ProductStats
        private void btnShowMonthly_Click(object sender, EventArgs e)
        {
            panelMonthlyStock.Visible = true;
        }

        private void btnCloseMonthly_Click(object sender, EventArgs e)
        {
            panelMonthlyStock.Visible = false;
        }

        private void btnRefreshTotalStock_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();
            int number = db.CountTotalStocks();
            labelTotalStock.Text = $"Total Items In Stock: {number}";
        }

        private void btnRefreshStockActivePerc_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();
            int totalStockNumber = db.CountTotalStocks();
            int totalActiveStockNumber = db.CountTotalActiveStocks();
            double result = (Convert.ToDouble(totalActiveStockNumber) / Convert.ToDouble(totalStockNumber)) * 100;
            labelTotalActiveStockPerc.Text = $"Active Items: {result.ToString("F")}%";
        }

        private void btnRefreshStockInactivePerc_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();
            int totalStockNumber = db.CountTotalStocks();
            int totalInActiveStockNumber = db.CountTotalInactiveStocks();
            double result = (Convert.ToDouble(totalInActiveStockNumber) / Convert.ToDouble(totalStockNumber)) * 100;
            labelTotalInactiveStockPerc.Text = $"Inactive Items: {result.ToString("F")}%";
        }

        private void btnTotalnactiveStockRefresh_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();
            int number = db.CountTotalInactiveStocks();
            labelTotalInactiveInStock.Text = $"Total Inactive Items In Stock: {number}";
        }

        private void btnTotalActiveStockRefresh_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();
            int number = db.CountTotalActiveStocks();
            labelActiveItemsInStock.Text = $"Total Active Items In Stock: {number}";

            
        }

        private void btnViewActiveStockStats_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();
            stocks_stats = db.GetActiveStock();
            UpdateBindStockStatistics();
        }

        private void btnViewInactiveStockStats_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();
            stocks_stats = db.GetInactiveStock();
            UpdateBindStockStatistics();
        }

        private void btnSortStockByType_Click(object sender, EventArgs e)
        {
            string inputType = textBoxSortType.Text;
            if (string.IsNullOrEmpty(inputType))
            {
                MessageBox.Show("The field for the item type cannot be empty");
            }
            else
            {
                CleanListboxStats();
                DataAccess db = new DataAccess();
                stocks_stats = db.GetStockBasedOnType(inputType);
                UpdateBindStockStatistics();
            }
            
        }

        private void btnSortStockQty_Click(object sender, EventArgs e)
        {
            CleanListboxStats();
            SortByQty sortListByQty = new SortByQty();
            stocks_stats.Sort(sortListByQty);
            stocks_stats.Reverse();
            UpdateBindStockStatistics();
        }

        private void btnPreviewSelectedStock_Click(object sender, EventArgs e)
        {
            int selectedValue = Convert.ToInt32(listBoxStockStats.SelectedValue);
            foreach (stock s in stocks_stats)
            {
                if (s.getId == selectedValue)
                {
                    textBoxPreviewStockName.Text = s.getName;
                    textBoxPreviewStockQty.Text = s.getQuantity.ToString();
                    textBoxPreviewStockPrice.Text = s.getpurchaseValue.ToString();
                    textBoxPreviewStockType.Text = s.getType;
                }
            }
        }

        private void btnRefreshMonthly_Click(object sender, EventArgs e)
        {
            int selectedValue = Convert.ToInt32(listBoxStockStats.SelectedValue);

            foreach (stock s in stocks_stats)
            {
                if (s.getId == selectedValue)
                {
                    DataAccessSR dbSR = new DataAccessSR();
                    int number = dbSR.GetNrStockRequestMonthly(selectedValue);
                    labelTotalRefillRequests.Text = $"Total Refill Stock Requests: {number}";

                    CleanListboxStatsRefill();
                    req_stocks_ref = dbSR.GetStockRefillReq(selectedValue);
                    UpdateBindStockStatisticsRefill();
                }
            }
        }



        #endregion

        #region SpecificEmployeeStatistics
        DataAccesEmployee accesEmployee = new DataAccesEmployee();
        MySqlConnection conn = new MySqlConnection("server=localhost;database=media_bazzar;uid=root;password=;");
        string employeeName;

        double totalIndividualPresent = 0;
        double totalIndividualAbsent = 0;
        double totalIndividualSick = 0;
        double totalIndividualShifts = 0;
        private void BtnViewSpecificEmployeeStatistics_Click(object sender, EventArgs e)
        {
            employeeName = textBox1.Text;

            #region Present
            string queryCountMPresent = "SELECT COUNT(*) FROM `shifts` WHERE mPresent = 'yes' AND employeeName = '" + employeeName + "';";
            string queryCountMdPresent = "SELECT COUNT(*) FROM `shifts` WHERE mdPresent = 'yes' AND employeeName = '" + employeeName + "';";
            string queryCountNPresent = "SELECT COUNT(*) FROM `shifts` WHERE nPresent = 'yes' AND employeeName = '" + employeeName + "';";
            List<string> countPresentQueryList = new List<string>();
            countPresentQueryList.Add(queryCountMPresent);
            countPresentQueryList.Add(queryCountMdPresent);
            countPresentQueryList.Add(queryCountNPresent);
            for (int i = 0; i < 3; i++)
            {
                double count = accesEmployee.GetEmployeePresentAbsentSick(countPresentQueryList[i]);
                totalIndividualPresent += count;
            }
            #endregion

            #region Shifts
            string queryCountMShifts = "SELECT COUNT(*) FROM `shifts` WHERE morningShift = 'yes' AND employeeName = '" + employeeName + "';";
            string queryCountMdShifts = "SELECT COUNT(*) FROM `shifts` WHERE middayShift = 'yes' AND employeeName = '" + employeeName + "';";
            string queryCountNShifts = "SELECT COUNT(*) FROM `shifts` WHERE nightShift = 'yes' AND employeeName = '" + employeeName + "';";
            List<string> countShiftQueryList = new List<string>();
            countShiftQueryList.Add(queryCountMShifts);
            countShiftQueryList.Add(queryCountMdShifts);
            countShiftQueryList.Add(queryCountNShifts);
            for (int i = 0; i < 3; i++)
            {
                double count2 = accesEmployee.GetEmployeePresentAbsentSick(countShiftQueryList[i]);
                totalIndividualShifts += count2;
            }
            #endregion

            #region Absent
            string queryCountMAbsent = "SELECT COUNT(*) FROM `shifts` WHERE mAbsent = 'yes' AND employeeName = '" + employeeName + "';";
            string queryCountMdAbsent = "SELECT COUNT(*) FROM `shifts` WHERE mdAbsent = 'yes' AND employeeName = '" + employeeName + "';";
            string queryCountNAbsent = "SELECT COUNT(*) FROM `shifts` WHERE nAbsent = 'yes' AND employeeName = '" + employeeName + "';";
            List<string> countAbsentQueryList = new List<string>();
            countAbsentQueryList.Add(queryCountMAbsent);
            countAbsentQueryList.Add(queryCountMdAbsent);
            countAbsentQueryList.Add(queryCountNAbsent);
            for (int i = 0; i < 3; i++)
            {
                double count3 = accesEmployee.GetEmployeePresentAbsentSick(countAbsentQueryList[i]);
                totalIndividualAbsent += count3;
            }
            #endregion

            #region Sick
            string queryCountMSick = "SELECT COUNT(*) FROM `shifts` WHERE mSick = 'yes' AND employeeName = '" + employeeName + "';";
            string queryCountMdSick = "SELECT COUNT(*) FROM `shifts` WHERE mdSick = 'yes' AND employeeName = '" + employeeName + "';";
            string queryCountNSick = "SELECT COUNT(*) FROM `shifts` WHERE nSick = 'yes' AND employeeName = '" + employeeName + "';";
            List<string> countSickQueryList = new List<string>();
            countSickQueryList.Add(queryCountMSick);
            countSickQueryList.Add(queryCountMdSick);
            countSickQueryList.Add(queryCountNSick);
            for (int i = 0; i < 3; i++)
            {
                double count4 = accesEmployee.GetEmployeePresentAbsentSick(countSickQueryList[i]);
                totalIndividualSick += count4;
            }
            #endregion

            #region GetLatest/FirstShift

            string queryGetFirstShiftDate = "SELECT MIN(date) FROM shifts WHERE employeeName = '" + employeeName + "';";
            string queryGetLatestShiftDate = "SELECT MAX(date) FROM shifts WHERE employeeName = '" + employeeName + "';";

            string firstShiftDate = accesEmployee.GetFirstLastShiftDate(queryGetFirstShiftDate);
            string latestShiftDate = accesEmployee.GetFirstLastShiftDate(queryGetLatestShiftDate);

            #endregion

            #region Labels
            string presentPercentage = "%" + ((totalIndividualPresent * 100) / totalIndividualShifts).ToString("0");
            string absentPercentage = "%" + ((totalIndividualAbsent * 100) / totalIndividualShifts).ToString("0");
            string sickPercentage = "%" + ((totalIndividualSick * 100) / totalIndividualShifts).ToString("0");

            LblPresentPercentage.Text = presentPercentage;
            LblAbsentPercentage.Text = absentPercentage;
            LblSickPercentage.Text = sickPercentage;

            LblPresent.Text = totalIndividualPresent.ToString();
            LblAbsent.Text = totalIndividualAbsent.ToString();
            LblSick.Text = totalIndividualSick.ToString();
            LblTotalShifts.Text = totalIndividualShifts.ToString();

            LblFirstShiftDate.Text = firstShiftDate;
            LblLatestShiftDate.Text = latestShiftDate;
            #endregion

            totalIndividualAbsent = 0;
            totalIndividualPresent = 0;
            totalIndividualShifts = 0;
            totalIndividualSick = 0;
        }
        #endregion

        #region AllEmployeeStatistics
        double totalAllPresent = 0;
        double totalAllAbsent = 0;
        double totalAllSick = 0;
        double totalAllShifts = 0;

        private void BtnStatisticsAllEmployees_Click(object sender, EventArgs e)
        {
            #region Present
            string queryCountMPresent = "SELECT COUNT(*) FROM `shifts` WHERE mPresent = 'yes';";
            string queryCountMdPresent = "SELECT COUNT(*) FROM `shifts` WHERE mdPresent = 'yes';";
            string queryCountNPresent = "SELECT COUNT(*) FROM `shifts` WHERE nPresent = 'yes';";
            List<string> countPresentQueryList = new List<string>();
            countPresentQueryList.Add(queryCountMPresent);
            countPresentQueryList.Add(queryCountMdPresent);
            countPresentQueryList.Add(queryCountNPresent);
            for (int i = 0; i < 3; i++)
            {
                double count = accesEmployee.GetEmployeePresentAbsentSick(countPresentQueryList[i]);
                totalAllPresent += count;
            }
            #endregion

            #region Shifts
            string queryCountMShifts = "SELECT COUNT(*) FROM `shifts` WHERE morningShift = 'yes';";
            string queryCountMdShifts = "SELECT COUNT(*) FROM `shifts` WHERE middayShift = 'yes';";
            string queryCountNShifts = "SELECT COUNT(*) FROM `shifts` WHERE nightShift = 'yes';";
            List<string> countShiftQueryList = new List<string>();
            countShiftQueryList.Add(queryCountMShifts);
            countShiftQueryList.Add(queryCountMdShifts);
            countShiftQueryList.Add(queryCountNShifts);
            for (int i = 0; i < 3; i++)
            {
                double count2 = accesEmployee.GetEmployeePresentAbsentSick(countShiftQueryList[i]);
                totalAllShifts += count2;
            }
            #endregion

            #region Absent
            string queryCountMAbsent = "SELECT COUNT(*) FROM `shifts` WHERE mAbsent = 'yes';";
            string queryCountMdAbsent = "SELECT COUNT(*) FROM `shifts` WHERE mdAbsent = 'yes';";
            string queryCountNAbsent = "SELECT COUNT(*) FROM `shifts` WHERE nAbsent = 'yes';";
            List<string> countAbsentQueryList = new List<string>();
            countAbsentQueryList.Add(queryCountMAbsent);
            countAbsentQueryList.Add(queryCountMdAbsent);
            countAbsentQueryList.Add(queryCountNAbsent);
            for (int i = 0; i < 3; i++)
            {
                double count3 = accesEmployee.GetEmployeePresentAbsentSick(countAbsentQueryList[i]);
                totalAllAbsent += count3;
            }
            #endregion

            #region Sick
            string queryCountMSick = "SELECT COUNT(*) FROM `shifts` WHERE mSick = 'yes';";
            string queryCountMdSick = "SELECT COUNT(*) FROM `shifts` WHERE mdSick = 'yes';";
            string queryCountNSick = "SELECT COUNT(*) FROM `shifts` WHERE nSick = 'yes';";
            List<string> countSickQueryList = new List<string>();
            countSickQueryList.Add(queryCountMSick);
            countSickQueryList.Add(queryCountMdSick);
            countSickQueryList.Add(queryCountNSick);
            for (int i = 0; i < 3; i++)
            {
                double count4 = accesEmployee.GetEmployeePresentAbsentSick(countSickQueryList[i]);
                totalAllSick += count4;
            }
            #endregion

            #region Labels
            string presentPercentage = "%" + ((totalAllPresent * 100) / totalAllShifts).ToString("0");
            string absentPercentage = "%" + ((totalAllAbsent * 100) / totalAllShifts).ToString("0");
            string sickPercentage = "%" + ((totalAllSick * 100) / totalAllShifts).ToString("0");

            LblPresentPercentageAllEmployees.Text = presentPercentage;
            LblAbsentPercentageAllEmployees.Text = absentPercentage;
            LblSickPercentageAllEmployees.Text = sickPercentage;

            LblPresentAllEmployees.Text = totalAllPresent.ToString();
            LblAbsentAllEmployees.Text = totalAllAbsent.ToString();
            LblSickAllEmployees.Text = totalAllSick.ToString();
            LblTotalShiftsAllEmployees.Text = totalAllShifts.ToString();
            #endregion

            totalAllPresent = 0;
            totalAllAbsent = 0;
            totalAllSick = 0;
            totalAllShifts = 0;
        }
        #endregion

       
        #region Employees

        private void bt_totalEmployees_Click(object sender, EventArgs e)
        {
            lb_info.Items.Clear();
            User user;
            user = new User();
            // string all = user.checkAllUsers().ToString();
            lb_info.Items.Add("There is " + user.checkAllUsers() + " in total users");
        }

        private void bt_totalActive_Click(object sender, EventArgs e)
        {
            lb_info.Items.Clear();
            User user;
            user = new User();
            // string all = user.checkAllUsers().ToString();
            lb_info.Items.Add("There is " + user.checkActiveUsers() + " in total active users");

        }

        private void bt_totalInactive_Click(object sender, EventArgs e)
        {
            lb_info.Items.Clear();
            User user;
            user = new User();
            // string all = user.checkAllUsers().ToString();
            lb_info.Items.Add("There is " + user.checkInactiveUsers() + " in total inactive users");
        }
        #endregion
        #region EmployeeType
        private void bt_allAdministration_Click(object sender, EventArgs e)
        {
            lb_info.Items.Clear();
            User user;
            user = new User();
            // string all = user.checkAllUsers().ToString();
            lb_info.Items.Add("There is " + user.checkAllAdministrators() + " administrator employees");

        }

        private void bt_allManager_Click(object sender, EventArgs e)
        {
            lb_info.Items.Clear();
            User user;
            user = new User();
            // string all = user.checkAllUsers().ToString();
            lb_info.Items.Add("There is " + user.checkAllManagers() + " manager employees");
        }

        private void bt_allDepo_Click(object sender, EventArgs e)
        {
            lb_info.Items.Clear();
            User user;
            user = new User();
            // string all = user.checkAllUsers().ToString();
            lb_info.Items.Add("There is " + user.checkAllDepo() + " depo employees");
        }
        #endregion
    }
}
