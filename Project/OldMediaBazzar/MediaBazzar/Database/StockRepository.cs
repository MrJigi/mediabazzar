﻿using MediaBazzar.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBazzar.Database
{
    public class StockRepository : BaseRepository<Stock>
    {
        private readonly string table = "Stocks";
        public StockRepository(string connectionString)
            : base(connectionString)
        {
        }

        public override IEnumerable<Stock> GetAll()
        {
            using (var command = new MySqlCommand("SELECT * FROM " + table))
            {
                return GetAll(command);
            }
        }
        public override Stock GetById(string id)
        {
            using (var command = new MySqlCommand("SELECT * FROM " + table + " WHERE Id=@id"))
            {
                command.Parameters.AddWithValue("@id", id);
                return GetById(command);
            }
        }

        public override void Add(Stock record)
        {
            using (var command = new MySqlCommand("INSERT INTO " + table + " (name, description, count) " +
                "VALUES(@name, @description, @count)"))
            {
                Fill(command, record);
                Save(command);
            }
        }

        public override void Update(Stock record)
        {
            using (var command = new MySqlCommand("UPDATE " + table + " SET name=@name, " +
                "description=@description, count=@count)" +
                "WHERE Id=@id"))
            {
                Fill(command, record);
                command.Parameters.AddWithValue("@id", record.Id);
                Save(command);
            }
        }

        public override void Delete(string id)
        {
            Delete(table, id);
        }

        public override Stock Populate(MySqlDataReader reader)
        {
            return new Stock
            {
                Id = reader.GetInt32(0),
                Name = reader.GetString(1),
                Description = reader.GetString(2),
                Count = reader.GetInt32(3)
            };
        }

        public override void Fill(MySqlCommand command, Stock record)
        {
            command.Parameters.AddWithValue("@name", record.Name);
            command.Parameters.AddWithValue("@description", record.Description);
            command.Parameters.AddWithValue("@count", record.Count);
        }


    }
}
