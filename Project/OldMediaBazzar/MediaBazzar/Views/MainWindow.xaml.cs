﻿using MediaBazzar.Database;
using MediaBazzar.Models;
using MediaBazzar.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MediaBazzar
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BaseRepository<User> userRepo;
        User loggedUser;
        Depo depoWindow;
        AdministrationWindow adminWindow;
        FlManagerWindows flManagerWindow;
        UpManagementWindown upManagementWindow;
        public MainWindow()
        {
            InitializeComponent();
            userRepo = new UserRepository(ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
            depoWindow = new Depo();
            adminWindow = new AdministrationWindow();
            flManagerWindow = new FlManagerWindows();
            upManagementWindow = new UpManagementWindown();
            //windows.Show();
            // Hide();
        }

        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            loggedUser = userRepo.GetById(UsernameTbx.Text);
            if(loggedUser.PasswordHash == PasswordTbx.Text)
            {
                switch(loggedUser.CurrPosition)
                {
                    case Position.HR:
                        adminWindow.Show();
                        Close();
                        break;
                    case Position.UpperManagment:
                        upManagementWindow.Show();
                        Close();
                        break;
                    case Position.DepoEmployee:
                        depoWindow.Show();
                        Close();
                        break;
                    case Position.FloorManager:
                        flManagerWindow.Show();
                        Close();
                        break;
                    default:
                        MessageBox.Show("Position error");
                        break;
                }
            }
            else
                loggedUser = null;
            
        }
    }
}
