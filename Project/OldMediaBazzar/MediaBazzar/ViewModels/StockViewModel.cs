﻿using MediaBazzar.Database;
using MediaBazzar.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MediaBazzar.ViewModels
{
    class StockViewModel : ViewModelBase
    {
        // THIS WHOLE STOCKS LIST VIEW SHOULD BE ITS OWN USER CONTROL
        // NO TIME :(
        public ICommand RefreshCommand { get; }
        public ICommand RefillCommand { get; }
        public Stock SelectedStock { get; set; }
        private BaseRepository<Stock> _stockRepo;
        private BaseRepository<StockRefill> _stockRefillRepo;
        private ObservableCollection<Stock> _stocks;
        public ObservableCollection<Stock> Stocks
        {
            get => _stocks;
            set
            {
                _stocks = value;
                NotifyPropertyChanged("Stocks");
            }
        }


        public StockViewModel()
        {
            SelectedStock = new Stock();
            RefreshCommand = new RelayCommand(param => RefreshList());
            RefillCommand = new RelayCommand(param => RefillRequest());
            _stockRepo = new StockRepository(ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
            _stockRefillRepo = new StockRefillRepository(ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
        }

        private void RefillRequest()
        {
            StockRefill stockRefill = new StockRefill();
            stockRefill.ItemId = SelectedStock.Id;
            _stockRefillRepo.Add(stockRefill);
        }

        private void RefreshList()
        {
            Stocks = new ObservableCollection<Stock>(_stockRepo.GetAll());
        }
    }
}
