﻿namespace Media_Bazaar
{
    partial class Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TbEmployeeName = new System.Windows.Forms.TextBox();
            this.LblSearchEmployee = new System.Windows.Forms.Label();
            this.LblId = new System.Windows.Forms.Label();
            this.LblName = new System.Windows.Forms.Label();
            this.LblFamilyName = new System.Windows.Forms.Label();
            this.LblDateOfBirth = new System.Windows.Forms.Label();
            this.TbId = new System.Windows.Forms.TextBox();
            this.TbName = new System.Windows.Forms.TextBox();
            this.TbFamilyName = new System.Windows.Forms.TextBox();
            this.TbDateOfBirth = new System.Windows.Forms.TextBox();
            this.CbToSearch = new System.Windows.Forms.ComboBox();
            this.LblBirthplace = new System.Windows.Forms.Label();
            this.TbBirthplace = new System.Windows.Forms.TextBox();
            this.LblNationality = new System.Windows.Forms.Label();
            this.LblLanguages = new System.Windows.Forms.Label();
            this.LblBsn = new System.Windows.Forms.Label();
            this.TbNationality = new System.Windows.Forms.TextBox();
            this.TbLanguages = new System.Windows.Forms.TextBox();
            this.TbBsn = new System.Windows.Forms.TextBox();
            this.LblHomeAddress = new System.Windows.Forms.Label();
            this.LblZipCode = new System.Windows.Forms.Label();
            this.TbHomeAddress = new System.Windows.Forms.TextBox();
            this.TbZipCode = new System.Windows.Forms.TextBox();
            this.BtnSearch = new System.Windows.Forms.Button();
            this.BtnSelectEmployee = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TbEmployeeName
            // 
            this.TbEmployeeName.Location = new System.Drawing.Point(346, 70);
            this.TbEmployeeName.Name = "TbEmployeeName";
            this.TbEmployeeName.Size = new System.Drawing.Size(100, 22);
            this.TbEmployeeName.TabIndex = 1;
            // 
            // LblSearchEmployee
            // 
            this.LblSearchEmployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSearchEmployee.Location = new System.Drawing.Point(53, 68);
            this.LblSearchEmployee.Name = "LblSearchEmployee";
            this.LblSearchEmployee.Size = new System.Drawing.Size(292, 23);
            this.LblSearchEmployee.TabIndex = 0;
            this.LblSearchEmployee.Text = "Employee Name:";
            // 
            // LblId
            // 
            this.LblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblId.Location = new System.Drawing.Point(53, 99);
            this.LblId.Name = "LblId";
            this.LblId.Size = new System.Drawing.Size(160, 23);
            this.LblId.TabIndex = 2;
            this.LblId.Text = "ID:";
            // 
            // LblName
            // 
            this.LblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblName.Location = new System.Drawing.Point(53, 130);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(160, 23);
            this.LblName.TabIndex = 3;
            this.LblName.Text = "Name:";
            // 
            // LblFamilyName
            // 
            this.LblFamilyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFamilyName.Location = new System.Drawing.Point(53, 161);
            this.LblFamilyName.Name = "LblFamilyName";
            this.LblFamilyName.Size = new System.Drawing.Size(160, 23);
            this.LblFamilyName.TabIndex = 4;
            this.LblFamilyName.Text = "Family Name:";
            // 
            // LblDateOfBirth
            // 
            this.LblDateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDateOfBirth.Location = new System.Drawing.Point(53, 192);
            this.LblDateOfBirth.Name = "LblDateOfBirth";
            this.LblDateOfBirth.Size = new System.Drawing.Size(160, 23);
            this.LblDateOfBirth.TabIndex = 5;
            this.LblDateOfBirth.Text = "Date Of Birth:";
            // 
            // TbId
            // 
            this.TbId.Location = new System.Drawing.Point(240, 101);
            this.TbId.Name = "TbId";
            this.TbId.Size = new System.Drawing.Size(100, 22);
            this.TbId.TabIndex = 6;
            // 
            // TbName
            // 
            this.TbName.Location = new System.Drawing.Point(240, 132);
            this.TbName.Name = "TbName";
            this.TbName.Size = new System.Drawing.Size(100, 22);
            this.TbName.TabIndex = 7;
            // 
            // TbFamilyName
            // 
            this.TbFamilyName.Location = new System.Drawing.Point(240, 163);
            this.TbFamilyName.Name = "TbFamilyName";
            this.TbFamilyName.Size = new System.Drawing.Size(100, 22);
            this.TbFamilyName.TabIndex = 8;
            // 
            // TbDateOfBirth
            // 
            this.TbDateOfBirth.Location = new System.Drawing.Point(240, 194);
            this.TbDateOfBirth.Name = "TbDateOfBirth";
            this.TbDateOfBirth.Size = new System.Drawing.Size(100, 22);
            this.TbDateOfBirth.TabIndex = 9;
            // 
            // CbToSearch
            // 
            this.CbToSearch.FormattingEnabled = true;
            this.CbToSearch.Items.AddRange(new object[] {
            "FirstName",
            "LastName"});
            this.CbToSearch.Location = new System.Drawing.Point(219, 70);
            this.CbToSearch.Name = "CbToSearch";
            this.CbToSearch.Size = new System.Drawing.Size(121, 24);
            this.CbToSearch.TabIndex = 10;
            // 
            // LblBirthplace
            // 
            this.LblBirthplace.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBirthplace.Location = new System.Drawing.Point(53, 223);
            this.LblBirthplace.Name = "LblBirthplace";
            this.LblBirthplace.Size = new System.Drawing.Size(160, 23);
            this.LblBirthplace.TabIndex = 11;
            this.LblBirthplace.Text = "Birthplace:";
            // 
            // TbBirthplace
            // 
            this.TbBirthplace.Location = new System.Drawing.Point(240, 225);
            this.TbBirthplace.Name = "TbBirthplace";
            this.TbBirthplace.Size = new System.Drawing.Size(100, 22);
            this.TbBirthplace.TabIndex = 12;
            // 
            // LblNationality
            // 
            this.LblNationality.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNationality.Location = new System.Drawing.Point(53, 254);
            this.LblNationality.Name = "LblNationality";
            this.LblNationality.Size = new System.Drawing.Size(160, 23);
            this.LblNationality.TabIndex = 13;
            this.LblNationality.Text = "Nationality:";
            // 
            // LblLanguages
            // 
            this.LblLanguages.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLanguages.Location = new System.Drawing.Point(53, 285);
            this.LblLanguages.Name = "LblLanguages";
            this.LblLanguages.Size = new System.Drawing.Size(160, 31);
            this.LblLanguages.TabIndex = 14;
            this.LblLanguages.Text = "Languages";
            // 
            // LblBsn
            // 
            this.LblBsn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBsn.Location = new System.Drawing.Point(53, 316);
            this.LblBsn.Name = "LblBsn";
            this.LblBsn.Size = new System.Drawing.Size(160, 23);
            this.LblBsn.TabIndex = 15;
            this.LblBsn.Text = "BSN:";
            // 
            // TbNationality
            // 
            this.TbNationality.Location = new System.Drawing.Point(240, 256);
            this.TbNationality.Name = "TbNationality";
            this.TbNationality.Size = new System.Drawing.Size(100, 22);
            this.TbNationality.TabIndex = 16;
            // 
            // TbLanguages
            // 
            this.TbLanguages.Location = new System.Drawing.Point(240, 287);
            this.TbLanguages.Name = "TbLanguages";
            this.TbLanguages.Size = new System.Drawing.Size(100, 22);
            this.TbLanguages.TabIndex = 17;
            // 
            // TbBsn
            // 
            this.TbBsn.Location = new System.Drawing.Point(240, 318);
            this.TbBsn.Name = "TbBsn";
            this.TbBsn.Size = new System.Drawing.Size(100, 22);
            this.TbBsn.TabIndex = 18;
            // 
            // LblHomeAddress
            // 
            this.LblHomeAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblHomeAddress.Location = new System.Drawing.Point(53, 347);
            this.LblHomeAddress.Name = "LblHomeAddress";
            this.LblHomeAddress.Size = new System.Drawing.Size(160, 23);
            this.LblHomeAddress.TabIndex = 19;
            this.LblHomeAddress.Text = "Home Address:";
            // 
            // LblZipCode
            // 
            this.LblZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblZipCode.Location = new System.Drawing.Point(53, 378);
            this.LblZipCode.Name = "LblZipCode";
            this.LblZipCode.Size = new System.Drawing.Size(160, 23);
            this.LblZipCode.TabIndex = 20;
            this.LblZipCode.Text = "Zip-code:";
            // 
            // TbHomeAddress
            // 
            this.TbHomeAddress.Location = new System.Drawing.Point(240, 349);
            this.TbHomeAddress.Name = "TbHomeAddress";
            this.TbHomeAddress.Size = new System.Drawing.Size(100, 22);
            this.TbHomeAddress.TabIndex = 21;
            // 
            // TbZipCode
            // 
            this.TbZipCode.Location = new System.Drawing.Point(240, 380);
            this.TbZipCode.Name = "TbZipCode";
            this.TbZipCode.Size = new System.Drawing.Size(100, 22);
            this.TbZipCode.TabIndex = 22;
            // 
            // BtnSearch
            // 
            this.BtnSearch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSearch.ForeColor = System.Drawing.Color.LightGray;
            this.BtnSearch.Location = new System.Drawing.Point(461, 56);
            this.BtnSearch.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSearch.Name = "BtnSearch";
            this.BtnSearch.Size = new System.Drawing.Size(109, 51);
            this.BtnSearch.TabIndex = 45;
            this.BtnSearch.Text = "Search Employee";
            this.BtnSearch.UseVisualStyleBackColor = false;
            this.BtnSearch.Click += new System.EventHandler(this.BtnSearch_Click);
            // 
            // BtnSelectEmployee
            // 
            this.BtnSelectEmployee.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnSelectEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSelectEmployee.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSelectEmployee.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSelectEmployee.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnSelectEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSelectEmployee.ForeColor = System.Drawing.Color.LightGray;
            this.BtnSelectEmployee.Location = new System.Drawing.Point(461, 351);
            this.BtnSelectEmployee.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSelectEmployee.Name = "BtnSelectEmployee";
            this.BtnSelectEmployee.Size = new System.Drawing.Size(109, 51);
            this.BtnSelectEmployee.TabIndex = 46;
            this.BtnSelectEmployee.Text = "Select this employee";
            this.BtnSelectEmployee.UseVisualStyleBackColor = false;
            this.BtnSelectEmployee.Click += new System.EventHandler(this.BtnSelectEmployee_Click);
            // 
            // Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 454);
            this.Controls.Add(this.BtnSelectEmployee);
            this.Controls.Add(this.BtnSearch);
            this.Controls.Add(this.TbZipCode);
            this.Controls.Add(this.TbHomeAddress);
            this.Controls.Add(this.LblZipCode);
            this.Controls.Add(this.LblHomeAddress);
            this.Controls.Add(this.TbBsn);
            this.Controls.Add(this.TbLanguages);
            this.Controls.Add(this.TbNationality);
            this.Controls.Add(this.LblBsn);
            this.Controls.Add(this.LblLanguages);
            this.Controls.Add(this.LblNationality);
            this.Controls.Add(this.TbBirthplace);
            this.Controls.Add(this.LblBirthplace);
            this.Controls.Add(this.CbToSearch);
            this.Controls.Add(this.TbDateOfBirth);
            this.Controls.Add(this.TbFamilyName);
            this.Controls.Add(this.TbName);
            this.Controls.Add(this.TbId);
            this.Controls.Add(this.LblDateOfBirth);
            this.Controls.Add(this.LblFamilyName);
            this.Controls.Add(this.LblName);
            this.Controls.Add(this.LblId);
            this.Controls.Add(this.LblSearchEmployee);
            this.Controls.Add(this.TbEmployeeName);
            this.Name = "Search";
            this.Text = "Search";
            this.Load += new System.EventHandler(this.Search_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox TbEmployeeName;
        private System.Windows.Forms.Label LblSearchEmployee;
        private System.Windows.Forms.Label LblId;
        private System.Windows.Forms.Label LblName;
        private System.Windows.Forms.Label LblFamilyName;
        private System.Windows.Forms.Label LblDateOfBirth;
        private System.Windows.Forms.TextBox TbId;
        private System.Windows.Forms.TextBox TbName;
        private System.Windows.Forms.TextBox TbFamilyName;
        private System.Windows.Forms.TextBox TbDateOfBirth;
        private System.Windows.Forms.ComboBox CbToSearch;
        private System.Windows.Forms.Label LblBirthplace;
        private System.Windows.Forms.TextBox TbBirthplace;
        private System.Windows.Forms.Label LblNationality;
        private System.Windows.Forms.Label LblLanguages;
        private System.Windows.Forms.Label LblBsn;
        private System.Windows.Forms.TextBox TbNationality;
        private System.Windows.Forms.TextBox TbLanguages;
        private System.Windows.Forms.TextBox TbBsn;
        private System.Windows.Forms.Label LblHomeAddress;
        private System.Windows.Forms.Label LblZipCode;
        private System.Windows.Forms.TextBox TbHomeAddress;
        private System.Windows.Forms.TextBox TbZipCode;
        private System.Windows.Forms.Button BtnSearch;
        private System.Windows.Forms.Button BtnSelectEmployee;
    }
}