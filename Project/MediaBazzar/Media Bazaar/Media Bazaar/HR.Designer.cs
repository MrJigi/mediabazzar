﻿namespace Media_Bazaar
{
    partial class HR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.bt_editEmployee = new System.Windows.Forms.Button();
            this.bt_addEmployee = new System.Windows.Forms.Button();
            this.btnWorkShifts = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelLogo = new System.Windows.Forms.Label();
            this.panelMainContent = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pnl_createEmployee = new System.Windows.Forms.Panel();
            this.tb_createPassword = new System.Windows.Forms.TextBox();
            this.lb_password = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_createUsername = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lb_username = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lb_createRank = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cb_createRank = new System.Windows.Forms.ComboBox();
            this.tb_id = new System.Windows.Forms.TextBox();
            this.dp_birthDay = new System.Windows.Forms.DateTimePicker();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.bt_addUser = new System.Windows.Forms.Button();
            this.tb_lastName = new System.Windows.Forms.TextBox();
            this.tb_zipCode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_adress = new System.Windows.Forms.TextBox();
            this.tb_birthplace = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_bsn = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_languages = new System.Windows.Forms.TextBox();
            this.tb_nationality = new System.Windows.Forms.TextBox();
            this.panelViewAssignWorkShift = new System.Windows.Forms.Panel();
            this.BtnSearchEmployee = new System.Windows.Forms.Button();
            this.BtnEditWorkShifts = new System.Windows.Forms.Button();
            this.BtnAssignWorkShifts = new System.Windows.Forms.Button();
            this.CbxNightShift = new System.Windows.Forms.CheckBox();
            this.CbxMiddayShift = new System.Windows.Forms.CheckBox();
            this.CbxMorningShift = new System.Windows.Forms.CheckBox();
            this.TbShiftEmployee = new System.Windows.Forms.TextBox();
            this.LbxWorkShifts = new System.Windows.Forms.ListBox();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.panelSearchEmployee = new System.Windows.Forms.Panel();
            this.BtnSelectEmployee = new System.Windows.Forms.Button();
            this.BtnSearch = new System.Windows.Forms.Button();
            this.TbZipCode = new System.Windows.Forms.TextBox();
            this.TbHomeAddress = new System.Windows.Forms.TextBox();
            this.LblZipCode = new System.Windows.Forms.Label();
            this.LblHomeAddress = new System.Windows.Forms.Label();
            this.TbBsn = new System.Windows.Forms.TextBox();
            this.TbLanguages = new System.Windows.Forms.TextBox();
            this.TbNationality = new System.Windows.Forms.TextBox();
            this.LblBsn = new System.Windows.Forms.Label();
            this.LblLanguages = new System.Windows.Forms.Label();
            this.LblNationality = new System.Windows.Forms.Label();
            this.TbBirthplace = new System.Windows.Forms.TextBox();
            this.LblBirthplace = new System.Windows.Forms.Label();
            this.CbToSearch = new System.Windows.Forms.ComboBox();
            this.TbDateOfBirth = new System.Windows.Forms.TextBox();
            this.TbFamilyName = new System.Windows.Forms.TextBox();
            this.TbName = new System.Windows.Forms.TextBox();
            this.TbId = new System.Windows.Forms.TextBox();
            this.LblDateOfBirth = new System.Windows.Forms.Label();
            this.LblFamilyName = new System.Windows.Forms.Label();
            this.LblName = new System.Windows.Forms.Label();
            this.LblId = new System.Windows.Forms.Label();
            this.LblSearchEmployee = new System.Windows.Forms.Label();
            this.TbEmployeeName = new System.Windows.Forms.TextBox();
            this.pnl_editEmployee = new System.Windows.Forms.Panel();
            this.bt_selectUser = new System.Windows.Forms.Button();
            this.bt_refresh = new System.Windows.Forms.Button();
            this.lb_allEmployees = new System.Windows.Forms.ListBox();
            this.label14 = new System.Windows.Forms.Label();
            this.bt_saveChanges = new System.Windows.Forms.Button();
            this.tb_editPassword = new System.Windows.Forms.TextBox();
            this.bt_fired = new System.Windows.Forms.Button();
            this.tb_employeeStatus = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_editUsername = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cb_editPosition = new System.Windows.Forms.ComboBox();
            this.dp_editDate = new System.Windows.Forms.DateTimePicker();
            this.tb_editZip = new System.Windows.Forms.TextBox();
            this.tb_editHomeAddress = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tb_editBsn = new System.Windows.Forms.TextBox();
            this.tb_editLanguages = new System.Windows.Forms.TextBox();
            this.tb_editNationality = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tb_editBirthplace = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tb_editSurname = new System.Windows.Forms.TextBox();
            this.tb_editName = new System.Windows.Forms.TextBox();
            this.tb_editId = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.LbxEmployeeNames = new System.Windows.Forms.ListBox();
            this.panelCheckPresentAbsent = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.monthCalendar2 = new System.Windows.Forms.MonthCalendar();
            this.RbPresent = new System.Windows.Forms.RadioButton();
            this.RbAbsent = new System.Windows.Forms.RadioButton();
            this.RbSick = new System.Windows.Forms.RadioButton();
            this.BtnMarkPAS = new System.Windows.Forms.Button();
            this.BtnMark = new System.Windows.Forms.Button();
            this.panelSideMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelMainContent.SuspendLayout();
            this.pnl_createEmployee.SuspendLayout();
            this.panelViewAssignWorkShift.SuspendLayout();
            this.panelSearchEmployee.SuspendLayout();
            this.pnl_editEmployee.SuspendLayout();
            this.panelCheckPresentAbsent.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panelSideMenu.Controls.Add(this.BtnMark);
            this.panelSideMenu.Controls.Add(this.bt_editEmployee);
            this.panelSideMenu.Controls.Add(this.bt_addEmployee);
            this.panelSideMenu.Controls.Add(this.btnWorkShifts);
            this.panelSideMenu.Controls.Add(this.btnLogout);
            this.panelSideMenu.Controls.Add(this.panel1);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(228, 794);
            this.panelSideMenu.TabIndex = 13;
            // 
            // bt_editEmployee
            // 
            this.bt_editEmployee.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_editEmployee.Dock = System.Windows.Forms.DockStyle.Top;
            this.bt_editEmployee.FlatAppearance.BorderSize = 0;
            this.bt_editEmployee.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.bt_editEmployee.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.bt_editEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_editEmployee.ForeColor = System.Drawing.Color.Gainsboro;
            this.bt_editEmployee.Location = new System.Drawing.Point(0, 178);
            this.bt_editEmployee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_editEmployee.Name = "bt_editEmployee";
            this.bt_editEmployee.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.bt_editEmployee.Size = new System.Drawing.Size(228, 39);
            this.bt_editEmployee.TabIndex = 6;
            this.bt_editEmployee.Text = "Edit employee";
            this.bt_editEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_editEmployee.UseVisualStyleBackColor = true;
            this.bt_editEmployee.Click += new System.EventHandler(this.bt_editEmployee_Click);
            // 
            // bt_addEmployee
            // 
            this.bt_addEmployee.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_addEmployee.Dock = System.Windows.Forms.DockStyle.Top;
            this.bt_addEmployee.FlatAppearance.BorderSize = 0;
            this.bt_addEmployee.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.bt_addEmployee.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.bt_addEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_addEmployee.ForeColor = System.Drawing.Color.Gainsboro;
            this.bt_addEmployee.Location = new System.Drawing.Point(0, 139);
            this.bt_addEmployee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_addEmployee.Name = "bt_addEmployee";
            this.bt_addEmployee.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.bt_addEmployee.Size = new System.Drawing.Size(228, 39);
            this.bt_addEmployee.TabIndex = 5;
            this.bt_addEmployee.Text = "Create employee";
            this.bt_addEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_addEmployee.UseVisualStyleBackColor = true;
            this.bt_addEmployee.Click += new System.EventHandler(this.bt_addEmployee_Click);
            // 
            // btnWorkShifts
            // 
            this.btnWorkShifts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnWorkShifts.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnWorkShifts.FlatAppearance.BorderSize = 0;
            this.btnWorkShifts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnWorkShifts.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnWorkShifts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWorkShifts.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnWorkShifts.Location = new System.Drawing.Point(0, 100);
            this.btnWorkShifts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnWorkShifts.Name = "btnWorkShifts";
            this.btnWorkShifts.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnWorkShifts.Size = new System.Drawing.Size(228, 39);
            this.btnWorkShifts.TabIndex = 4;
            this.btnWorkShifts.Text = "Workshifts";
            this.btnWorkShifts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnWorkShifts.UseVisualStyleBackColor = true;
            this.btnWorkShifts.Click += new System.EventHandler(this.btnWorkShifts_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnLogout.Location = new System.Drawing.Point(0, 755);
            this.btnLogout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnLogout.Size = new System.Drawing.Size(228, 39);
            this.btnLogout.TabIndex = 3;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelLogo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 100);
            this.panel1.TabIndex = 0;
            // 
            // labelLogo
            // 
            this.labelLogo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelLogo.AutoSize = true;
            this.labelLogo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelLogo.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLogo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelLogo.Location = new System.Drawing.Point(23, 14);
            this.labelLogo.Name = "labelLogo";
            this.labelLogo.Size = new System.Drawing.Size(157, 95);
            this.labelLogo.TabIndex = 4;
            this.labelLogo.Text = "MB";
            this.labelLogo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelMainContent
            // 
            this.panelMainContent.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelMainContent.Controls.Add(this.label3);
            this.panelMainContent.Controls.Add(this.label11);
            this.panelMainContent.Location = new System.Drawing.Point(1234, 610);
            this.panelMainContent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelMainContent.Name = "panelMainContent";
            this.panelMainContent.Size = new System.Drawing.Size(707, 460);
            this.panelMainContent.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.label3.Location = new System.Drawing.Point(92, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(606, 95);
            this.label3.TabIndex = 8;
            this.label3.Text = "Media Bazaar";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Stencil", 36F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.label11.Location = new System.Drawing.Point(204, 242);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(421, 71);
            this.label11.TabIndex = 11;
            this.label11.Text = "management";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnl_createEmployee
            // 
            this.pnl_createEmployee.Controls.Add(this.tb_createPassword);
            this.pnl_createEmployee.Controls.Add(this.lb_password);
            this.pnl_createEmployee.Controls.Add(this.label12);
            this.pnl_createEmployee.Controls.Add(this.tb_createUsername);
            this.pnl_createEmployee.Controls.Add(this.label10);
            this.pnl_createEmployee.Controls.Add(this.lb_username);
            this.pnl_createEmployee.Controls.Add(this.label9);
            this.pnl_createEmployee.Controls.Add(this.lb_createRank);
            this.pnl_createEmployee.Controls.Add(this.label8);
            this.pnl_createEmployee.Controls.Add(this.cb_createRank);
            this.pnl_createEmployee.Controls.Add(this.tb_id);
            this.pnl_createEmployee.Controls.Add(this.dp_birthDay);
            this.pnl_createEmployee.Controls.Add(this.tb_name);
            this.pnl_createEmployee.Controls.Add(this.bt_addUser);
            this.pnl_createEmployee.Controls.Add(this.tb_lastName);
            this.pnl_createEmployee.Controls.Add(this.tb_zipCode);
            this.pnl_createEmployee.Controls.Add(this.label7);
            this.pnl_createEmployee.Controls.Add(this.tb_adress);
            this.pnl_createEmployee.Controls.Add(this.tb_birthplace);
            this.pnl_createEmployee.Controls.Add(this.label1);
            this.pnl_createEmployee.Controls.Add(this.label6);
            this.pnl_createEmployee.Controls.Add(this.label2);
            this.pnl_createEmployee.Controls.Add(this.label5);
            this.pnl_createEmployee.Controls.Add(this.tb_bsn);
            this.pnl_createEmployee.Controls.Add(this.label4);
            this.pnl_createEmployee.Controls.Add(this.tb_languages);
            this.pnl_createEmployee.Controls.Add(this.tb_nationality);
            this.pnl_createEmployee.Location = new System.Drawing.Point(235, 758);
            this.pnl_createEmployee.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnl_createEmployee.Name = "pnl_createEmployee";
            this.pnl_createEmployee.Size = new System.Drawing.Size(837, 518);
            this.pnl_createEmployee.TabIndex = 17;
            this.pnl_createEmployee.Visible = false;
            // 
            // tb_createPassword
            // 
            this.tb_createPassword.Location = new System.Drawing.Point(636, 150);
            this.tb_createPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_createPassword.Name = "tb_createPassword";
            this.tb_createPassword.Size = new System.Drawing.Size(185, 22);
            this.tb_createPassword.TabIndex = 53;
            // 
            // lb_password
            // 
            this.lb_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_password.Location = new System.Drawing.Point(501, 150);
            this.lb_password.Name = "lb_password";
            this.lb_password.Size = new System.Drawing.Size(111, 23);
            this.lb_password.TabIndex = 52;
            this.lb_password.Text = "Password:";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(32, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(160, 23);
            this.label12.TabIndex = 25;
            this.label12.Text = "ID:";
            // 
            // tb_createUsername
            // 
            this.tb_createUsername.Location = new System.Drawing.Point(636, 119);
            this.tb_createUsername.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_createUsername.Name = "tb_createUsername";
            this.tb_createUsername.Size = new System.Drawing.Size(185, 22);
            this.tb_createUsername.TabIndex = 51;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(32, 90);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(160, 23);
            this.label10.TabIndex = 26;
            this.label10.Text = "Name:";
            // 
            // lb_username
            // 
            this.lb_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_username.Location = new System.Drawing.Point(501, 119);
            this.lb_username.Name = "lb_username";
            this.lb_username.Size = new System.Drawing.Size(111, 23);
            this.lb_username.TabIndex = 50;
            this.lb_username.Text = "Username:";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(32, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(160, 23);
            this.label9.TabIndex = 27;
            this.label9.Text = "Surname:";
            // 
            // lb_createRank
            // 
            this.lb_createRank.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_createRank.Location = new System.Drawing.Point(501, 62);
            this.lb_createRank.Name = "lb_createRank";
            this.lb_createRank.Size = new System.Drawing.Size(75, 23);
            this.lb_createRank.TabIndex = 49;
            this.lb_createRank.Text = "Position :";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(32, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(160, 23);
            this.label8.TabIndex = 28;
            this.label8.Text = "Date Of Birth:";
            // 
            // cb_createRank
            // 
            this.cb_createRank.FormattingEnabled = true;
            this.cb_createRank.Location = new System.Drawing.Point(636, 60);
            this.cb_createRank.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cb_createRank.Name = "cb_createRank";
            this.cb_createRank.Size = new System.Drawing.Size(185, 24);
            this.cb_createRank.TabIndex = 48;
            // 
            // tb_id
            // 
            this.tb_id.Location = new System.Drawing.Point(219, 60);
            this.tb_id.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_id.Name = "tb_id";
            this.tb_id.ReadOnly = true;
            this.tb_id.Size = new System.Drawing.Size(264, 22);
            this.tb_id.TabIndex = 29;
            // 
            // dp_birthDay
            // 
            this.dp_birthDay.Location = new System.Drawing.Point(219, 154);
            this.dp_birthDay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dp_birthDay.Name = "dp_birthDay";
            this.dp_birthDay.Size = new System.Drawing.Size(264, 22);
            this.dp_birthDay.TabIndex = 47;
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(219, 91);
            this.tb_name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(264, 22);
            this.tb_name.TabIndex = 30;
            // 
            // bt_addUser
            // 
            this.bt_addUser.Location = new System.Drawing.Point(693, 331);
            this.bt_addUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bt_addUser.Name = "bt_addUser";
            this.bt_addUser.Size = new System.Drawing.Size(100, 39);
            this.bt_addUser.TabIndex = 46;
            this.bt_addUser.Text = "Submit";
            this.bt_addUser.UseVisualStyleBackColor = true;
            this.bt_addUser.Click += new System.EventHandler(this.bt_addUser_Click);
            // 
            // tb_lastName
            // 
            this.tb_lastName.Location = new System.Drawing.Point(219, 122);
            this.tb_lastName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_lastName.Name = "tb_lastName";
            this.tb_lastName.Size = new System.Drawing.Size(264, 22);
            this.tb_lastName.TabIndex = 31;
            // 
            // tb_zipCode
            // 
            this.tb_zipCode.Location = new System.Drawing.Point(219, 340);
            this.tb_zipCode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_zipCode.Name = "tb_zipCode";
            this.tb_zipCode.Size = new System.Drawing.Size(264, 22);
            this.tb_zipCode.TabIndex = 45;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(32, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(160, 23);
            this.label7.TabIndex = 34;
            this.label7.Text = "Birthplace:";
            // 
            // tb_adress
            // 
            this.tb_adress.Location = new System.Drawing.Point(219, 309);
            this.tb_adress.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_adress.Name = "tb_adress";
            this.tb_adress.Size = new System.Drawing.Size(264, 22);
            this.tb_adress.TabIndex = 44;
            // 
            // tb_birthplace
            // 
            this.tb_birthplace.Location = new System.Drawing.Point(219, 185);
            this.tb_birthplace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_birthplace.Name = "tb_birthplace";
            this.tb_birthplace.Size = new System.Drawing.Size(264, 22);
            this.tb_birthplace.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 337);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 23);
            this.label1.TabIndex = 43;
            this.label1.Text = "Zip-code:";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(32, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(160, 23);
            this.label6.TabIndex = 36;
            this.label6.Text = "Nationality:";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 306);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 23);
            this.label2.TabIndex = 42;
            this.label2.Text = "Home Address:";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(32, 245);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 31);
            this.label5.TabIndex = 37;
            this.label5.Text = "Languages";
            // 
            // tb_bsn
            // 
            this.tb_bsn.Location = new System.Drawing.Point(219, 277);
            this.tb_bsn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_bsn.Name = "tb_bsn";
            this.tb_bsn.Size = new System.Drawing.Size(264, 22);
            this.tb_bsn.TabIndex = 41;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 276);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 23);
            this.label4.TabIndex = 38;
            this.label4.Text = "BSN:";
            // 
            // tb_languages
            // 
            this.tb_languages.Location = new System.Drawing.Point(219, 246);
            this.tb_languages.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_languages.Name = "tb_languages";
            this.tb_languages.Size = new System.Drawing.Size(264, 22);
            this.tb_languages.TabIndex = 40;
            // 
            // tb_nationality
            // 
            this.tb_nationality.Location = new System.Drawing.Point(219, 215);
            this.tb_nationality.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_nationality.Name = "tb_nationality";
            this.tb_nationality.Size = new System.Drawing.Size(264, 22);
            this.tb_nationality.TabIndex = 39;
            // 
            // panelViewAssignWorkShift
            // 
            this.panelViewAssignWorkShift.Controls.Add(this.LbxEmployeeNames);
            this.panelViewAssignWorkShift.Controls.Add(this.BtnSearchEmployee);
            this.panelViewAssignWorkShift.Controls.Add(this.BtnEditWorkShifts);
            this.panelViewAssignWorkShift.Controls.Add(this.BtnAssignWorkShifts);
            this.panelViewAssignWorkShift.Controls.Add(this.CbxNightShift);
            this.panelViewAssignWorkShift.Controls.Add(this.CbxMiddayShift);
            this.panelViewAssignWorkShift.Controls.Add(this.CbxMorningShift);
            this.panelViewAssignWorkShift.Controls.Add(this.TbShiftEmployee);
            this.panelViewAssignWorkShift.Controls.Add(this.LbxWorkShifts);
            this.panelViewAssignWorkShift.Controls.Add(this.monthCalendar1);
            this.panelViewAssignWorkShift.Location = new System.Drawing.Point(1085, 544);
            this.panelViewAssignWorkShift.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelViewAssignWorkShift.Name = "panelViewAssignWorkShift";
            this.panelViewAssignWorkShift.Size = new System.Drawing.Size(61, 44);
            this.panelViewAssignWorkShift.TabIndex = 15;
            this.panelViewAssignWorkShift.Visible = false;
            // 
            // BtnSearchEmployee
            // 
            this.BtnSearchEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSearchEmployee.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSearchEmployee.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSearchEmployee.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnSearchEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSearchEmployee.ForeColor = System.Drawing.Color.LightGray;
            this.BtnSearchEmployee.Location = new System.Drawing.Point(651, 207);
            this.BtnSearchEmployee.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnSearchEmployee.Name = "BtnSearchEmployee";
            this.BtnSearchEmployee.Size = new System.Drawing.Size(109, 50);
            this.BtnSearchEmployee.TabIndex = 44;
            this.BtnSearchEmployee.Text = "Search Employee";
            this.BtnSearchEmployee.UseVisualStyleBackColor = false;
            this.BtnSearchEmployee.Click += new System.EventHandler(this.BtnSearchEmployee_Click);
            // 
            // BtnEditWorkShifts
            // 
            this.BtnEditWorkShifts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnEditWorkShifts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnEditWorkShifts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnEditWorkShifts.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnEditWorkShifts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEditWorkShifts.ForeColor = System.Drawing.Color.LightGray;
            this.BtnEditWorkShifts.Location = new System.Drawing.Point(396, 177);
            this.BtnEditWorkShifts.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnEditWorkShifts.Name = "BtnEditWorkShifts";
            this.BtnEditWorkShifts.Size = new System.Drawing.Size(109, 50);
            this.BtnEditWorkShifts.TabIndex = 42;
            this.BtnEditWorkShifts.Text = "Edit WorkShift";
            this.BtnEditWorkShifts.UseVisualStyleBackColor = false;
            this.BtnEditWorkShifts.Click += new System.EventHandler(this.BtnEditWorkShifts_Click);
            // 
            // BtnAssignWorkShifts
            // 
            this.BtnAssignWorkShifts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnAssignWorkShifts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnAssignWorkShifts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnAssignWorkShifts.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnAssignWorkShifts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAssignWorkShifts.ForeColor = System.Drawing.Color.LightGray;
            this.BtnAssignWorkShifts.Location = new System.Drawing.Point(396, 86);
            this.BtnAssignWorkShifts.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnAssignWorkShifts.Name = "BtnAssignWorkShifts";
            this.BtnAssignWorkShifts.Size = new System.Drawing.Size(109, 50);
            this.BtnAssignWorkShifts.TabIndex = 41;
            this.BtnAssignWorkShifts.Text = "Assign WorkShift";
            this.BtnAssignWorkShifts.UseVisualStyleBackColor = false;
            this.BtnAssignWorkShifts.Click += new System.EventHandler(this.BtnAssignWorkShifts_Click);
            // 
            // CbxNightShift
            // 
            this.CbxNightShift.AutoSize = true;
            this.CbxNightShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbxNightShift.Location = new System.Drawing.Point(528, 151);
            this.CbxNightShift.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CbxNightShift.Name = "CbxNightShift";
            this.CbxNightShift.Size = new System.Drawing.Size(129, 24);
            this.CbxNightShift.TabIndex = 40;
            this.CbxNightShift.Text = "16:00 - 20:00";
            this.CbxNightShift.UseVisualStyleBackColor = true;
            // 
            // CbxMiddayShift
            // 
            this.CbxMiddayShift.AutoSize = true;
            this.CbxMiddayShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbxMiddayShift.Location = new System.Drawing.Point(528, 121);
            this.CbxMiddayShift.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CbxMiddayShift.Name = "CbxMiddayShift";
            this.CbxMiddayShift.Size = new System.Drawing.Size(129, 24);
            this.CbxMiddayShift.TabIndex = 39;
            this.CbxMiddayShift.Text = "12:00 - 16:00";
            this.CbxMiddayShift.UseVisualStyleBackColor = true;
            // 
            // CbxMorningShift
            // 
            this.CbxMorningShift.AutoSize = true;
            this.CbxMorningShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbxMorningShift.Location = new System.Drawing.Point(528, 91);
            this.CbxMorningShift.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CbxMorningShift.Name = "CbxMorningShift";
            this.CbxMorningShift.Size = new System.Drawing.Size(129, 24);
            this.CbxMorningShift.TabIndex = 38;
            this.CbxMorningShift.Text = "08:00 - 12:00";
            this.CbxMorningShift.UseVisualStyleBackColor = true;
            // 
            // TbShiftEmployee
            // 
            this.TbShiftEmployee.Location = new System.Drawing.Point(528, 204);
            this.TbShiftEmployee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbShiftEmployee.Name = "TbShiftEmployee";
            this.TbShiftEmployee.Size = new System.Drawing.Size(100, 22);
            this.TbShiftEmployee.TabIndex = 37;
            // 
            // LbxWorkShifts
            // 
            this.LbxWorkShifts.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbxWorkShifts.FormattingEnabled = true;
            this.LbxWorkShifts.ItemHeight = 29;
            this.LbxWorkShifts.Location = new System.Drawing.Point(45, 302);
            this.LbxWorkShifts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LbxWorkShifts.Name = "LbxWorkShifts";
            this.LbxWorkShifts.Size = new System.Drawing.Size(392, 294);
            this.LbxWorkShifts.TabIndex = 34;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(45, 66);
            this.monthCalendar1.MaxSelectionCount = 1;
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 33;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // panelSearchEmployee
            // 
            this.panelSearchEmployee.Controls.Add(this.BtnSelectEmployee);
            this.panelSearchEmployee.Controls.Add(this.BtnSearch);
            this.panelSearchEmployee.Controls.Add(this.TbZipCode);
            this.panelSearchEmployee.Controls.Add(this.TbHomeAddress);
            this.panelSearchEmployee.Controls.Add(this.LblZipCode);
            this.panelSearchEmployee.Controls.Add(this.LblHomeAddress);
            this.panelSearchEmployee.Controls.Add(this.TbBsn);
            this.panelSearchEmployee.Controls.Add(this.TbLanguages);
            this.panelSearchEmployee.Controls.Add(this.TbNationality);
            this.panelSearchEmployee.Controls.Add(this.LblBsn);
            this.panelSearchEmployee.Controls.Add(this.LblLanguages);
            this.panelSearchEmployee.Controls.Add(this.LblNationality);
            this.panelSearchEmployee.Controls.Add(this.TbBirthplace);
            this.panelSearchEmployee.Controls.Add(this.LblBirthplace);
            this.panelSearchEmployee.Controls.Add(this.CbToSearch);
            this.panelSearchEmployee.Controls.Add(this.TbDateOfBirth);
            this.panelSearchEmployee.Controls.Add(this.TbFamilyName);
            this.panelSearchEmployee.Controls.Add(this.TbName);
            this.panelSearchEmployee.Controls.Add(this.TbId);
            this.panelSearchEmployee.Controls.Add(this.LblDateOfBirth);
            this.panelSearchEmployee.Controls.Add(this.LblFamilyName);
            this.panelSearchEmployee.Controls.Add(this.LblName);
            this.panelSearchEmployee.Controls.Add(this.LblId);
            this.panelSearchEmployee.Controls.Add(this.LblSearchEmployee);
            this.panelSearchEmployee.Controls.Add(this.TbEmployeeName);
            this.panelSearchEmployee.Location = new System.Drawing.Point(1166, 280);
            this.panelSearchEmployee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelSearchEmployee.Name = "panelSearchEmployee";
            this.panelSearchEmployee.Size = new System.Drawing.Size(199, 308);
            this.panelSearchEmployee.TabIndex = 16;
            this.panelSearchEmployee.Visible = false;
            // 
            // BtnSelectEmployee
            // 
            this.BtnSelectEmployee.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnSelectEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSelectEmployee.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSelectEmployee.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSelectEmployee.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnSelectEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSelectEmployee.ForeColor = System.Drawing.Color.LightGray;
            this.BtnSelectEmployee.Location = new System.Drawing.Point(238, 279);
            this.BtnSelectEmployee.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnSelectEmployee.Name = "BtnSelectEmployee";
            this.BtnSelectEmployee.Size = new System.Drawing.Size(109, 50);
            this.BtnSelectEmployee.TabIndex = 71;
            this.BtnSelectEmployee.Text = "Select this employee";
            this.BtnSelectEmployee.UseVisualStyleBackColor = false;
            this.BtnSelectEmployee.Click += new System.EventHandler(this.BtnSelectEmployee_Click);
            // 
            // BtnSearch
            // 
            this.BtnSearch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSearch.ForeColor = System.Drawing.Color.LightGray;
            this.BtnSearch.Location = new System.Drawing.Point(238, -16);
            this.BtnSearch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnSearch.Name = "BtnSearch";
            this.BtnSearch.Size = new System.Drawing.Size(109, 50);
            this.BtnSearch.TabIndex = 70;
            this.BtnSearch.Text = "Search Employee";
            this.BtnSearch.UseVisualStyleBackColor = false;
            this.BtnSearch.Click += new System.EventHandler(this.BtnSearch_Click);
            // 
            // TbZipCode
            // 
            this.TbZipCode.Location = new System.Drawing.Point(347, 427);
            this.TbZipCode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbZipCode.Name = "TbZipCode";
            this.TbZipCode.Size = new System.Drawing.Size(100, 22);
            this.TbZipCode.TabIndex = 69;
            // 
            // TbHomeAddress
            // 
            this.TbHomeAddress.Location = new System.Drawing.Point(347, 396);
            this.TbHomeAddress.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbHomeAddress.Name = "TbHomeAddress";
            this.TbHomeAddress.Size = new System.Drawing.Size(100, 22);
            this.TbHomeAddress.TabIndex = 68;
            // 
            // LblZipCode
            // 
            this.LblZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblZipCode.Location = new System.Drawing.Point(160, 425);
            this.LblZipCode.Name = "LblZipCode";
            this.LblZipCode.Size = new System.Drawing.Size(160, 23);
            this.LblZipCode.TabIndex = 67;
            this.LblZipCode.Text = "Zip-code:";
            // 
            // LblHomeAddress
            // 
            this.LblHomeAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblHomeAddress.Location = new System.Drawing.Point(160, 394);
            this.LblHomeAddress.Name = "LblHomeAddress";
            this.LblHomeAddress.Size = new System.Drawing.Size(160, 23);
            this.LblHomeAddress.TabIndex = 66;
            this.LblHomeAddress.Text = "Home Address:";
            // 
            // TbBsn
            // 
            this.TbBsn.Location = new System.Drawing.Point(347, 366);
            this.TbBsn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbBsn.Name = "TbBsn";
            this.TbBsn.Size = new System.Drawing.Size(100, 22);
            this.TbBsn.TabIndex = 65;
            // 
            // TbLanguages
            // 
            this.TbLanguages.Location = new System.Drawing.Point(347, 334);
            this.TbLanguages.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbLanguages.Name = "TbLanguages";
            this.TbLanguages.Size = new System.Drawing.Size(100, 22);
            this.TbLanguages.TabIndex = 64;
            // 
            // TbNationality
            // 
            this.TbNationality.Location = new System.Drawing.Point(347, 303);
            this.TbNationality.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbNationality.Name = "TbNationality";
            this.TbNationality.Size = new System.Drawing.Size(100, 22);
            this.TbNationality.TabIndex = 63;
            // 
            // LblBsn
            // 
            this.LblBsn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBsn.Location = new System.Drawing.Point(160, 363);
            this.LblBsn.Name = "LblBsn";
            this.LblBsn.Size = new System.Drawing.Size(160, 23);
            this.LblBsn.TabIndex = 62;
            this.LblBsn.Text = "BSN:";
            // 
            // LblLanguages
            // 
            this.LblLanguages.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLanguages.Location = new System.Drawing.Point(160, 332);
            this.LblLanguages.Name = "LblLanguages";
            this.LblLanguages.Size = new System.Drawing.Size(160, 31);
            this.LblLanguages.TabIndex = 61;
            this.LblLanguages.Text = "Languages";
            // 
            // LblNationality
            // 
            this.LblNationality.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNationality.Location = new System.Drawing.Point(160, 302);
            this.LblNationality.Name = "LblNationality";
            this.LblNationality.Size = new System.Drawing.Size(160, 23);
            this.LblNationality.TabIndex = 60;
            this.LblNationality.Text = "Nationality:";
            // 
            // TbBirthplace
            // 
            this.TbBirthplace.Location = new System.Drawing.Point(347, 272);
            this.TbBirthplace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbBirthplace.Name = "TbBirthplace";
            this.TbBirthplace.Size = new System.Drawing.Size(100, 22);
            this.TbBirthplace.TabIndex = 59;
            // 
            // LblBirthplace
            // 
            this.LblBirthplace.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBirthplace.Location = new System.Drawing.Point(160, 270);
            this.LblBirthplace.Name = "LblBirthplace";
            this.LblBirthplace.Size = new System.Drawing.Size(160, 23);
            this.LblBirthplace.TabIndex = 58;
            this.LblBirthplace.Text = "Birthplace:";
            // 
            // CbToSearch
            // 
            this.CbToSearch.FormattingEnabled = true;
            this.CbToSearch.Items.AddRange(new object[] {
            "FirstName",
            "LastName"});
            this.CbToSearch.Location = new System.Drawing.Point(325, 117);
            this.CbToSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CbToSearch.Name = "CbToSearch";
            this.CbToSearch.Size = new System.Drawing.Size(121, 24);
            this.CbToSearch.TabIndex = 57;
            // 
            // TbDateOfBirth
            // 
            this.TbDateOfBirth.Location = new System.Drawing.Point(347, 241);
            this.TbDateOfBirth.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbDateOfBirth.Name = "TbDateOfBirth";
            this.TbDateOfBirth.Size = new System.Drawing.Size(100, 22);
            this.TbDateOfBirth.TabIndex = 56;
            // 
            // TbFamilyName
            // 
            this.TbFamilyName.Location = new System.Drawing.Point(347, 210);
            this.TbFamilyName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbFamilyName.Name = "TbFamilyName";
            this.TbFamilyName.Size = new System.Drawing.Size(100, 22);
            this.TbFamilyName.TabIndex = 55;
            // 
            // TbName
            // 
            this.TbName.Location = new System.Drawing.Point(347, 178);
            this.TbName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbName.Name = "TbName";
            this.TbName.Size = new System.Drawing.Size(100, 22);
            this.TbName.TabIndex = 54;
            // 
            // TbId
            // 
            this.TbId.Location = new System.Drawing.Point(347, 148);
            this.TbId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbId.Name = "TbId";
            this.TbId.Size = new System.Drawing.Size(100, 22);
            this.TbId.TabIndex = 53;
            // 
            // LblDateOfBirth
            // 
            this.LblDateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDateOfBirth.Location = new System.Drawing.Point(160, 239);
            this.LblDateOfBirth.Name = "LblDateOfBirth";
            this.LblDateOfBirth.Size = new System.Drawing.Size(160, 23);
            this.LblDateOfBirth.TabIndex = 52;
            this.LblDateOfBirth.Text = "Date Of Birth:";
            // 
            // LblFamilyName
            // 
            this.LblFamilyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFamilyName.Location = new System.Drawing.Point(160, 208);
            this.LblFamilyName.Name = "LblFamilyName";
            this.LblFamilyName.Size = new System.Drawing.Size(160, 23);
            this.LblFamilyName.TabIndex = 51;
            this.LblFamilyName.Text = "Family Name:";
            // 
            // LblName
            // 
            this.LblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblName.Location = new System.Drawing.Point(160, 177);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(160, 23);
            this.LblName.TabIndex = 50;
            this.LblName.Text = "Name:";
            // 
            // LblId
            // 
            this.LblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblId.Location = new System.Drawing.Point(160, 146);
            this.LblId.Name = "LblId";
            this.LblId.Size = new System.Drawing.Size(160, 23);
            this.LblId.TabIndex = 49;
            this.LblId.Text = "ID:";
            // 
            // LblSearchEmployee
            // 
            this.LblSearchEmployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSearchEmployee.Location = new System.Drawing.Point(160, 114);
            this.LblSearchEmployee.Name = "LblSearchEmployee";
            this.LblSearchEmployee.Size = new System.Drawing.Size(292, 23);
            this.LblSearchEmployee.TabIndex = 47;
            this.LblSearchEmployee.Text = "Employee Name:";
            // 
            // TbEmployeeName
            // 
            this.TbEmployeeName.Location = new System.Drawing.Point(453, 117);
            this.TbEmployeeName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TbEmployeeName.Name = "TbEmployeeName";
            this.TbEmployeeName.Size = new System.Drawing.Size(100, 22);
            this.TbEmployeeName.TabIndex = 48;
            // 
            // pnl_editEmployee
            // 
            this.pnl_editEmployee.Controls.Add(this.bt_selectUser);
            this.pnl_editEmployee.Controls.Add(this.bt_refresh);
            this.pnl_editEmployee.Controls.Add(this.lb_allEmployees);
            this.pnl_editEmployee.Controls.Add(this.label14);
            this.pnl_editEmployee.Controls.Add(this.bt_saveChanges);
            this.pnl_editEmployee.Controls.Add(this.tb_editPassword);
            this.pnl_editEmployee.Controls.Add(this.bt_fired);
            this.pnl_editEmployee.Controls.Add(this.tb_employeeStatus);
            this.pnl_editEmployee.Controls.Add(this.label13);
            this.pnl_editEmployee.Controls.Add(this.tb_editUsername);
            this.pnl_editEmployee.Controls.Add(this.label15);
            this.pnl_editEmployee.Controls.Add(this.label16);
            this.pnl_editEmployee.Controls.Add(this.cb_editPosition);
            this.pnl_editEmployee.Controls.Add(this.dp_editDate);
            this.pnl_editEmployee.Controls.Add(this.tb_editZip);
            this.pnl_editEmployee.Controls.Add(this.tb_editHomeAddress);
            this.pnl_editEmployee.Controls.Add(this.label17);
            this.pnl_editEmployee.Controls.Add(this.label18);
            this.pnl_editEmployee.Controls.Add(this.tb_editBsn);
            this.pnl_editEmployee.Controls.Add(this.tb_editLanguages);
            this.pnl_editEmployee.Controls.Add(this.tb_editNationality);
            this.pnl_editEmployee.Controls.Add(this.label19);
            this.pnl_editEmployee.Controls.Add(this.label20);
            this.pnl_editEmployee.Controls.Add(this.label21);
            this.pnl_editEmployee.Controls.Add(this.tb_editBirthplace);
            this.pnl_editEmployee.Controls.Add(this.label22);
            this.pnl_editEmployee.Controls.Add(this.tb_editSurname);
            this.pnl_editEmployee.Controls.Add(this.tb_editName);
            this.pnl_editEmployee.Controls.Add(this.tb_editId);
            this.pnl_editEmployee.Controls.Add(this.label23);
            this.pnl_editEmployee.Controls.Add(this.label24);
            this.pnl_editEmployee.Controls.Add(this.label25);
            this.pnl_editEmployee.Controls.Add(this.label26);
            this.pnl_editEmployee.Location = new System.Drawing.Point(1219, 162);
            this.pnl_editEmployee.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnl_editEmployee.Name = "pnl_editEmployee";
            this.pnl_editEmployee.Size = new System.Drawing.Size(88, 50);
            this.pnl_editEmployee.TabIndex = 17;
            this.pnl_editEmployee.Visible = false;
            // 
            // bt_selectUser
            // 
            this.bt_selectUser.Location = new System.Drawing.Point(111, 544);
            this.bt_selectUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bt_selectUser.Name = "bt_selectUser";
            this.bt_selectUser.Size = new System.Drawing.Size(93, 42);
            this.bt_selectUser.TabIndex = 113;
            this.bt_selectUser.Text = "show details";
            this.bt_selectUser.UseVisualStyleBackColor = true;
            this.bt_selectUser.Click += new System.EventHandler(this.bt_selectUser_Click);
            // 
            // bt_refresh
            // 
            this.bt_refresh.Location = new System.Drawing.Point(4, 544);
            this.bt_refresh.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bt_refresh.Name = "bt_refresh";
            this.bt_refresh.Size = new System.Drawing.Size(83, 42);
            this.bt_refresh.TabIndex = 112;
            this.bt_refresh.Text = "refresh";
            this.bt_refresh.UseVisualStyleBackColor = true;
            this.bt_refresh.Click += new System.EventHandler(this.bt_refresh_Click);
            // 
            // lb_allEmployees
            // 
            this.lb_allEmployees.FormattingEnabled = true;
            this.lb_allEmployees.ItemHeight = 16;
            this.lb_allEmployees.Location = new System.Drawing.Point(4, 2);
            this.lb_allEmployees.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lb_allEmployees.Name = "lb_allEmployees";
            this.lb_allEmployees.Size = new System.Drawing.Size(212, 532);
            this.lb_allEmployees.TabIndex = 111;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(665, 170);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(123, 23);
            this.label14.TabIndex = 86;
            this.label14.Text = "Status:";
            // 
            // bt_saveChanges
            // 
            this.bt_saveChanges.Location = new System.Drawing.Point(887, 319);
            this.bt_saveChanges.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bt_saveChanges.Name = "bt_saveChanges";
            this.bt_saveChanges.Size = new System.Drawing.Size(100, 28);
            this.bt_saveChanges.TabIndex = 110;
            this.bt_saveChanges.Text = "save";
            this.bt_saveChanges.UseVisualStyleBackColor = true;
            this.bt_saveChanges.Click += new System.EventHandler(this.bt_saveChanges_Click);
            // 
            // tb_editPassword
            // 
            this.tb_editPassword.Location = new System.Drawing.Point(800, 134);
            this.tb_editPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_editPassword.Name = "tb_editPassword";
            this.tb_editPassword.Size = new System.Drawing.Size(197, 22);
            this.tb_editPassword.TabIndex = 109;
            // 
            // bt_fired
            // 
            this.bt_fired.Location = new System.Drawing.Point(831, 202);
            this.bt_fired.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bt_fired.Name = "bt_fired";
            this.bt_fired.Size = new System.Drawing.Size(133, 33);
            this.bt_fired.TabIndex = 84;
            this.bt_fired.Text = "change status";
            this.bt_fired.UseVisualStyleBackColor = true;
            this.bt_fired.Click += new System.EventHandler(this.bt_fired_Click);
            // 
            // tb_employeeStatus
            // 
            this.tb_employeeStatus.Location = new System.Drawing.Point(800, 170);
            this.tb_employeeStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tb_employeeStatus.Name = "tb_employeeStatus";
            this.tb_employeeStatus.ReadOnly = true;
            this.tb_employeeStatus.Size = new System.Drawing.Size(197, 22);
            this.tb_employeeStatus.TabIndex = 82;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(665, 134);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 23);
            this.label13.TabIndex = 108;
            this.label13.Text = "Password:";
            // 
            // tb_editUsername
            // 
            this.tb_editUsername.Location = new System.Drawing.Point(800, 103);
            this.tb_editUsername.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_editUsername.Name = "tb_editUsername";
            this.tb_editUsername.Size = new System.Drawing.Size(197, 22);
            this.tb_editUsername.TabIndex = 107;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(665, 103);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(123, 23);
            this.label15.TabIndex = 106;
            this.label15.Text = "Username:";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(665, 46);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 23);
            this.label16.TabIndex = 105;
            this.label16.Text = "Position :";
            // 
            // cb_editPosition
            // 
            this.cb_editPosition.FormattingEnabled = true;
            this.cb_editPosition.Location = new System.Drawing.Point(800, 44);
            this.cb_editPosition.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cb_editPosition.Name = "cb_editPosition";
            this.cb_editPosition.Size = new System.Drawing.Size(197, 24);
            this.cb_editPosition.TabIndex = 104;
            // 
            // dp_editDate
            // 
            this.dp_editDate.Location = new System.Drawing.Point(381, 138);
            this.dp_editDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dp_editDate.Name = "dp_editDate";
            this.dp_editDate.Size = new System.Drawing.Size(264, 22);
            this.dp_editDate.TabIndex = 103;
            // 
            // tb_editZip
            // 
            this.tb_editZip.Location = new System.Drawing.Point(381, 324);
            this.tb_editZip.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_editZip.Name = "tb_editZip";
            this.tb_editZip.Size = new System.Drawing.Size(264, 22);
            this.tb_editZip.TabIndex = 102;
            // 
            // tb_editHomeAddress
            // 
            this.tb_editHomeAddress.Location = new System.Drawing.Point(381, 293);
            this.tb_editHomeAddress.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_editHomeAddress.Name = "tb_editHomeAddress";
            this.tb_editHomeAddress.Size = new System.Drawing.Size(264, 22);
            this.tb_editHomeAddress.TabIndex = 101;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(224, 322);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(151, 23);
            this.label17.TabIndex = 100;
            this.label17.Text = "Zip-code:";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(224, 292);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(151, 23);
            this.label18.TabIndex = 99;
            this.label18.Text = "Home Address:";
            // 
            // tb_editBsn
            // 
            this.tb_editBsn.Location = new System.Drawing.Point(381, 261);
            this.tb_editBsn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_editBsn.Name = "tb_editBsn";
            this.tb_editBsn.Size = new System.Drawing.Size(264, 22);
            this.tb_editBsn.TabIndex = 98;
            // 
            // tb_editLanguages
            // 
            this.tb_editLanguages.Location = new System.Drawing.Point(381, 230);
            this.tb_editLanguages.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_editLanguages.Name = "tb_editLanguages";
            this.tb_editLanguages.Size = new System.Drawing.Size(264, 22);
            this.tb_editLanguages.TabIndex = 97;
            // 
            // tb_editNationality
            // 
            this.tb_editNationality.Location = new System.Drawing.Point(381, 199);
            this.tb_editNationality.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_editNationality.Name = "tb_editNationality";
            this.tb_editNationality.Size = new System.Drawing.Size(264, 22);
            this.tb_editNationality.TabIndex = 96;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(224, 261);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(151, 23);
            this.label19.TabIndex = 95;
            this.label19.Text = "BSN:";
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(224, 230);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(151, 31);
            this.label20.TabIndex = 94;
            this.label20.Text = "Languages";
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(224, 198);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(151, 23);
            this.label21.TabIndex = 93;
            this.label21.Text = "Nationality:";
            // 
            // tb_editBirthplace
            // 
            this.tb_editBirthplace.Location = new System.Drawing.Point(381, 169);
            this.tb_editBirthplace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_editBirthplace.Name = "tb_editBirthplace";
            this.tb_editBirthplace.Size = new System.Drawing.Size(264, 22);
            this.tb_editBirthplace.TabIndex = 92;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(224, 167);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(151, 23);
            this.label22.TabIndex = 91;
            this.label22.Text = "Birthplace:";
            // 
            // tb_editSurname
            // 
            this.tb_editSurname.Location = new System.Drawing.Point(381, 106);
            this.tb_editSurname.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_editSurname.Name = "tb_editSurname";
            this.tb_editSurname.Size = new System.Drawing.Size(264, 22);
            this.tb_editSurname.TabIndex = 90;
            // 
            // tb_editName
            // 
            this.tb_editName.Location = new System.Drawing.Point(381, 75);
            this.tb_editName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_editName.Name = "tb_editName";
            this.tb_editName.Size = new System.Drawing.Size(264, 22);
            this.tb_editName.TabIndex = 89;
            // 
            // tb_editId
            // 
            this.tb_editId.Location = new System.Drawing.Point(381, 44);
            this.tb_editId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_editId.Name = "tb_editId";
            this.tb_editId.ReadOnly = true;
            this.tb_editId.Size = new System.Drawing.Size(264, 22);
            this.tb_editId.TabIndex = 88;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(224, 137);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(151, 23);
            this.label23.TabIndex = 87;
            this.label23.Text = "Date Of Birth:";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(224, 106);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(151, 23);
            this.label24.TabIndex = 85;
            this.label24.Text = "Surname:";
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(224, 75);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(151, 23);
            this.label25.TabIndex = 83;
            this.label25.Text = "Name:";
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(224, 43);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(151, 23);
            this.label26.TabIndex = 81;
            this.label26.Text = "ID:";
            // 
            // LbxEmployeeNames
            // 
            this.LbxEmployeeNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbxEmployeeNames.FormattingEnabled = true;
            this.LbxEmployeeNames.ItemHeight = 20;
            this.LbxEmployeeNames.Location = new System.Drawing.Point(506, 302);
            this.LbxEmployeeNames.Name = "LbxEmployeeNames";
            this.LbxEmployeeNames.Size = new System.Drawing.Size(197, 264);
            this.LbxEmployeeNames.TabIndex = 45;
            this.LbxEmployeeNames.SelectedIndexChanged += new System.EventHandler(this.LbxEmployeeNames_SelectedIndexChanged);
            // 
            // panelCheckPresentAbsent
            // 
            this.panelCheckPresentAbsent.Controls.Add(this.BtnMarkPAS);
            this.panelCheckPresentAbsent.Controls.Add(this.RbSick);
            this.panelCheckPresentAbsent.Controls.Add(this.RbAbsent);
            this.panelCheckPresentAbsent.Controls.Add(this.RbPresent);
            this.panelCheckPresentAbsent.Controls.Add(this.listBox1);
            this.panelCheckPresentAbsent.Controls.Add(this.monthCalendar2);
            this.panelCheckPresentAbsent.Location = new System.Drawing.Point(256, 14);
            this.panelCheckPresentAbsent.Name = "panelCheckPresentAbsent";
            this.panelCheckPresentAbsent.Size = new System.Drawing.Size(763, 559);
            this.panelCheckPresentAbsent.TabIndex = 18;
            this.panelCheckPresentAbsent.Visible = false;
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 29;
            this.listBox1.Location = new System.Drawing.Point(15, 255);
            this.listBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(392, 294);
            this.listBox1.TabIndex = 36;
            // 
            // monthCalendar2
            // 
            this.monthCalendar2.Location = new System.Drawing.Point(15, 19);
            this.monthCalendar2.MaxSelectionCount = 1;
            this.monthCalendar2.Name = "monthCalendar2";
            this.monthCalendar2.TabIndex = 35;
            this.monthCalendar2.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar2_DateChanged);
            // 
            // RbPresent
            // 
            this.RbPresent.AutoSize = true;
            this.RbPresent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RbPresent.Location = new System.Drawing.Point(484, 57);
            this.RbPresent.Name = "RbPresent";
            this.RbPresent.Size = new System.Drawing.Size(100, 29);
            this.RbPresent.TabIndex = 37;
            this.RbPresent.TabStop = true;
            this.RbPresent.Text = "Present";
            this.RbPresent.UseVisualStyleBackColor = true;
            // 
            // RbAbsent
            // 
            this.RbAbsent.AutoSize = true;
            this.RbAbsent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RbAbsent.Location = new System.Drawing.Point(484, 92);
            this.RbAbsent.Name = "RbAbsent";
            this.RbAbsent.Size = new System.Drawing.Size(95, 29);
            this.RbAbsent.TabIndex = 38;
            this.RbAbsent.TabStop = true;
            this.RbAbsent.Text = "Absent";
            this.RbAbsent.UseVisualStyleBackColor = true;
            // 
            // RbSick
            // 
            this.RbSick.AutoSize = true;
            this.RbSick.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RbSick.Location = new System.Drawing.Point(484, 127);
            this.RbSick.Name = "RbSick";
            this.RbSick.Size = new System.Drawing.Size(71, 29);
            this.RbSick.TabIndex = 39;
            this.RbSick.TabStop = true;
            this.RbSick.Text = "Sick";
            this.RbSick.UseVisualStyleBackColor = true;
            // 
            // BtnMarkPAS
            // 
            this.BtnMarkPAS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnMarkPAS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnMarkPAS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnMarkPAS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnMarkPAS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMarkPAS.ForeColor = System.Drawing.Color.LightGray;
            this.BtnMarkPAS.Location = new System.Drawing.Point(335, 80);
            this.BtnMarkPAS.Margin = new System.Windows.Forms.Padding(4);
            this.BtnMarkPAS.Name = "BtnMarkPAS";
            this.BtnMarkPAS.Size = new System.Drawing.Size(109, 50);
            this.BtnMarkPAS.TabIndex = 42;
            this.BtnMarkPAS.Text = "Mark as";
            this.BtnMarkPAS.UseVisualStyleBackColor = false;
            this.BtnMarkPAS.Click += new System.EventHandler(this.BtnMarkPAS_Click);
            // 
            // BtnMark
            // 
            this.BtnMark.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnMark.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnMark.FlatAppearance.BorderSize = 0;
            this.BtnMark.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnMark.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnMark.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMark.ForeColor = System.Drawing.Color.Gainsboro;
            this.BtnMark.Location = new System.Drawing.Point(0, 217);
            this.BtnMark.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnMark.Name = "BtnMark";
            this.BtnMark.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.BtnMark.Size = new System.Drawing.Size(228, 39);
            this.BtnMark.TabIndex = 7;
            this.BtnMark.Text = "Mark Present";
            this.BtnMark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnMark.UseVisualStyleBackColor = true;
            this.BtnMark.Click += new System.EventHandler(this.BtnMark_Click);
            // 
            // HR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1377, 794);
            this.Controls.Add(this.panelCheckPresentAbsent);
            this.Controls.Add(this.pnl_createEmployee);
            this.Controls.Add(this.pnl_editEmployee);
            this.Controls.Add(this.panelSearchEmployee);
            this.Controls.Add(this.panelViewAssignWorkShift);
            this.Controls.Add(this.panelMainContent);
            this.Controls.Add(this.panelSideMenu);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(899, 598);
            this.Name = "HR";
            this.Text = "HR";
            this.Load += new System.EventHandler(this.HR_Load);
            this.panelSideMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelMainContent.ResumeLayout(false);
            this.panelMainContent.PerformLayout();
            this.pnl_createEmployee.ResumeLayout(false);
            this.pnl_createEmployee.PerformLayout();
            this.panelViewAssignWorkShift.ResumeLayout(false);
            this.panelViewAssignWorkShift.PerformLayout();
            this.panelSearchEmployee.ResumeLayout(false);
            this.panelSearchEmployee.PerformLayout();
            this.pnl_editEmployee.ResumeLayout(false);
            this.pnl_editEmployee.PerformLayout();
            this.panelCheckPresentAbsent.ResumeLayout(false);
            this.panelCheckPresentAbsent.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Button btnWorkShifts;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelLogo;
        private System.Windows.Forms.Panel panelMainContent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panelViewAssignWorkShift;
        private System.Windows.Forms.CheckBox CbxNightShift;
        private System.Windows.Forms.CheckBox CbxMiddayShift;
        private System.Windows.Forms.CheckBox CbxMorningShift;
        private System.Windows.Forms.TextBox TbShiftEmployee;
        private System.Windows.Forms.ListBox LbxWorkShifts;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button BtnAssignWorkShifts;
        private System.Windows.Forms.Button BtnEditWorkShifts;
        private System.Windows.Forms.Button BtnSearchEmployee;
        private System.Windows.Forms.Panel panelSearchEmployee;
        private System.Windows.Forms.Button BtnSelectEmployee;
        private System.Windows.Forms.Button BtnSearch;
        private System.Windows.Forms.TextBox TbZipCode;
        private System.Windows.Forms.TextBox TbHomeAddress;
        private System.Windows.Forms.Label LblZipCode;
        private System.Windows.Forms.Label LblHomeAddress;
        private System.Windows.Forms.TextBox TbBsn;
        private System.Windows.Forms.TextBox TbLanguages;
        private System.Windows.Forms.TextBox TbNationality;
        private System.Windows.Forms.Label LblBsn;
        private System.Windows.Forms.Label LblLanguages;
        private System.Windows.Forms.Label LblNationality;
        private System.Windows.Forms.TextBox TbBirthplace;
        private System.Windows.Forms.Label LblBirthplace;
        private System.Windows.Forms.ComboBox CbToSearch;
        private System.Windows.Forms.TextBox TbDateOfBirth;
        private System.Windows.Forms.TextBox TbFamilyName;
        private System.Windows.Forms.TextBox TbName;
        private System.Windows.Forms.TextBox TbId;
        private System.Windows.Forms.Label LblDateOfBirth;
        private System.Windows.Forms.Label LblFamilyName;
        private System.Windows.Forms.Label LblName;
        private System.Windows.Forms.Label LblId;
        private System.Windows.Forms.Label LblSearchEmployee;
        private System.Windows.Forms.TextBox TbEmployeeName;
        private System.Windows.Forms.Button bt_editEmployee;
        private System.Windows.Forms.Button bt_addEmployee;
        private System.Windows.Forms.Panel pnl_createEmployee;
        private System.Windows.Forms.TextBox tb_createPassword;
        private System.Windows.Forms.Label lb_password;
        private System.Windows.Forms.TextBox tb_createUsername;
        private System.Windows.Forms.Label lb_username;
        private System.Windows.Forms.Label lb_createRank;
        private System.Windows.Forms.ComboBox cb_createRank;
        private System.Windows.Forms.DateTimePicker dp_birthDay;
        private System.Windows.Forms.Button bt_addUser;
        private System.Windows.Forms.TextBox tb_zipCode;
        private System.Windows.Forms.TextBox tb_adress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_bsn;
        private System.Windows.Forms.TextBox tb_languages;
        private System.Windows.Forms.TextBox tb_nationality;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_birthplace;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_lastName;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.TextBox tb_id;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel pnl_editEmployee;
        private System.Windows.Forms.Button bt_selectUser;
        private System.Windows.Forms.Button bt_refresh;
        private System.Windows.Forms.ListBox lb_allEmployees;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button bt_saveChanges;
        private System.Windows.Forms.TextBox tb_editPassword;
        private System.Windows.Forms.Button bt_fired;
        private System.Windows.Forms.TextBox tb_employeeStatus;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_editUsername;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cb_editPosition;
        private System.Windows.Forms.DateTimePicker dp_editDate;
        private System.Windows.Forms.TextBox tb_editZip;
        private System.Windows.Forms.TextBox tb_editHomeAddress;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tb_editBsn;
        private System.Windows.Forms.TextBox tb_editLanguages;
        private System.Windows.Forms.TextBox tb_editNationality;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tb_editBirthplace;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tb_editSurname;
        private System.Windows.Forms.TextBox tb_editName;
        private System.Windows.Forms.TextBox tb_editId;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ListBox LbxEmployeeNames;
        private System.Windows.Forms.Panel panelCheckPresentAbsent;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.MonthCalendar monthCalendar2;
        private System.Windows.Forms.Button BtnMarkPAS;
        private System.Windows.Forms.RadioButton RbSick;
        private System.Windows.Forms.RadioButton RbAbsent;
        private System.Windows.Forms.RadioButton RbPresent;
        private System.Windows.Forms.Button BtnMark;
    }
}