﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Media_Bazaar
{
    public partial class Search : Form
    {
        public Search()
        {
            InitializeComponent();
        }

        MySqlConnection conn = new MySqlConnection("server=localhost;database=media_bazzar;uid=root;password=;");

        string sql = "SELECT name FROM employees;";

        private void DbConnect(string string1, MySqlConnection conn1)
        {
            MySqlCommand cmd1 = new MySqlCommand(string1, conn1);
        }

        private void Search_Load(object sender, EventArgs e)
        {
            DbConnect(sql, conn);
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            conn.Open();
            string searching = "";
            string toSearch = CbToSearch.Text;
            if (toSearch == "FirstName")
            {
                searching = "name";
            }
            else if (toSearch == "LastName")
            {
                searching = "surname";
            }
            string name = TbEmployeeName.Text;
            string query = "SELECT * FROM employees WHERE " + searching + " = '" + name + "' ";
            MySqlCommand cmd2 = new MySqlCommand(query, conn);
            MySqlDataReader dr = cmd2.ExecuteReader();
            if (dr.Read())
            {
                TbId.Text = (dr["id"].ToString());
                TbName.Text = (dr["name"].ToString());
                TbFamilyName.Text = (dr["surname"].ToString());
                TbDateOfBirth.Text = (dr["birthDate"].ToString());
                TbBirthplace.Text = (dr["birthPlace"].ToString());
                TbNationality.Text = (dr["nationality"].ToString());
                TbLanguages.Text = (dr["languages"].ToString());
                TbBsn.Text = (dr["bsn"].ToString());
                TbHomeAddress.Text = (dr["homeAddress"].ToString());
                TbZipCode.Text = (dr["zipCode"].ToString());
            }
            conn.Close();
        }

        private void BtnSelectEmployee_Click(object sender, EventArgs e)
        {

        }
    }
}
