﻿namespace Media_Bazaar
{
    partial class LogInF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bt_Login = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.tb_username = new System.Windows.Forms.TextBox();
            this.lb_statusMessage = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Location = new System.Drawing.Point(-2, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(406, 109);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.bt_Login);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.tb_password);
            this.panel2.Controls.Add(this.tb_username);
            this.panel2.Location = new System.Drawing.Point(49, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(270, 260);
            this.panel2.TabIndex = 1;
            // 
            // bt_Login
            // 
            this.bt_Login.BackColor = System.Drawing.Color.LightSkyBlue;
            this.bt_Login.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bt_Login.Location = new System.Drawing.Point(94, 202);
            this.bt_Login.Name = "bt_Login";
            this.bt_Login.Size = new System.Drawing.Size(83, 23);
            this.bt_Login.TabIndex = 3;
            this.bt_Login.Text = "LogIn";
            this.bt_Login.UseVisualStyleBackColor = false;
            this.bt_Login.Click += new System.EventHandler(this.bt_Login_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = global::Media_Bazaar.Properties.Resources.User;
            this.pictureBox1.Location = new System.Drawing.Point(94, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(83, 74);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // tb_password
            // 
            this.tb_password.Location = new System.Drawing.Point(36, 159);
            this.tb_password.Name = "tb_password";
            this.tb_password.Size = new System.Drawing.Size(198, 20);
            this.tb_password.TabIndex = 1;
            this.tb_password.Text = "Password";
            // 
            // tb_username
            // 
            this.tb_username.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_username.Location = new System.Drawing.Point(36, 120);
            this.tb_username.Name = "tb_username";
            this.tb_username.Size = new System.Drawing.Size(198, 20);
            this.tb_username.TabIndex = 0;
            this.tb_username.Text = "Username";
            // 
            // lb_statusMessage
            // 
            this.lb_statusMessage.AutoSize = true;
            this.lb_statusMessage.ForeColor = System.Drawing.Color.DarkRed;
            this.lb_statusMessage.Location = new System.Drawing.Point(119, 326);
            this.lb_statusMessage.Name = "lb_statusMessage";
            this.lb_statusMessage.Size = new System.Drawing.Size(0, 13);
            this.lb_statusMessage.TabIndex = 2;
            // 
            // LogInF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(379, 438);
            this.Controls.Add(this.lb_statusMessage);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LogInF";
            this.Text = "Username";
            this.Load += new System.EventHandler(this.LogInF_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tb_password;
        private System.Windows.Forms.TextBox tb_username;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button bt_Login;
        private System.Windows.Forms.Label lb_statusMessage;
    }
}

