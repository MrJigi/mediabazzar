﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Media_Bazaar
{
    class DataAccessWS
    {
        private MySqlConnection conn = new MySqlConnection("server=localhost;database=media_bazzar;uid=root;password=;");
        private string sql = "SELECT date FROM shifts;";
        private string sql2 = "SELECT name FROM employees;";


        public DataAccessWS()
        {
            MySqlCommand cmd1 = new MySqlCommand(sql, conn);
            MySqlCommand cmd2 = new MySqlCommand(sql2, conn);
        }

        public void AssignEditWorkShift(string query)
        {
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(query, conn);
            MySqlDataReader dr = cmd.ExecuteReader();
            conn.Close();
        }

        public List<string> GetAssignedEmployees(string query)
        {
            List<string> employeeNames = new List<string>();
            string buggedName;
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(query, conn);
            MySqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                buggedName = dr["employeeName"].ToString();
                foreach (var item in dr)
                {
                    employeeNames.Add(dr["employeeName"].ToString());
                }
                employeeNames.Add(buggedName);
            }
            conn.Close();
            return employeeNames;
        }
    }
}
