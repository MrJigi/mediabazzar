﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBazzar.Models
{
    public enum Position
    {
        HR,
        Worker,
        UpperManagment,
        FloorManager,
        DepoEmployee
    }
    public class User : INotifyPropertyChanged
    {
        private int _id;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                NotifyPropertyChanged("Id");
            }
        }
        private string _username;
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                NotifyPropertyChanged("Username");
            }
        }
        private string _passwordHash;
        public string PasswordHash
        {
            get
            {
                return _passwordHash;
            }
            set
            {
                _passwordHash = value;
                NotifyPropertyChanged("PasswordHash");
            }
        }
        // should be enum, string for now
        private Position _currPosition;
        public Position CurrPosition
        {
            get
            {
                return _currPosition;
            }
            set
            {
                _currPosition = value;
                NotifyPropertyChanged("CurrPosition");
            }
        }
        private bool _fired;
        public bool Fired
        {
            get
            {
                return _fired;
            }
            set
            {
                _fired = value;
                NotifyPropertyChanged("Fired");
            }
        }
        private bool _toBeFired;
        public bool ToBeFired
        {
            get
            {
                return _toBeFired;
            }
            set
            {
                _toBeFired = value;
                NotifyPropertyChanged("ToBeFired");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyname = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }
    }
}
