﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media_Bazaar
{
    public class stock
    {
        private int id;
        private string name;
        private int quantity;
        private string description;
        private float purchaseValue;
        private string state;
        private string type;


        public int getId { get { return this.id; } }
        public string getName { get { return this.name; } }
        public int getQuantity { get { return this.quantity; } }
        public string getDescription { get { return this.description; } }

        public float getpurchaseValue { get { return this.purchaseValue; } }

        public string getState { get { return this.state; } }
        public string getType { get { return this.type; } }


        public stock(int id, string name, int quantity, string description, float purchaseValue, string state, string type)
        {
            this.id = id;
            this.name = name;
            this.quantity = quantity;
            this.description = description;
            this.purchaseValue = purchaseValue;
            this.state = state;
            this.type = type;
        }

        public string FullInfo
        {
            get { return $"Id:{id} | Type:{type} | Name:{name} | Qty:{quantity} | Purchase Value:{purchaseValue}€ | State:{state} "; }
        }

        public string StatisticsInfo
        {
            get { return $"Type:{type} | Name:{name} | Qty:{quantity} | Price:{purchaseValue}€ "; }
        }
    }
}
