﻿using MediaBazzar.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBazzar.Database
{
    class StockRefillRepository : BaseRepository<StockRefill>
    {
        private readonly string table = "stock_refill";
        public StockRefillRepository(string connectionString)
            : base(connectionString)
        {
        }

        public override IEnumerable<StockRefill> GetAll()
        {
            using (var command = new MySqlCommand("SELECT * FROM " + table))
            {
                return GetAll(command);
            }
        }
        public override StockRefill GetById(string id)
        {
            using (var command = new MySqlCommand("SELECT * FROM " + table + " WHERE Id=@id"))
            {
                command.Parameters.AddWithValue("@id", id);
                return GetById(command);
            }
        }

        public override void Add(StockRefill record)
        {
            using (var command = new MySqlCommand("INSERT INTO " + table + " (stockId, accepted) " +
                "VALUES(@stockId, @accepted)"))
            {
                Fill(command, record);
                Save(command);
            }
        }

        public override void Update(StockRefill record)
        {
            using (var command = new MySqlCommand("UPDATE " + table + " SET stockId=@stockId, accepted=@accepted " +
                "WHERE Id=@id"))
            {
                Fill(command, record);
                command.Parameters.AddWithValue("@id", record.Id);
                Save(command);
            }
        }

        public override void Delete(string id)
        {
            Delete(table, id);
        }

        public override StockRefill Populate(MySqlDataReader reader)
        {
            return new StockRefill
            {
                Id = reader.GetInt32(0),
                ItemId = reader.GetInt32(1),
                Accepted = reader.GetBoolean(2)
            };
        }

        public override void Fill(MySqlCommand command, StockRefill record)
        {
            command.Parameters.AddWithValue("@stockId", record.ItemId);
            command.Parameters.AddWithValue("@accepted", record.Accepted);
        }


    }
}
