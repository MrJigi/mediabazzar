﻿using MediaBazzar.Database;
using MediaBazzar.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MediaBazzar.ViewModels
{
    class UpManagementViewModel : ViewModelBase
    {
        public ICommand FireCommand { get; }
        public ICommand DeclineCommand { get; }
        public UserListViewModel UserList { get; set; }
        private BaseRepository<User> _userRepo;
        public UpManagementViewModel ()
        {
            UserList = new UserListViewModel();
            FireCommand = new RelayCommand(param => FireEmployee());
            DeclineCommand = new RelayCommand(param => DeclineFireRequest());
            _userRepo = new UserRepository(ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
        }

        private void FireEmployee()
        {
            UserList.SelectedUser.Fired = true;
            _userRepo.Update(UserList.SelectedUser);
        }
        
        private void DeclineFireRequest()
        {
            UserList.SelectedUser.ToBeFired = false;
            UserList.SelectedUser.Fired = false;
            _userRepo.Update(UserList.SelectedUser);
        }
    }
}
