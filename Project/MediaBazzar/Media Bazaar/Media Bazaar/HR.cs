﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Media_Bazaar
{
    public partial class HR : Form
    {
        public HR()
        {
            InitializeComponent();
            panelMainContent.Visible = true;
            panelMainContent.Dock = DockStyle.Fill;
            cb_createRank.DataSource = Enum.GetValues(typeof(User.pos));
            cb_editPosition.DataSource = Enum.GetValues(typeof(User.pos));
            dp_birthDay.Format = DateTimePickerFormat.Custom;
            dp_editDate.Format = DateTimePickerFormat.Custom;
            dp_birthDay.CustomFormat = "dd/MM/yyyy";
            dp_editDate.CustomFormat = "dd/MM/yyyy";
            
        }

        #region SideMenu

        private void btnWorkShifts_Click(object sender, EventArgs e)
        {
            panelViewAssignWorkShift.Visible = true;
            pnl_createEmployee.Visible = false;
            panelViewAssignWorkShift.Dock = DockStyle.Fill;
            panelMainContent.Visible = false;
            panelSearchEmployee.Visible = false;
            pnl_editEmployee.Visible = false;
            panelCheckPresentAbsent.Visible = false;
        }
        private void bt_addEmployee_Click(object sender, EventArgs e)
        {
            pnl_createEmployee.Visible = true;
            pnl_createEmployee.Dock = DockStyle.Fill;
            panelViewAssignWorkShift.Visible = false;
            panelMainContent.Visible = false;
            panelSearchEmployee.Visible = false;
            pnl_editEmployee.Visible = false;
            panelCheckPresentAbsent.Visible = false;
        }
        private void bt_editEmployee_Click(object sender, EventArgs e)
        {
            pnl_editEmployee.Visible = true;
            pnl_createEmployee.Visible = false;
            pnl_editEmployee.Dock = DockStyle.Fill;
            panelViewAssignWorkShift.Visible = false;
            panelMainContent.Visible = false;
            panelSearchEmployee.Visible = false;
            panelCheckPresentAbsent.Visible = false;
        }
        private void BtnMark_Click(object sender, EventArgs e)
        {
            panelCheckPresentAbsent.Visible = true;
            panelCheckPresentAbsent.Dock = DockStyle.Fill;
            panelMainContent.Visible = false;
            panelSearchEmployee.Visible = false;
            pnl_editEmployee.Visible = false;
            panelViewAssignWorkShift.Visible = false;
            pnl_createEmployee.Visible = false;
        }

        #endregion

        #region ViewAssignWorkShifts

        string morningShift = "08:00 - 12:00 ";
        string middayShift = "12:00 - 16:00 ";
        string nightShift = "16:00 - 20:00 ";
        string shift1;
        string shift2;
        string shift3;
        string name;
        string date;

        DataAccessWS accessWS = new DataAccessWS();

        private void BtnAssignWorkShifts_Click(object sender, EventArgs e)
        {
            ShiftTime();
            string query = "INSERT INTO `media_bazzar`.`shifts` (`employeeName`, `date`, `morningShift`, `middayShift`, `nightShift`) VALUES ('" + name + "', '" + date + "', '" + shift1 + "', '" + shift2 + "', '" + shift3 + "');";
            accessWS.AssignEditWorkShift(query);
            shift1 = "";
            shift2 = "";
            shift3 = "";
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            LbxWorkShifts.Items.Clear();
            string day = monthCalendar1.SelectionStart.Date.ToString();
            string day2 = day.Substring(0, 10);
            string query1 = "SELECT * FROM shifts WHERE date = '" + day2 + "' AND morningShift = 'yes'";
            string query2 = "SELECT * FROM shifts WHERE date = '" + day2 + "' AND middayShift = 'yes'";
            string query3 = "SELECT * FROM shifts WHERE date = '" + day2 + "' AND nightShift = 'yes'";
            List<string> querys = new List<string>();
            querys.Add(query1);
            querys.Add(query2);
            querys.Add(query3);
            for (int i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    LbxWorkShifts.Items.Add(morningShift);
                }
                else if (i == 1)
                {
                    LbxWorkShifts.Items.Add(middayShift);
                }
                else if (i == 2)
                {
                    LbxWorkShifts.Items.Add(nightShift);
                }
                List<string> assignedEmployees = accessWS.GetAssignedEmployees(querys[i]);
                foreach(var item in assignedEmployees)
                {
                    LbxWorkShifts.Items.Add(item);
                }
            }
        }

        private void ShiftTime()
        {
            name = TbShiftEmployee.Text;
            if (CbxMorningShift.Checked)
            {
                shift1 = "yes";
            }
            else
            {
                shift1 = "no";
            }

            if (CbxMiddayShift.Checked)
            {
                shift2 = "yes";
            }
            else
            {
                shift2 = "no";
            }
            if (CbxNightShift.Checked)
            {
                shift3 = "yes";
            }
            else
            {
                shift3 = "no";
            }
            string day = monthCalendar1.SelectionStart.Date.ToString();
            date = day.Substring(0, 10);
        }

        private void BtnEditWorkShifts_Click(object sender, EventArgs e)
        {
            ShiftTime();
            string query = "UPDATE `media_bazzar`.`shifts` SET `morningShift` = '" + shift1 + "', `middayShift` = '" + shift2 + "', `nightShift` = '" + shift3 + "' WHERE (`employeeName` = '" + name + "') AND (`date` = '" + date + "');";
            accessWS.AssignEditWorkShift(query);
        }

        private void BtnSearchEmployee_Click(object sender, EventArgs e)
        {
            panelViewAssignWorkShift.Visible = false;
            panelSearchEmployee.Visible = true;
            pnl_createEmployee.Visible = false;
            panelSearchEmployee.Dock = DockStyle.Fill;
        }

        private void LbxEmployeeNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            TbShiftEmployee.Text = LbxEmployeeNames.SelectedItem.ToString();
        }

        private void HR_Load(object sender, EventArgs e)
        {
            LbxEmployeeNames.Items.Clear();
            //List<User> UserList = new List<User>();
            User users = new User();
            List<User> users1 = new List<User>();
            users1 = users.GetAllEmployees();

            foreach (User info in users1)
            {
                LbxEmployeeNames.Items.Add(info.Name);
            }

            LbxEmployeeNames.DisplayMember = "FullInfo";
        }

        #endregion

        #region SearchEmployee

        DataAccesEmployee accesEmployee = new DataAccesEmployee();
        string selectedEmployee;

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            string searching = "";
            string toSearch = CbToSearch.Text;
            if (toSearch == "FirstName")
            {
                searching = "name";
            }
            else if (toSearch == "LastName")
            {
                searching = "surname";
            }
            string name = TbEmployeeName.Text;
            accesEmployee.GetEmployee(searching, name);
            TbId.Text = accesEmployee.id;
            TbName.Text = accesEmployee.name;
            TbFamilyName.Text = accesEmployee.surName;
            TbDateOfBirth.Text = accesEmployee.birthDate;
            TbBirthplace.Text = accesEmployee.birthPlace;
            TbNationality.Text = accesEmployee.nationality;
            TbLanguages.Text = accesEmployee.languages;
            TbBsn.Text = accesEmployee.bsn;
            TbHomeAddress.Text = accesEmployee.homeAddress;
            TbZipCode.Text = accesEmployee.zipCode;
        }

        private void BtnSelectEmployee_Click(object sender, EventArgs e)
        {
            selectedEmployee = TbName.Text;
            TbShiftEmployee.Text = selectedEmployee;
            panelSearchEmployee.Visible = false;
            panelViewAssignWorkShift.Visible = true;
        }


        #endregion

        #region variables
        private int id;
        private string names;
        private string surname;
        private string birthPlace;
        private string birthDate; //Idk need to set how to retrieve that date
        private string nationality;
        private string languages;
        private int bsn;
        private string position;
        private string homeAddress;
        private string zipCode;
        private string username;
        private string password;
        private bool IsFired;
        
        User Employee;
        List<User> UserList = new List<User>();
        DateTimePicker dateTime;

        private bool checkPass;


        #endregion

        #region AddEmployee
        private void bt_addUser_Click(object sender, EventArgs e)
        {
            check();
            if (checkPass)
            {
                Employee = new User(names, surname, birthDate.ToString(), birthPlace, nationality, languages, bsn, homeAddress, zipCode, position, username, password);
                //UserList = Employee
                if (Employee.checkIfPasswordExists(username, password))
                {
                    Employee.CreateUser();
                    MessageBox.Show("Add successfull"); // need to add a state if not successful;
                }
                else
                {
                    MessageBox.Show("A user with a username or password already exists");
                }
            }
            else
            {
                MessageBox.Show("All fields were not filled fully");
            }
        }
        #endregion

        #region EditEmployee
        private void bt_refresh_Click(object sender, EventArgs e)
        {
            lb_allEmployees.Items.Clear();
            //List<User> UserList = new List<User>();
            User users = new User();
            UserList = users.GetAllEmployees();

            foreach (User info in UserList)
            {
                lb_allEmployees.Items.Add(info.Id + " " + info.Name + " " + info.Surname);
            }

            lb_allEmployees.DisplayMember = "FullInfo";
            //UserList.Add(newEmployee.GetAllEmployees());
            // String.Concat(newEmployee.GetAllEmployees().Select(List < User >=> newEmployee.GetAllEmployees().ToString()));
            //UserList.Parse(typeof(User.pos), login.Rank, true)
        }

        private void bt_selectUser_Click(object sender, EventArgs e)
        {
            if (lb_allEmployees.SelectedIndex >= 0)
            {
                User users = new User();
                users.IndexOfEmp = lb_allEmployees.SelectedIndex;
                UserList = users.GetAllUserInfo();

                foreach (User info in UserList)
                {
                    tb_editId.Text = info.Id.ToString();
                    tb_editName.Text = info.Name;
                    tb_editSurname.Text = info.Surname;
                   // dateTime.CustomFormat = info.BirthDate;
                    dp_editDate.Value = Convert.ToDateTime(info.BirthDate);
                    tb_editBirthplace.Text = info.BirthPlace;
                    tb_editNationality.Text = info.Nationality;
                    tb_editLanguages.Text = info.Languages;
                    tb_editBsn.Text = info.Bsn.ToString();
                    tb_editHomeAddress.Text = info.HomeAdress;
                    tb_editZip.Text = info.ZipCode;
                    cb_editPosition.SelectedItem = info.Rank;
                    tb_editUsername.Text = info.Username;
                    tb_editPassword.Text = info.Password;
                    tb_employeeStatus.Text = changeToStatusName(info.Isfired);

                }
            }
            else
            {
                MessageBox.Show("Select an employee");
            }
            //lb_allEmployees.DisplayMember = "FullInfo";
        }

        private void bt_fired_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure if you want to fire an employee", "Warning", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {

                //IsFired = true; //Change to fired

                //User users = new User(name, surname, birthDate, birthPlace, nationality, languages, bsn, homeAddress, zipCode, position,IsFired, username, password);
                //User users = new User(id, IsFired);
                //checkChangedInfo();
                //IndexOfEmp = lb_allEmployees.SelectedIndex;
                //users.Isfired = true;

                foreach (User users in UserList)
                {
                    if(users.Id == (users.intexOfEmp  = lb_allEmployees.SelectedIndex + 1))
                    {
                        users.changeToFired();
                    }
                }
                //IsFired = users.Isfired;
                //UserList = users.GetAllUserInfo();

                //users.changeToFired();

            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }

        private void bt_saveChanges_Click(object sender, EventArgs e)
        {
            if (lb_allEmployees.SelectedIndex >= 0)
            {
                User users;
                checkChangedInfo();
                users = new User(names, surname, birthDate, birthPlace, nationality, languages, bsn, homeAddress, zipCode, position, username, password);
                if (checkPass)//
                {
                    users.IndexOfEmp = lb_allEmployees.SelectedIndex;
                    users.SaveAllUserChanges();
                    MessageBox.Show("Changes were succesfull");
                }
                else
                {
                    MessageBox.Show("Fill in all fields");
                }
            }
            else
            {
                MessageBox.Show("Select an employee");
            }
        }
        #endregion

        #region CheckableValues
        public void check()
        {
            checkPass = true;
            if (!string.IsNullOrEmpty(tb_name.Text))
            {
                names = tb_name.Text;
            }
            else
            {
                checkPass = false;
            }
            if (!string.IsNullOrEmpty(tb_lastName.Text))
            {
                surname = tb_lastName.Text.ToString();
            }
            else
            {
                checkPass = false;
            }
            if (!string.IsNullOrEmpty(tb_birthplace.Text))
            {
                birthPlace = tb_birthplace.Text;
            }
            else
            {
                checkPass = false;
            }
            if (!string.IsNullOrEmpty(dp_birthDay.Value.ToString()))
            {
                birthDate = dp_birthDay.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                checkPass = false;
            }
            //birthDate = dp_birthDay.Value.ToString();//Idk need to set how to retrieve that date

            if (!string.IsNullOrEmpty(tb_nationality.Text))
            {
                nationality = tb_nationality.Text;
            }
            else
            {
                checkPass = false;
            }

            if (!string.IsNullOrEmpty(tb_languages.Text))
            {
                languages = tb_languages.Text;
            }
            else
            {
                checkPass = false;
            }
            string bsns = tb_bsn.Text;
            if (IsDigitsOnly(bsns))
            {
                bsn = Convert.ToInt32(bsns);
                checkPass = true;
            }
            else
            {
                checkPass = false;
            }
            position = cb_createRank.Text;


            if (!string.IsNullOrEmpty(tb_adress.Text))
            {
                homeAddress = tb_adress.Text;
            }
            else
            {
                checkPass = false;
            }

            if (!string.IsNullOrEmpty(tb_zipCode.Text))
            {
                zipCode = tb_zipCode.Text;
            }
            else
            {
                checkPass = false;
            }

            if (!string.IsNullOrEmpty(tb_createUsername.Text))
            {
                username = tb_createUsername.Text;
            }
            else
            {
                checkPass = false;
            }

            if (!string.IsNullOrEmpty(tb_createPassword.Text))
            {
                password = tb_createPassword.Text;
            }
            else
            {
                checkPass = false;
            }
            /*name = tb_name.Text.ToString();
            surname = tb_lastName.Text.ToString();
            birthplace = tb_birthplace.Text;
            birthDate = dp_birthDay.Text; //Idk need to set how to retrieve that date
            nationality = tb_nationality.Text;
            languages = tb_languages.Text;
            string bsns = tb_bsn.Text;
            if (IsDigitsOnly(bsns)){
                bsn = Convert.ToInt32(bsns);
            }
            position = cb_createRank.Text;
            homeAddress = tb_adress.Text;
            zipCode = tb_zipCode.Text;
            username = tb_createUsername.Text;
            password = tb_createPassword.Text;*/

        }

        public void checkChangedInfo()
        {
            checkPass = true;
            if (!string.IsNullOrEmpty(tb_editName.Text))
                names = tb_editName.Text;
            else
            {
                checkPass = false;
            }

            if (!string.IsNullOrEmpty(tb_editSurname.Text))
                surname = tb_editSurname.Text.ToString();
            else
            {
                checkPass = false;
            }

            if (!string.IsNullOrEmpty(tb_editBirthplace.Text))
            {
                birthPlace = tb_editBirthplace.Text;
            }
            else
            {
                checkPass = false;
            }
            //Here is the date change
            //dp_editDate.Format = DateTimePickerFormat.Custom;
            //dp_birthDay.CustomFormat = "dd/MM/yyyy";
            if (!string.IsNullOrEmpty(dp_editDate.Value.ToString()))
            {
                birthDate = dp_editDate.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                checkPass = false;
            }
            //birthDate = tb_birthplace.Text; //Idk need to set how to retrieve that date

            if (!string.IsNullOrEmpty(tb_editNationality.Text))
            {
                nationality = tb_editNationality.Text;
            }
            else
            {
                checkPass = false;
            }

            if (!string.IsNullOrEmpty(tb_editLanguages.Text))
            {
                languages = tb_editLanguages.Text;
            }
            else
            {
                checkPass = false;
            }
            string bsns = tb_editBsn.Text;
            if (IsDigitsOnly(bsns) || !string.IsNullOrEmpty(bsns))
            {
                bsn = Convert.ToInt32(bsns);

            }
            else
            {
                checkPass = false;
            }

            position = cb_createRank.Text;


            if (!string.IsNullOrEmpty(tb_editHomeAddress.Text))
            {
                homeAddress = tb_editHomeAddress.Text;
            }
            else
            {
                checkPass = false;
            }

            if (!string.IsNullOrEmpty(tb_editZip.Text))
            {
                zipCode = tb_editZip.Text;
            }
            else
            {
                checkPass = false;
            }
            // IsFired = false;

            if (!string.IsNullOrEmpty(tb_editUsername.Text))
            {
                username = tb_editUsername.Text;
            }
            else
            {
                checkPass = false;
            }

            if (!string.IsNullOrEmpty(tb_editPassword.Text))
            {
                password = tb_editPassword.Text;
            }
            else
            {
                checkPass = false;
            }
        }

        public bool IsDigitsOnly(string message)
        {
            foreach (char c in message)
            {
                if (char.IsLetter(c))
                {
                    MessageBox.Show("There can't be letters in bsn");
                    return false;
                }

            }
            return true;

        }
        private string changeToStatusName(bool state)
        {
            string message;
            if (state)
            {
                return message = "Inactive";
            }
            else
            {
                return message = "Active";
            }

        }
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            this.bt_refresh_Click(null, null);
        }

        #endregion

        #region CheckPresentAbsent
        string mPresent = "no";
        string mdPresent = "no";
        string nPresent = "no";
        string mAbsent = "no";
        string mdAbsent = "no";
        string nAbsent = "no";
        string mSick = "no";
        string mdSick = "no";
        string nSick = "no";

        private void monthCalendar2_DateChanged(object sender, DateRangeEventArgs e)
        {
            listBox1.Items.Clear();
            string day = monthCalendar2.SelectionStart.Date.ToString();
            string day2 = day.Substring(0, 10);
            string query1 = "SELECT * FROM shifts WHERE date = '" + day2 + "' AND morningShift = 'yes'";
            string query2 = "SELECT * FROM shifts WHERE date = '" + day2 + "' AND middayShift = 'yes'";
            string query3 = "SELECT * FROM shifts WHERE date = '" + day2 + "' AND nightShift = 'yes'";
            List<string> querys = new List<string>();
            querys.Add(query1);
            querys.Add(query2);
            querys.Add(query3);
            for (int i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    listBox1.Items.Add(morningShift);
                }
                else if (i == 1)
                {
                    listBox1.Items.Add(middayShift);
                }
                else if (i == 2)
                {
                    listBox1.Items.Add(nightShift);
                }
                List<string> assignedEmployees = accessWS.GetAssignedEmployees(querys[i]);
                foreach (var item in assignedEmployees)
                {
                    listBox1.Items.Add(item);
                }
            }
        }

        private void BtnMarkPAS_Click(object sender, EventArgs e)
        {
            CheckedRadioButtons();
            string query = "UPDATE `media_bazzar`.`shifts` SET `mPresent` = '" + mPresent + "', `mdPresent` = '" + mdPresent + "', `nPresent` = '" + nPresent + "', `mAbsent` = '" + mAbsent + "', `mdAbsent` = '" + mdAbsent + "', `nAbsent` = '" + nAbsent + "', `mSick` = '" + mSick + "', `mdSick` = '" + mdSick + "', `nSick` = '" + nSick + "' WHERE (`employeeName` = '" + name + "') AND (`date` = '" + date + "')";
            accessWS.AssignEditWorkShift(query);
            mPresent = "no";
            mdPresent = "no";
            nPresent = "no";
            mAbsent = "no";
            mdAbsent = "no";
            nAbsent = "no";
            mSick = "no";
            mdSick = "no";
            nSick = "no";
        }

        private void CheckedRadioButtons()
        {
            string selectedEmployeeName = listBox1.SelectedItem.ToString();
            name = selectedEmployeeName;
            int indexOfMdShift = listBox1.Items.IndexOf(middayShift);
            int indexOfNShift = listBox1.Items.IndexOf(nightShift);
            int indexOfName = listBox1.Items.IndexOf(name);

            if (RbPresent.Checked)
            {
                if (indexOfName < indexOfMdShift)
                {
                    mPresent = "yes";
                }
                else if (indexOfName > indexOfNShift)
                {
                    nPresent = "yes";
                }
                else
                {
                    mdPresent = "yes";
                }
            }
            else if (RbAbsent.Checked)
            {
                if (indexOfName < indexOfMdShift)
                {
                    mAbsent = "yes";
                }
                else if (indexOfName > indexOfNShift)
                {
                    nAbsent = "yes";
                }
                else
                {
                    mdAbsent = "yes";
                }
            }
            else if (RbSick.Checked)
            {
                if (indexOfName < indexOfMdShift)
                {
                    mSick = "yes";
                }
                else if (indexOfName > indexOfNShift)
                {
                    nSick = "yes";
                }
                else
                {
                    mdSick = "yes";
                }
            }

            string day = monthCalendar1.SelectionStart.Date.ToString();
            date = day.Substring(0, 10);
        }

        #endregion
    }
}
