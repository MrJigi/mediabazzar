﻿using System;
using System.Windows.Forms;



namespace Media_Bazaar
{
    public partial class LogInF : Form
    {
        LogIn login;
        Form2 depo;
        manager manager;
        HR hr;
        LogInF logInForm;
        
        
        public LogInF()
        {
            InitializeComponent();
            depo = new Form2();
            manager = new manager();
            //Add depo and manager to activate the log in when data base is set
            
            tb_password.PasswordChar = '*';
            depo.Show();
            manager.Show();
            //logInForm = new Form1();
            
        }

        private void bt_Login_Click(object sender, EventArgs e)
        {
            depo = new Form2();
            manager = new manager();
            hr = new HR();
            lb_statusMessage.Text = "";
            string username = tb_username.Text;
            string password = tb_password.Text;
            string message;
            login = new LogIn(username, password);
            login.pass(username, password);

            if (login.LogInStatus == true)
            {
                logInForm = new LogInF();

                switch (Enum.Parse(typeof(User.pos), login.Rank, true))
                {
                    case User.pos.Administrator:

                        logInForm.Close();
                       // HR hr = new HR();
                        hr.Show();
                        // manager.Show();
                        //throw new Exception("Administration");
                        break;
                    case User.pos.Manager:
                        //  administration.Show();
                        
                        manager.Show();
                        //throw new Exception("Manager");
                        break;
                    case User.pos.Depo:
                        
                        depo.Show();
                        //throw new Exception("Depo");
                        break;
                    default:
                        MessageBox.Show("Rank Error");
                        break;
                }

                message = login.Message;
                // string rankShow = login.Rank.ToString() ;
                lb_statusMessage.Text = message;
            }
            
            message = login.Message;
           // string rankShow = login.Rank.ToString() ;
            lb_statusMessage.Text = message;
           // lb_rankShow.Text = rankShow;
        }

        private void LogInF_Load(object sender, EventArgs e)
        {
            manager.Show();
            HR hR = new HR();
            hR.Show();
        }
    }
}
