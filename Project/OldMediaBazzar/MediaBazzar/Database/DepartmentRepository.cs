﻿using MediaBazzar.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaBazzar.Database
{
    public class DepartmentRepository : BaseRepository<Department>
    {
        private readonly string table = "Departments";
        public DepartmentRepository(string connectionString)
            : base(connectionString)
        {
        }

        public override IEnumerable<Department> GetAll()
        {
            using (var command = new MySqlCommand("SELECT * FROM " + table))
            {
                return GetAll(command);
            }
        }
        public override Department GetById(string id)
        {
            using (var command = new MySqlCommand("SELECT * FROM " + table + " WHERE name=@id"))
            {
                command.Parameters.AddWithValue("@id", id);
                return GetById(command);
            }
        }

        public override void Add(Department record)
        {
            using (var command = new MySqlCommand("INSERT INTO " + table + " (name) " +
                "VALUES(@name)"))
            {
                Fill(command, record);
                Save(command);
            }
        }

        public override void Update(Department record)
        {
            using (var command = new MySqlCommand("UPDATE " + table + " SET name=@name " +
                "WHERE Id=@id"))
            {
                Fill(command, record);
                command.Parameters.AddWithValue("@id", record.Id);
                Save(command);
            }
        }

        public override void Delete(string id)
        {
            Delete(table, id);
        }

        public override Department Populate(MySqlDataReader reader)
        {
            return new Department
            {
                Id = reader.GetInt32(0),
                name = reader.GetString(1)
            };
        }

        public override void Fill(MySqlCommand command, Department record)
        {
            command.Parameters.AddWithValue("@name", record.name);
        }


    }
}

