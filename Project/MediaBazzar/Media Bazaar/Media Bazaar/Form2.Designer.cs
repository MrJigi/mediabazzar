﻿namespace Media_Bazaar
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.btnProductReq = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.panelProductDashboardSubMenu = new System.Windows.Forms.Panel();
            this.btnMenuModifyProduct = new System.Windows.Forms.Button();
            this.btnMenuAddProduct = new System.Windows.Forms.Button();
            this.btnProductDashboard = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelMainContent = new System.Windows.Forms.Panel();
            this.panelProductDash = new System.Windows.Forms.Panel();
            this.panelAddProduct = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxAddProductType = new System.Windows.Forms.TextBox();
            this.btnAddProductClearFields = new System.Windows.Forms.Button();
            this.btnBackFromAddProduct = new System.Windows.Forms.Button();
            this.btnAddProductToDb = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxAddProductValue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxAddProductDescription = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxAddProductQuantity = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAddProductName = new System.Windows.Forms.TextBox();
            this.panelSearchSpecificProduct = new System.Windows.Forms.Panel();
            this.LbMultipleProducts = new System.Windows.Forms.ListBox();
            this.TbxState = new System.Windows.Forms.TextBox();
            this.LblState = new System.Windows.Forms.Label();
            this.TbxPurchaseValue = new System.Windows.Forms.TextBox();
            this.LblPurchaseValue = new System.Windows.Forms.Label();
            this.CbxToSearch = new System.Windows.Forms.ComboBox();
            this.TbxDescription = new System.Windows.Forms.TextBox();
            this.TbxQuantity = new System.Windows.Forms.TextBox();
            this.TbxName = new System.Windows.Forms.TextBox();
            this.TbxId = new System.Windows.Forms.TextBox();
            this.LblDescription = new System.Windows.Forms.Label();
            this.LblQuantity = new System.Windows.Forms.Label();
            this.LblName = new System.Windows.Forms.Label();
            this.LblId = new System.Windows.Forms.Label();
            this.LblProduct = new System.Windows.Forms.Label();
            this.TbProductnq = new System.Windows.Forms.TextBox();
            this.BtnSearchSpecificProduct = new System.Windows.Forms.Button();
            this.panelModifyProduct = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxModifyProductType = new System.Windows.Forms.TextBox();
            this.dateTimePickerModStock = new System.Windows.Forms.DateTimePicker();
            this.radioButtonInactiveProduct = new System.Windows.Forms.RadioButton();
            this.radioButtonActiveProduct = new System.Windows.Forms.RadioButton();
            this.btnClearModifyProductFields = new System.Windows.Forms.Button();
            this.btnBackFromModifyProduct = new System.Windows.Forms.Button();
            this.btnModifyProductToDb = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxModifyProductValue = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxModifyProductDescription = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxModifyProductQuantity = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxModifyProductName = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelNotificationCount = new System.Windows.Forms.Panel();
            this.labelActiveNotif = new System.Windows.Forms.Label();
            this.btnNotificationReq = new System.Windows.Forms.Button();
            this.panelInfoRequestNotification = new System.Windows.Forms.Panel();
            this.btnCloseNotificationPop = new System.Windows.Forms.Button();
            this.labelNotificationPopText = new System.Windows.Forms.Label();
            this.panelProductRequests = new System.Windows.Forms.Panel();
            this.btnMarkPending = new System.Windows.Forms.Button();
            this.btnViewClosedReq = new System.Windows.Forms.Button();
            this.btnViewPendingReq = new System.Windows.Forms.Button();
            this.btnViewNewProductReq = new System.Windows.Forms.Button();
            this.btnCloseProductReq = new System.Windows.Forms.Button();
            this.listBoxProductReq = new System.Windows.Forms.ListBox();
            this.btnNavRemoveProduct = new System.Windows.Forms.Button();
            this.btnNavModifyProduct = new System.Windows.Forms.Button();
            this.btnNavAddProduct = new System.Windows.Forms.Button();
            this.btnViewAvailableProducts = new System.Windows.Forms.Button();
            this.btnViewUnavailableProducts = new System.Windows.Forms.Button();
            this.btnViewAllProducts = new System.Windows.Forms.Button();
            this.listBoxProducts = new System.Windows.Forms.ListBox();
            this.BtnSearchProduct = new System.Windows.Forms.Button();
            this.btnViewStockDescription = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.timerCheckNotif = new System.Windows.Forms.Timer(this.components);
            this.panelSideMenu.SuspendLayout();
            this.panelProductDashboardSubMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelMainContent.SuspendLayout();
            this.panelProductDash.SuspendLayout();
            this.panelAddProduct.SuspendLayout();
            this.panelSearchSpecificProduct.SuspendLayout();
            this.panelModifyProduct.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelNotificationCount.SuspendLayout();
            this.panelInfoRequestNotification.SuspendLayout();
            this.panelProductRequests.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panelSideMenu.Controls.Add(this.btnProductReq);
            this.panelSideMenu.Controls.Add(this.btnLogout);
            this.panelSideMenu.Controls.Add(this.panelProductDashboardSubMenu);
            this.panelSideMenu.Controls.Add(this.btnProductDashboard);
            this.panelSideMenu.Controls.Add(this.panel1);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(228, 561);
            this.panelSideMenu.TabIndex = 3;
            // 
            // btnProductReq
            // 
            this.btnProductReq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProductReq.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProductReq.FlatAppearance.BorderSize = 0;
            this.btnProductReq.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnProductReq.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnProductReq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductReq.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnProductReq.Location = new System.Drawing.Point(0, 232);
            this.btnProductReq.Name = "btnProductReq";
            this.btnProductReq.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnProductReq.Size = new System.Drawing.Size(228, 40);
            this.btnProductReq.TabIndex = 4;
            this.btnProductReq.Text = "Stock Requests";
            this.btnProductReq.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProductReq.UseVisualStyleBackColor = true;
            this.btnProductReq.Click += new System.EventHandler(this.btnProductReq_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnLogout.Location = new System.Drawing.Point(0, 521);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnLogout.Size = new System.Drawing.Size(228, 40);
            this.btnLogout.TabIndex = 3;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // panelProductDashboardSubMenu
            // 
            this.panelProductDashboardSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(30)))));
            this.panelProductDashboardSubMenu.Controls.Add(this.btnMenuModifyProduct);
            this.panelProductDashboardSubMenu.Controls.Add(this.btnMenuAddProduct);
            this.panelProductDashboardSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelProductDashboardSubMenu.Location = new System.Drawing.Point(0, 140);
            this.panelProductDashboardSubMenu.Name = "panelProductDashboardSubMenu";
            this.panelProductDashboardSubMenu.Size = new System.Drawing.Size(228, 92);
            this.panelProductDashboardSubMenu.TabIndex = 2;
            // 
            // btnMenuModifyProduct
            // 
            this.btnMenuModifyProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(30)))));
            this.btnMenuModifyProduct.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMenuModifyProduct.FlatAppearance.BorderSize = 0;
            this.btnMenuModifyProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(30)))));
            this.btnMenuModifyProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(30)))));
            this.btnMenuModifyProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenuModifyProduct.ForeColor = System.Drawing.Color.LightGray;
            this.btnMenuModifyProduct.Location = new System.Drawing.Point(0, 44);
            this.btnMenuModifyProduct.Name = "btnMenuModifyProduct";
            this.btnMenuModifyProduct.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnMenuModifyProduct.Size = new System.Drawing.Size(228, 44);
            this.btnMenuModifyProduct.TabIndex = 1;
            this.btnMenuModifyProduct.Text = "Modify Stock";
            this.btnMenuModifyProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenuModifyProduct.UseVisualStyleBackColor = false;
            // 
            // btnMenuAddProduct
            // 
            this.btnMenuAddProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(30)))));
            this.btnMenuAddProduct.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMenuAddProduct.FlatAppearance.BorderSize = 0;
            this.btnMenuAddProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(30)))));
            this.btnMenuAddProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(30)))));
            this.btnMenuAddProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenuAddProduct.ForeColor = System.Drawing.Color.LightGray;
            this.btnMenuAddProduct.Location = new System.Drawing.Point(0, 0);
            this.btnMenuAddProduct.Name = "btnMenuAddProduct";
            this.btnMenuAddProduct.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnMenuAddProduct.Size = new System.Drawing.Size(228, 44);
            this.btnMenuAddProduct.TabIndex = 0;
            this.btnMenuAddProduct.Text = "Add Stock";
            this.btnMenuAddProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenuAddProduct.UseVisualStyleBackColor = false;
            // 
            // btnProductDashboard
            // 
            this.btnProductDashboard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProductDashboard.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProductDashboard.FlatAppearance.BorderSize = 0;
            this.btnProductDashboard.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnProductDashboard.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnProductDashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductDashboard.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnProductDashboard.Location = new System.Drawing.Point(0, 100);
            this.btnProductDashboard.Name = "btnProductDashboard";
            this.btnProductDashboard.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnProductDashboard.Size = new System.Drawing.Size(228, 40);
            this.btnProductDashboard.TabIndex = 1;
            this.btnProductDashboard.Text = "Stock Dashboard";
            this.btnProductDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProductDashboard.UseVisualStyleBackColor = true;
            this.btnProductDashboard.Click += new System.EventHandler(this.btnProductDashboard_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 100);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(23, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 76);
            this.label2.TabIndex = 4;
            this.label2.Text = "MB";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // panelMainContent
            // 
            this.panelMainContent.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelMainContent.Controls.Add(this.panelProductDash);
            this.panelMainContent.Controls.Add(this.label3);
            this.panelMainContent.Controls.Add(this.label11);
            this.panelMainContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMainContent.Location = new System.Drawing.Point(228, 0);
            this.panelMainContent.Name = "panelMainContent";
            this.panelMainContent.Size = new System.Drawing.Size(756, 561);
            this.panelMainContent.TabIndex = 4;
            // 
            // panelProductDash
            // 
            this.panelProductDash.Controls.Add(this.panelAddProduct);
            this.panelProductDash.Controls.Add(this.panelSearchSpecificProduct);
            this.panelProductDash.Controls.Add(this.panelModifyProduct);
            this.panelProductDash.Controls.Add(this.panel2);
            this.panelProductDash.Controls.Add(this.panelInfoRequestNotification);
            this.panelProductDash.Controls.Add(this.panelProductRequests);
            this.panelProductDash.Controls.Add(this.btnNavRemoveProduct);
            this.panelProductDash.Controls.Add(this.btnNavModifyProduct);
            this.panelProductDash.Controls.Add(this.btnNavAddProduct);
            this.panelProductDash.Controls.Add(this.btnViewAvailableProducts);
            this.panelProductDash.Controls.Add(this.btnViewUnavailableProducts);
            this.panelProductDash.Controls.Add(this.btnViewAllProducts);
            this.panelProductDash.Controls.Add(this.listBoxProducts);
            this.panelProductDash.Controls.Add(this.BtnSearchProduct);
            this.panelProductDash.Controls.Add(this.btnViewStockDescription);
            this.panelProductDash.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelProductDash.Location = new System.Drawing.Point(0, 0);
            this.panelProductDash.Name = "panelProductDash";
            this.panelProductDash.Size = new System.Drawing.Size(756, 561);
            this.panelProductDash.TabIndex = 9;
            this.panelProductDash.Visible = false;
            // 
            // panelAddProduct
            // 
            this.panelAddProduct.Controls.Add(this.label13);
            this.panelAddProduct.Controls.Add(this.textBoxAddProductType);
            this.panelAddProduct.Controls.Add(this.btnAddProductClearFields);
            this.panelAddProduct.Controls.Add(this.btnBackFromAddProduct);
            this.panelAddProduct.Controls.Add(this.btnAddProductToDb);
            this.panelAddProduct.Controls.Add(this.label6);
            this.panelAddProduct.Controls.Add(this.textBoxAddProductValue);
            this.panelAddProduct.Controls.Add(this.label5);
            this.panelAddProduct.Controls.Add(this.textBoxAddProductDescription);
            this.panelAddProduct.Controls.Add(this.label4);
            this.panelAddProduct.Controls.Add(this.textBoxAddProductQuantity);
            this.panelAddProduct.Controls.Add(this.label1);
            this.panelAddProduct.Controls.Add(this.textBoxAddProductName);
            this.panelAddProduct.Location = new System.Drawing.Point(470, 505);
            this.panelAddProduct.Name = "panelAddProduct";
            this.panelAddProduct.Size = new System.Drawing.Size(694, 514);
            this.panelAddProduct.TabIndex = 7;
            this.panelAddProduct.Visible = false;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(343, 128);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 17);
            this.label13.TabIndex = 27;
            this.label13.Text = "Type";
            // 
            // textBoxAddProductType
            // 
            this.textBoxAddProductType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAddProductType.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxAddProductType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAddProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddProductType.Location = new System.Drawing.Point(261, 151);
            this.textBoxAddProductType.Name = "textBoxAddProductType";
            this.textBoxAddProductType.Size = new System.Drawing.Size(212, 26);
            this.textBoxAddProductType.TabIndex = 26;
            // 
            // btnAddProductClearFields
            // 
            this.btnAddProductClearFields.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddProductClearFields.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnAddProductClearFields.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddProductClearFields.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnAddProductClearFields.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnAddProductClearFields.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddProductClearFields.ForeColor = System.Drawing.Color.LightGray;
            this.btnAddProductClearFields.Location = new System.Drawing.Point(599, 26);
            this.btnAddProductClearFields.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddProductClearFields.Name = "btnAddProductClearFields";
            this.btnAddProductClearFields.Size = new System.Drawing.Size(80, 31);
            this.btnAddProductClearFields.TabIndex = 21;
            this.btnAddProductClearFields.Text = "Clear";
            this.btnAddProductClearFields.UseVisualStyleBackColor = false;
            this.btnAddProductClearFields.Click += new System.EventHandler(this.btnAddProductClearFields_Click);
            // 
            // btnBackFromAddProduct
            // 
            this.btnBackFromAddProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBackFromAddProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnBackFromAddProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBackFromAddProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnBackFromAddProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnBackFromAddProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackFromAddProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackFromAddProduct.ForeColor = System.Drawing.Color.LightGray;
            this.btnBackFromAddProduct.Location = new System.Drawing.Point(17, 26);
            this.btnBackFromAddProduct.Margin = new System.Windows.Forms.Padding(4);
            this.btnBackFromAddProduct.Name = "btnBackFromAddProduct";
            this.btnBackFromAddProduct.Size = new System.Drawing.Size(80, 31);
            this.btnBackFromAddProduct.TabIndex = 9;
            this.btnBackFromAddProduct.Text = "<<";
            this.btnBackFromAddProduct.UseVisualStyleBackColor = false;
            this.btnBackFromAddProduct.Click += new System.EventHandler(this.btnBackFromAddProduct_Click);
            // 
            // btnAddProductToDb
            // 
            this.btnAddProductToDb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddProductToDb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnAddProductToDb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddProductToDb.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnAddProductToDb.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnAddProductToDb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddProductToDb.ForeColor = System.Drawing.Color.LightGray;
            this.btnAddProductToDb.Location = new System.Drawing.Point(313, 452);
            this.btnAddProductToDb.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddProductToDb.Name = "btnAddProductToDb";
            this.btnAddProductToDb.Size = new System.Drawing.Size(109, 51);
            this.btnAddProductToDb.TabIndex = 8;
            this.btnAddProductToDb.Text = "Add Product";
            this.btnAddProductToDb.UseVisualStyleBackColor = false;
            this.btnAddProductToDb.Click += new System.EventHandler(this.btnAddProductToDb_Click);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(312, 383);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Purchase Value";
            // 
            // textBoxAddProductValue
            // 
            this.textBoxAddProductValue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAddProductValue.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxAddProductValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAddProductValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddProductValue.Location = new System.Drawing.Point(261, 405);
            this.textBoxAddProductValue.Name = "textBoxAddProductValue";
            this.textBoxAddProductValue.Size = new System.Drawing.Size(207, 26);
            this.textBoxAddProductValue.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(327, 257);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Description";
            // 
            // textBoxAddProductDescription
            // 
            this.textBoxAddProductDescription.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAddProductDescription.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxAddProductDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAddProductDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddProductDescription.Location = new System.Drawing.Point(194, 280);
            this.textBoxAddProductDescription.Multiline = true;
            this.textBoxAddProductDescription.Name = "textBoxAddProductDescription";
            this.textBoxAddProductDescription.Size = new System.Drawing.Size(355, 91);
            this.textBoxAddProductDescription.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(335, 193);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Quantity";
            // 
            // textBoxAddProductQuantity
            // 
            this.textBoxAddProductQuantity.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAddProductQuantity.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxAddProductQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAddProductQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddProductQuantity.Location = new System.Drawing.Point(261, 216);
            this.textBoxAddProductQuantity.Name = "textBoxAddProductQuantity";
            this.textBoxAddProductQuantity.Size = new System.Drawing.Size(207, 26);
            this.textBoxAddProductQuantity.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(340, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            // 
            // textBoxAddProductName
            // 
            this.textBoxAddProductName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAddProductName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxAddProductName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAddProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddProductName.Location = new System.Drawing.Point(261, 88);
            this.textBoxAddProductName.Name = "textBoxAddProductName";
            this.textBoxAddProductName.Size = new System.Drawing.Size(207, 26);
            this.textBoxAddProductName.TabIndex = 0;
            // 
            // panelSearchSpecificProduct
            // 
            this.panelSearchSpecificProduct.Controls.Add(this.LbMultipleProducts);
            this.panelSearchSpecificProduct.Controls.Add(this.TbxState);
            this.panelSearchSpecificProduct.Controls.Add(this.LblState);
            this.panelSearchSpecificProduct.Controls.Add(this.TbxPurchaseValue);
            this.panelSearchSpecificProduct.Controls.Add(this.LblPurchaseValue);
            this.panelSearchSpecificProduct.Controls.Add(this.CbxToSearch);
            this.panelSearchSpecificProduct.Controls.Add(this.TbxDescription);
            this.panelSearchSpecificProduct.Controls.Add(this.TbxQuantity);
            this.panelSearchSpecificProduct.Controls.Add(this.TbxName);
            this.panelSearchSpecificProduct.Controls.Add(this.TbxId);
            this.panelSearchSpecificProduct.Controls.Add(this.LblDescription);
            this.panelSearchSpecificProduct.Controls.Add(this.LblQuantity);
            this.panelSearchSpecificProduct.Controls.Add(this.LblName);
            this.panelSearchSpecificProduct.Controls.Add(this.LblId);
            this.panelSearchSpecificProduct.Controls.Add(this.LblProduct);
            this.panelSearchSpecificProduct.Controls.Add(this.TbProductnq);
            this.panelSearchSpecificProduct.Controls.Add(this.BtnSearchSpecificProduct);
            this.panelSearchSpecificProduct.Location = new System.Drawing.Point(734, 232);
            this.panelSearchSpecificProduct.Name = "panelSearchSpecificProduct";
            this.panelSearchSpecificProduct.Size = new System.Drawing.Size(707, 400);
            this.panelSearchSpecificProduct.TabIndex = 18;
            this.panelSearchSpecificProduct.Visible = false;
            // 
            // LbMultipleProducts
            // 
            this.LbMultipleProducts.FormattingEnabled = true;
            this.LbMultipleProducts.ItemHeight = 16;
            this.LbMultipleProducts.Location = new System.Drawing.Point(460, 122);
            this.LbMultipleProducts.Name = "LbMultipleProducts";
            this.LbMultipleProducts.Size = new System.Drawing.Size(227, 244);
            this.LbMultipleProducts.TabIndex = 57;
            // 
            // TbxState
            // 
            this.TbxState.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbxState.Location = new System.Drawing.Point(209, 332);
            this.TbxState.Name = "TbxState";
            this.TbxState.Size = new System.Drawing.Size(225, 28);
            this.TbxState.TabIndex = 56;
            // 
            // LblState
            // 
            this.LblState.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblState.Location = new System.Drawing.Point(22, 335);
            this.LblState.Name = "LblState";
            this.LblState.Size = new System.Drawing.Size(160, 28);
            this.LblState.TabIndex = 55;
            this.LblState.Text = "State:";
            // 
            // TbxPurchaseValue
            // 
            this.TbxPurchaseValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbxPurchaseValue.Location = new System.Drawing.Point(209, 290);
            this.TbxPurchaseValue.Name = "TbxPurchaseValue";
            this.TbxPurchaseValue.Size = new System.Drawing.Size(225, 28);
            this.TbxPurchaseValue.TabIndex = 54;
            // 
            // LblPurchaseValue
            // 
            this.LblPurchaseValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPurchaseValue.Location = new System.Drawing.Point(22, 293);
            this.LblPurchaseValue.Name = "LblPurchaseValue";
            this.LblPurchaseValue.Size = new System.Drawing.Size(160, 23);
            this.LblPurchaseValue.TabIndex = 53;
            this.LblPurchaseValue.Text = "Purchase Value:";
            // 
            // CbxToSearch
            // 
            this.CbxToSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbxToSearch.FormattingEnabled = true;
            this.CbxToSearch.Items.AddRange(new object[] {
            "Name",
            "Quantity"});
            this.CbxToSearch.Location = new System.Drawing.Point(118, 64);
            this.CbxToSearch.Name = "CbxToSearch";
            this.CbxToSearch.Size = new System.Drawing.Size(121, 30);
            this.CbxToSearch.TabIndex = 52;
            // 
            // TbxDescription
            // 
            this.TbxDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbxDescription.Location = new System.Drawing.Point(209, 248);
            this.TbxDescription.Name = "TbxDescription";
            this.TbxDescription.Size = new System.Drawing.Size(225, 28);
            this.TbxDescription.TabIndex = 51;
            // 
            // TbxQuantity
            // 
            this.TbxQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbxQuantity.Location = new System.Drawing.Point(209, 208);
            this.TbxQuantity.Name = "TbxQuantity";
            this.TbxQuantity.Size = new System.Drawing.Size(225, 28);
            this.TbxQuantity.TabIndex = 50;
            // 
            // TbxName
            // 
            this.TbxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbxName.Location = new System.Drawing.Point(209, 164);
            this.TbxName.Name = "TbxName";
            this.TbxName.Size = new System.Drawing.Size(225, 28);
            this.TbxName.TabIndex = 49;
            // 
            // TbxId
            // 
            this.TbxId.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbxId.Location = new System.Drawing.Point(209, 122);
            this.TbxId.Name = "TbxId";
            this.TbxId.Size = new System.Drawing.Size(225, 28);
            this.TbxId.TabIndex = 48;
            // 
            // LblDescription
            // 
            this.LblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDescription.Location = new System.Drawing.Point(22, 251);
            this.LblDescription.Name = "LblDescription";
            this.LblDescription.Size = new System.Drawing.Size(160, 23);
            this.LblDescription.TabIndex = 47;
            this.LblDescription.Text = "Description:";
            // 
            // LblQuantity
            // 
            this.LblQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblQuantity.Location = new System.Drawing.Point(22, 209);
            this.LblQuantity.Name = "LblQuantity";
            this.LblQuantity.Size = new System.Drawing.Size(160, 23);
            this.LblQuantity.TabIndex = 46;
            this.LblQuantity.Text = "Quantity:";
            // 
            // LblName
            // 
            this.LblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblName.Location = new System.Drawing.Point(22, 167);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(160, 23);
            this.LblName.TabIndex = 45;
            this.LblName.Text = "Name:";
            // 
            // LblId
            // 
            this.LblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblId.Location = new System.Drawing.Point(22, 125);
            this.LblId.Name = "LblId";
            this.LblId.Size = new System.Drawing.Size(160, 23);
            this.LblId.TabIndex = 44;
            this.LblId.Text = "ID:";
            // 
            // LblProduct
            // 
            this.LblProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProduct.Location = new System.Drawing.Point(22, 67);
            this.LblProduct.Name = "LblProduct";
            this.LblProduct.Size = new System.Drawing.Size(200, 23);
            this.LblProduct.TabIndex = 42;
            this.LblProduct.Text = "Product:";
            // 
            // TbProductnq
            // 
            this.TbProductnq.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbProductnq.Location = new System.Drawing.Point(245, 64);
            this.TbProductnq.Name = "TbProductnq";
            this.TbProductnq.Size = new System.Drawing.Size(100, 28);
            this.TbProductnq.TabIndex = 43;
            // 
            // BtnSearchSpecificProduct
            // 
            this.BtnSearchSpecificProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSearchSpecificProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSearchSpecificProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSearchSpecificProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnSearchSpecificProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSearchSpecificProduct.ForeColor = System.Drawing.Color.LightGray;
            this.BtnSearchSpecificProduct.Location = new System.Drawing.Point(390, 59);
            this.BtnSearchSpecificProduct.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSearchSpecificProduct.Name = "BtnSearchSpecificProduct";
            this.BtnSearchSpecificProduct.Size = new System.Drawing.Size(109, 51);
            this.BtnSearchSpecificProduct.TabIndex = 18;
            this.BtnSearchSpecificProduct.Text = "Search";
            this.BtnSearchSpecificProduct.UseVisualStyleBackColor = false;
            this.BtnSearchSpecificProduct.Click += new System.EventHandler(this.BtnSearchSpecificProduct_Click);
            // 
            // panelModifyProduct
            // 
            this.panelModifyProduct.Controls.Add(this.label12);
            this.panelModifyProduct.Controls.Add(this.textBoxModifyProductType);
            this.panelModifyProduct.Controls.Add(this.dateTimePickerModStock);
            this.panelModifyProduct.Controls.Add(this.radioButtonInactiveProduct);
            this.panelModifyProduct.Controls.Add(this.radioButtonActiveProduct);
            this.panelModifyProduct.Controls.Add(this.btnClearModifyProductFields);
            this.panelModifyProduct.Controls.Add(this.btnBackFromModifyProduct);
            this.panelModifyProduct.Controls.Add(this.btnModifyProductToDb);
            this.panelModifyProduct.Controls.Add(this.label7);
            this.panelModifyProduct.Controls.Add(this.textBoxModifyProductValue);
            this.panelModifyProduct.Controls.Add(this.label8);
            this.panelModifyProduct.Controls.Add(this.textBoxModifyProductDescription);
            this.panelModifyProduct.Controls.Add(this.label9);
            this.panelModifyProduct.Controls.Add(this.textBoxModifyProductQuantity);
            this.panelModifyProduct.Controls.Add(this.label10);
            this.panelModifyProduct.Controls.Add(this.textBoxModifyProductName);
            this.panelModifyProduct.Cursor = System.Windows.Forms.Cursors.Default;
            this.panelModifyProduct.Location = new System.Drawing.Point(655, 60);
            this.panelModifyProduct.Name = "panelModifyProduct";
            this.panelModifyProduct.Size = new System.Drawing.Size(667, 493);
            this.panelModifyProduct.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(171, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 17);
            this.label12.TabIndex = 25;
            this.label12.Text = "Type";
            // 
            // textBoxModifyProductType
            // 
            this.textBoxModifyProductType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxModifyProductType.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxModifyProductType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxModifyProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxModifyProductType.Location = new System.Drawing.Point(222, 65);
            this.textBoxModifyProductType.Name = "textBoxModifyProductType";
            this.textBoxModifyProductType.Size = new System.Drawing.Size(288, 26);
            this.textBoxModifyProductType.TabIndex = 24;
            // 
            // dateTimePickerModStock
            // 
            this.dateTimePickerModStock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dateTimePickerModStock.CalendarForeColor = System.Drawing.Color.WhiteSmoke;
            this.dateTimePickerModStock.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.dateTimePickerModStock.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(60)))));
            this.dateTimePickerModStock.CalendarTitleForeColor = System.Drawing.Color.WhiteSmoke;
            this.dateTimePickerModStock.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerModStock.Location = new System.Drawing.Point(292, 311);
            this.dateTimePickerModStock.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerModStock.Name = "dateTimePickerModStock";
            this.dateTimePickerModStock.Size = new System.Drawing.Size(122, 23);
            this.dateTimePickerModStock.TabIndex = 23;
            this.dateTimePickerModStock.Visible = false;
            // 
            // radioButtonInactiveProduct
            // 
            this.radioButtonInactiveProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioButtonInactiveProduct.AutoSize = true;
            this.radioButtonInactiveProduct.Location = new System.Drawing.Point(292, 284);
            this.radioButtonInactiveProduct.Name = "radioButtonInactiveProduct";
            this.radioButtonInactiveProduct.Size = new System.Drawing.Size(127, 21);
            this.radioButtonInactiveProduct.TabIndex = 22;
            this.radioButtonInactiveProduct.TabStop = true;
            this.radioButtonInactiveProduct.Text = "Prodcut Inactive";
            this.radioButtonInactiveProduct.UseVisualStyleBackColor = true;
            // 
            // radioButtonActiveProduct
            // 
            this.radioButtonActiveProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioButtonActiveProduct.AutoSize = true;
            this.radioButtonActiveProduct.Location = new System.Drawing.Point(292, 261);
            this.radioButtonActiveProduct.Name = "radioButtonActiveProduct";
            this.radioButtonActiveProduct.Size = new System.Drawing.Size(117, 21);
            this.radioButtonActiveProduct.TabIndex = 21;
            this.radioButtonActiveProduct.TabStop = true;
            this.radioButtonActiveProduct.Text = "Product Active";
            this.radioButtonActiveProduct.UseVisualStyleBackColor = true;
            // 
            // btnClearModifyProductFields
            // 
            this.btnClearModifyProductFields.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClearModifyProductFields.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnClearModifyProductFields.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearModifyProductFields.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnClearModifyProductFields.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnClearModifyProductFields.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearModifyProductFields.ForeColor = System.Drawing.Color.LightGray;
            this.btnClearModifyProductFields.Location = new System.Drawing.Point(572, 17);
            this.btnClearModifyProductFields.Margin = new System.Windows.Forms.Padding(4);
            this.btnClearModifyProductFields.Name = "btnClearModifyProductFields";
            this.btnClearModifyProductFields.Size = new System.Drawing.Size(80, 31);
            this.btnClearModifyProductFields.TabIndex = 20;
            this.btnClearModifyProductFields.Text = "Clear";
            this.btnClearModifyProductFields.UseVisualStyleBackColor = false;
            this.btnClearModifyProductFields.Click += new System.EventHandler(this.btnClearModifyProductFields_Click);
            // 
            // btnBackFromModifyProduct
            // 
            this.btnBackFromModifyProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBackFromModifyProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnBackFromModifyProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBackFromModifyProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnBackFromModifyProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnBackFromModifyProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackFromModifyProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackFromModifyProduct.ForeColor = System.Drawing.Color.LightGray;
            this.btnBackFromModifyProduct.Location = new System.Drawing.Point(13, 17);
            this.btnBackFromModifyProduct.Margin = new System.Windows.Forms.Padding(4);
            this.btnBackFromModifyProduct.Name = "btnBackFromModifyProduct";
            this.btnBackFromModifyProduct.Size = new System.Drawing.Size(76, 32);
            this.btnBackFromModifyProduct.TabIndex = 19;
            this.btnBackFromModifyProduct.Text = "<<";
            this.btnBackFromModifyProduct.UseVisualStyleBackColor = false;
            this.btnBackFromModifyProduct.Click += new System.EventHandler(this.btnBackFromModifyProduct_Click);
            // 
            // btnModifyProductToDb
            // 
            this.btnModifyProductToDb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnModifyProductToDb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnModifyProductToDb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModifyProductToDb.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnModifyProductToDb.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnModifyProductToDb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModifyProductToDb.ForeColor = System.Drawing.Color.LightGray;
            this.btnModifyProductToDb.Location = new System.Drawing.Point(298, 418);
            this.btnModifyProductToDb.Margin = new System.Windows.Forms.Padding(4);
            this.btnModifyProductToDb.Name = "btnModifyProductToDb";
            this.btnModifyProductToDb.Size = new System.Drawing.Size(109, 51);
            this.btnModifyProductToDb.TabIndex = 18;
            this.btnModifyProductToDb.Text = "Modify Product";
            this.btnModifyProductToDb.UseVisualStyleBackColor = false;
            this.btnModifyProductToDb.Click += new System.EventHandler(this.btnModifyProductToDb_Click);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(109, 237);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Purchase Value";
            // 
            // textBoxModifyProductValue
            // 
            this.textBoxModifyProductValue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxModifyProductValue.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxModifyProductValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxModifyProductValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxModifyProductValue.Location = new System.Drawing.Point(223, 232);
            this.textBoxModifyProductValue.Name = "textBoxModifyProductValue";
            this.textBoxModifyProductValue.Size = new System.Drawing.Size(288, 26);
            this.textBoxModifyProductValue.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(138, 167);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 17);
            this.label8.TabIndex = 15;
            this.label8.Text = "Description";
            // 
            // textBoxModifyProductDescription
            // 
            this.textBoxModifyProductDescription.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxModifyProductDescription.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxModifyProductDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxModifyProductDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxModifyProductDescription.Location = new System.Drawing.Point(223, 135);
            this.textBoxModifyProductDescription.Multiline = true;
            this.textBoxModifyProductDescription.Name = "textBoxModifyProductDescription";
            this.textBoxModifyProductDescription.Size = new System.Drawing.Size(288, 91);
            this.textBoxModifyProductDescription.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(156, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 17);
            this.label9.TabIndex = 13;
            this.label9.Text = "Quantity";
            // 
            // textBoxModifyProductQuantity
            // 
            this.textBoxModifyProductQuantity.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxModifyProductQuantity.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxModifyProductQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxModifyProductQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxModifyProductQuantity.Location = new System.Drawing.Point(223, 103);
            this.textBoxModifyProductQuantity.Name = "textBoxModifyProductQuantity";
            this.textBoxModifyProductQuantity.Size = new System.Drawing.Size(288, 26);
            this.textBoxModifyProductQuantity.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(171, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 17);
            this.label10.TabIndex = 11;
            this.label10.Text = "Name";
            // 
            // textBoxModifyProductName
            // 
            this.textBoxModifyProductName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxModifyProductName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxModifyProductName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxModifyProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxModifyProductName.Location = new System.Drawing.Point(222, 33);
            this.textBoxModifyProductName.Name = "textBoxModifyProductName";
            this.textBoxModifyProductName.Size = new System.Drawing.Size(288, 26);
            this.textBoxModifyProductName.TabIndex = 10;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.BackgroundImage = global::Media_Bazaar.Properties.Resources.rounded_rectangle_1;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Controls.Add(this.panelNotificationCount);
            this.panel2.Controls.Add(this.btnNotificationReq);
            this.panel2.Location = new System.Drawing.Point(695, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(41, 33);
            this.panel2.TabIndex = 16;
            // 
            // panelNotificationCount
            // 
            this.panelNotificationCount.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelNotificationCount.BackColor = System.Drawing.Color.Firebrick;
            this.panelNotificationCount.Controls.Add(this.labelActiveNotif);
            this.panelNotificationCount.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.panelNotificationCount.Location = new System.Drawing.Point(25, 0);
            this.panelNotificationCount.Name = "panelNotificationCount";
            this.panelNotificationCount.Size = new System.Drawing.Size(16, 14);
            this.panelNotificationCount.TabIndex = 15;
            this.panelNotificationCount.Visible = false;
            // 
            // labelActiveNotif
            // 
            this.labelActiveNotif.AutoSize = true;
            this.labelActiveNotif.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActiveNotif.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelActiveNotif.Location = new System.Drawing.Point(2, 0);
            this.labelActiveNotif.Name = "labelActiveNotif";
            this.labelActiveNotif.Size = new System.Drawing.Size(15, 12);
            this.labelActiveNotif.TabIndex = 16;
            this.labelActiveNotif.Text = "20";
            this.labelActiveNotif.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNotificationReq
            // 
            this.btnNotificationReq.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnNotificationReq.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnNotificationReq.BackgroundImage = global::Media_Bazaar.Properties.Resources.notification_copy;
            this.btnNotificationReq.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnNotificationReq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNotificationReq.FlatAppearance.BorderSize = 0;
            this.btnNotificationReq.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnNotificationReq.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnNotificationReq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNotificationReq.Location = new System.Drawing.Point(10, 5);
            this.btnNotificationReq.Name = "btnNotificationReq";
            this.btnNotificationReq.Size = new System.Drawing.Size(22, 24);
            this.btnNotificationReq.TabIndex = 14;
            this.btnNotificationReq.UseVisualStyleBackColor = false;
            this.btnNotificationReq.Click += new System.EventHandler(this.btnNotificationReq_Click);
            // 
            // panelInfoRequestNotification
            // 
            this.panelInfoRequestNotification.BackColor = System.Drawing.Color.Transparent;
            this.panelInfoRequestNotification.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelInfoRequestNotification.BackgroundImage")));
            this.panelInfoRequestNotification.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelInfoRequestNotification.Controls.Add(this.btnCloseNotificationPop);
            this.panelInfoRequestNotification.Controls.Add(this.labelNotificationPopText);
            this.panelInfoRequestNotification.Location = new System.Drawing.Point(28, 4);
            this.panelInfoRequestNotification.Name = "panelInfoRequestNotification";
            this.panelInfoRequestNotification.Size = new System.Drawing.Size(617, 25);
            this.panelInfoRequestNotification.TabIndex = 13;
            this.panelInfoRequestNotification.Visible = false;
            // 
            // btnCloseNotificationPop
            // 
            this.btnCloseNotificationPop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnCloseNotificationPop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCloseNotificationPop.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnCloseNotificationPop.FlatAppearance.BorderSize = 0;
            this.btnCloseNotificationPop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnCloseNotificationPop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnCloseNotificationPop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseNotificationPop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseNotificationPop.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCloseNotificationPop.Location = new System.Drawing.Point(580, 2);
            this.btnCloseNotificationPop.Name = "btnCloseNotificationPop";
            this.btnCloseNotificationPop.Size = new System.Drawing.Size(32, 23);
            this.btnCloseNotificationPop.TabIndex = 16;
            this.btnCloseNotificationPop.Text = "X";
            this.btnCloseNotificationPop.UseVisualStyleBackColor = false;
            this.btnCloseNotificationPop.Click += new System.EventHandler(this.btnCloseNotificationPop_Click);
            // 
            // labelNotificationPopText
            // 
            this.labelNotificationPopText.AutoSize = true;
            this.labelNotificationPopText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNotificationPopText.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelNotificationPopText.Location = new System.Drawing.Point(209, 4);
            this.labelNotificationPopText.Name = "labelNotificationPopText";
            this.labelNotificationPopText.Size = new System.Drawing.Size(160, 15);
            this.labelNotificationPopText.TabIndex = 16;
            this.labelNotificationPopText.Text = "Loading... (ETA:10 seconds)";
            // 
            // panelProductRequests
            // 
            this.panelProductRequests.Controls.Add(this.btnMarkPending);
            this.panelProductRequests.Controls.Add(this.btnViewClosedReq);
            this.panelProductRequests.Controls.Add(this.btnViewPendingReq);
            this.panelProductRequests.Controls.Add(this.btnViewNewProductReq);
            this.panelProductRequests.Controls.Add(this.btnCloseProductReq);
            this.panelProductRequests.Controls.Add(this.listBoxProductReq);
            this.panelProductRequests.Location = new System.Drawing.Point(3, 508);
            this.panelProductRequests.Name = "panelProductRequests";
            this.panelProductRequests.Size = new System.Drawing.Size(694, 514);
            this.panelProductRequests.TabIndex = 11;
            // 
            // btnMarkPending
            // 
            this.btnMarkPending.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnMarkPending.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnMarkPending.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMarkPending.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnMarkPending.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnMarkPending.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMarkPending.ForeColor = System.Drawing.Color.LightGray;
            this.btnMarkPending.Location = new System.Drawing.Point(148, 436);
            this.btnMarkPending.Margin = new System.Windows.Forms.Padding(4);
            this.btnMarkPending.Name = "btnMarkPending";
            this.btnMarkPending.Size = new System.Drawing.Size(125, 51);
            this.btnMarkPending.TabIndex = 9;
            this.btnMarkPending.Text = "Mark Request As Pending";
            this.btnMarkPending.UseVisualStyleBackColor = false;
            this.btnMarkPending.Visible = false;
            this.btnMarkPending.Click += new System.EventHandler(this.btnMarkPending_Click);
            // 
            // btnViewClosedReq
            // 
            this.btnViewClosedReq.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewClosedReq.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewClosedReq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewClosedReq.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewClosedReq.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewClosedReq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewClosedReq.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewClosedReq.Location = new System.Drawing.Point(281, 365);
            this.btnViewClosedReq.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewClosedReq.Name = "btnViewClosedReq";
            this.btnViewClosedReq.Size = new System.Drawing.Size(125, 51);
            this.btnViewClosedReq.TabIndex = 8;
            this.btnViewClosedReq.Text = "View Closed Request";
            this.btnViewClosedReq.UseVisualStyleBackColor = false;
            this.btnViewClosedReq.Click += new System.EventHandler(this.btnViewClosedReq_Click);
            // 
            // btnViewPendingReq
            // 
            this.btnViewPendingReq.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewPendingReq.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewPendingReq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewPendingReq.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewPendingReq.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewPendingReq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewPendingReq.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewPendingReq.Location = new System.Drawing.Point(148, 365);
            this.btnViewPendingReq.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewPendingReq.Name = "btnViewPendingReq";
            this.btnViewPendingReq.Size = new System.Drawing.Size(125, 51);
            this.btnViewPendingReq.TabIndex = 7;
            this.btnViewPendingReq.Text = "View Pending Request";
            this.btnViewPendingReq.UseVisualStyleBackColor = false;
            this.btnViewPendingReq.Click += new System.EventHandler(this.btnViewPendingReq_Click);
            // 
            // btnViewNewProductReq
            // 
            this.btnViewNewProductReq.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewNewProductReq.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewNewProductReq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewNewProductReq.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewNewProductReq.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewNewProductReq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewNewProductReq.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewNewProductReq.Location = new System.Drawing.Point(15, 365);
            this.btnViewNewProductReq.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewNewProductReq.Name = "btnViewNewProductReq";
            this.btnViewNewProductReq.Size = new System.Drawing.Size(125, 51);
            this.btnViewNewProductReq.TabIndex = 6;
            this.btnViewNewProductReq.Text = "View Open Request";
            this.btnViewNewProductReq.UseVisualStyleBackColor = false;
            this.btnViewNewProductReq.Click += new System.EventHandler(this.btnViewProductReq_Click);
            // 
            // btnCloseProductReq
            // 
            this.btnCloseProductReq.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCloseProductReq.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnCloseProductReq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCloseProductReq.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnCloseProductReq.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnCloseProductReq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseProductReq.ForeColor = System.Drawing.Color.LightGray;
            this.btnCloseProductReq.Location = new System.Drawing.Point(454, 365);
            this.btnCloseProductReq.Margin = new System.Windows.Forms.Padding(4);
            this.btnCloseProductReq.Name = "btnCloseProductReq";
            this.btnCloseProductReq.Size = new System.Drawing.Size(125, 51);
            this.btnCloseProductReq.TabIndex = 5;
            this.btnCloseProductReq.Text = "Close Request";
            this.btnCloseProductReq.UseVisualStyleBackColor = false;
            this.btnCloseProductReq.Visible = false;
            this.btnCloseProductReq.Click += new System.EventHandler(this.btnConfirmProductReq_Click);
            // 
            // listBoxProductReq
            // 
            this.listBoxProductReq.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listBoxProductReq.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listBoxProductReq.FormattingEnabled = true;
            this.listBoxProductReq.ItemHeight = 16;
            this.listBoxProductReq.Location = new System.Drawing.Point(15, 13);
            this.listBoxProductReq.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxProductReq.Name = "listBoxProductReq";
            this.listBoxProductReq.Size = new System.Drawing.Size(665, 324);
            this.listBoxProductReq.TabIndex = 1;
            // 
            // btnNavRemoveProduct
            // 
            this.btnNavRemoveProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnNavRemoveProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnNavRemoveProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNavRemoveProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnNavRemoveProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnNavRemoveProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNavRemoveProduct.ForeColor = System.Drawing.Color.LightGray;
            this.btnNavRemoveProduct.Location = new System.Drawing.Point(326, 442);
            this.btnNavRemoveProduct.Margin = new System.Windows.Forms.Padding(4);
            this.btnNavRemoveProduct.Name = "btnNavRemoveProduct";
            this.btnNavRemoveProduct.Size = new System.Drawing.Size(125, 51);
            this.btnNavRemoveProduct.TabIndex = 6;
            this.btnNavRemoveProduct.Text = "Make Stock Unavailable";
            this.btnNavRemoveProduct.UseVisualStyleBackColor = false;
            this.btnNavRemoveProduct.Visible = false;
            this.btnNavRemoveProduct.Click += new System.EventHandler(this.btnNavRemoveProduct_Click);
            // 
            // btnNavModifyProduct
            // 
            this.btnNavModifyProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnNavModifyProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnNavModifyProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNavModifyProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnNavModifyProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnNavModifyProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNavModifyProduct.ForeColor = System.Drawing.Color.LightGray;
            this.btnNavModifyProduct.Location = new System.Drawing.Point(193, 442);
            this.btnNavModifyProduct.Margin = new System.Windows.Forms.Padding(4);
            this.btnNavModifyProduct.Name = "btnNavModifyProduct";
            this.btnNavModifyProduct.Size = new System.Drawing.Size(125, 51);
            this.btnNavModifyProduct.TabIndex = 5;
            this.btnNavModifyProduct.Text = "Modify Stock";
            this.btnNavModifyProduct.UseVisualStyleBackColor = false;
            this.btnNavModifyProduct.Click += new System.EventHandler(this.btnNavModifyProduct_Click);
            // 
            // btnNavAddProduct
            // 
            this.btnNavAddProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnNavAddProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnNavAddProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNavAddProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnNavAddProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnNavAddProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNavAddProduct.ForeColor = System.Drawing.Color.LightGray;
            this.btnNavAddProduct.Location = new System.Drawing.Point(60, 442);
            this.btnNavAddProduct.Margin = new System.Windows.Forms.Padding(4);
            this.btnNavAddProduct.Name = "btnNavAddProduct";
            this.btnNavAddProduct.Size = new System.Drawing.Size(125, 51);
            this.btnNavAddProduct.TabIndex = 4;
            this.btnNavAddProduct.Text = "Add Stock";
            this.btnNavAddProduct.UseVisualStyleBackColor = false;
            this.btnNavAddProduct.Click += new System.EventHandler(this.btnNavAddProduct_Click);
            // 
            // btnViewAvailableProducts
            // 
            this.btnViewAvailableProducts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewAvailableProducts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewAvailableProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewAvailableProducts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewAvailableProducts.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewAvailableProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewAvailableProducts.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewAvailableProducts.Location = new System.Drawing.Point(618, 144);
            this.btnViewAvailableProducts.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewAvailableProducts.Name = "btnViewAvailableProducts";
            this.btnViewAvailableProducts.Size = new System.Drawing.Size(109, 51);
            this.btnViewAvailableProducts.TabIndex = 3;
            this.btnViewAvailableProducts.Text = "View Available";
            this.btnViewAvailableProducts.UseVisualStyleBackColor = false;
            this.btnViewAvailableProducts.Click += new System.EventHandler(this.btnViewAvailableProducts_Click);
            // 
            // btnViewUnavailableProducts
            // 
            this.btnViewUnavailableProducts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewUnavailableProducts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewUnavailableProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewUnavailableProducts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewUnavailableProducts.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewUnavailableProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewUnavailableProducts.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewUnavailableProducts.Location = new System.Drawing.Point(618, 206);
            this.btnViewUnavailableProducts.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewUnavailableProducts.Name = "btnViewUnavailableProducts";
            this.btnViewUnavailableProducts.Size = new System.Drawing.Size(109, 51);
            this.btnViewUnavailableProducts.TabIndex = 2;
            this.btnViewUnavailableProducts.Text = "View Unavailable";
            this.btnViewUnavailableProducts.UseVisualStyleBackColor = false;
            this.btnViewUnavailableProducts.Click += new System.EventHandler(this.btnViewUnavailableProducts_Click);
            // 
            // btnViewAllProducts
            // 
            this.btnViewAllProducts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewAllProducts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewAllProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewAllProducts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewAllProducts.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewAllProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewAllProducts.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewAllProducts.Location = new System.Drawing.Point(618, 81);
            this.btnViewAllProducts.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewAllProducts.Name = "btnViewAllProducts";
            this.btnViewAllProducts.Size = new System.Drawing.Size(109, 51);
            this.btnViewAllProducts.TabIndex = 1;
            this.btnViewAllProducts.Text = "View All";
            this.btnViewAllProducts.UseVisualStyleBackColor = false;
            this.btnViewAllProducts.Click += new System.EventHandler(this.btnViewAllProducts_Click);
            // 
            // listBoxProducts
            // 
            this.listBoxProducts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listBoxProducts.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listBoxProducts.FormattingEnabled = true;
            this.listBoxProducts.ItemHeight = 16;
            this.listBoxProducts.Location = new System.Drawing.Point(63, 81);
            this.listBoxProducts.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxProducts.Name = "listBoxProducts";
            this.listBoxProducts.Size = new System.Drawing.Size(547, 324);
            this.listBoxProducts.TabIndex = 0;
            // 
            // BtnSearchProduct
            // 
            this.BtnSearchProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnSearchProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSearchProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSearchProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSearchProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnSearchProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSearchProduct.ForeColor = System.Drawing.Color.LightGray;
            this.BtnSearchProduct.Location = new System.Drawing.Point(618, 265);
            this.BtnSearchProduct.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSearchProduct.Name = "BtnSearchProduct";
            this.BtnSearchProduct.Size = new System.Drawing.Size(109, 51);
            this.BtnSearchProduct.TabIndex = 17;
            this.BtnSearchProduct.Text = "Search Product";
            this.BtnSearchProduct.UseVisualStyleBackColor = false;
            this.BtnSearchProduct.Click += new System.EventHandler(this.BtnSearchProduct_Click);
            // 
            // btnViewStockDescription
            // 
            this.btnViewStockDescription.BackColor = System.Drawing.Color.Transparent;
            this.btnViewStockDescription.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnViewStockDescription.BackgroundImage")));
            this.btnViewStockDescription.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnViewStockDescription.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewStockDescription.FlatAppearance.BorderSize = 0;
            this.btnViewStockDescription.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnViewStockDescription.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnViewStockDescription.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewStockDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewStockDescription.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnViewStockDescription.Location = new System.Drawing.Point(636, 323);
            this.btnViewStockDescription.Name = "btnViewStockDescription";
            this.btnViewStockDescription.Size = new System.Drawing.Size(72, 38);
            this.btnViewStockDescription.TabIndex = 19;
            this.btnViewStockDescription.Text = "Description";
            this.btnViewStockDescription.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnViewStockDescription.UseVisualStyleBackColor = false;
            this.btnViewStockDescription.Click += new System.EventHandler(this.btnViewStockDescription_Click);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.label3.Location = new System.Drawing.Point(117, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(486, 76);
            this.label3.TabIndex = 8;
            this.label3.Text = "Media Bazaar";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Stencil", 36F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.label11.Location = new System.Drawing.Point(229, 292);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(309, 57);
            this.label11.TabIndex = 11;
            this.label11.Text = "warehouse";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timerCheckNotif
            // 
            this.timerCheckNotif.Enabled = true;
            this.timerCheckNotif.Interval = 10000;
            this.timerCheckNotif.Tick += new System.EventHandler(this.timerCheckNotif_Tick);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.panelMainContent);
            this.Controls.Add(this.panelSideMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(950, 600);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WarehouseApp";
            this.Click += new System.EventHandler(this.btnProductDashboard_Click);
            this.panelSideMenu.ResumeLayout(false);
            this.panelProductDashboardSubMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelMainContent.ResumeLayout(false);
            this.panelMainContent.PerformLayout();
            this.panelProductDash.ResumeLayout(false);
            this.panelAddProduct.ResumeLayout(false);
            this.panelAddProduct.PerformLayout();
            this.panelSearchSpecificProduct.ResumeLayout(false);
            this.panelSearchSpecificProduct.PerformLayout();
            this.panelModifyProduct.ResumeLayout(false);
            this.panelModifyProduct.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panelNotificationCount.ResumeLayout(false);
            this.panelNotificationCount.PerformLayout();
            this.panelInfoRequestNotification.ResumeLayout(false);
            this.panelInfoRequestNotification.PerformLayout();
            this.panelProductRequests.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Button btnProductReq;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Panel panelProductDashboardSubMenu;
        private System.Windows.Forms.Button btnMenuModifyProduct;
        private System.Windows.Forms.Button btnMenuAddProduct;
        private System.Windows.Forms.Button btnProductDashboard;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelMainContent;
        private System.Windows.Forms.Panel panelProductDash;
        private System.Windows.Forms.Panel panelSearchSpecificProduct;
        private System.Windows.Forms.ListBox LbMultipleProducts;
        private System.Windows.Forms.TextBox TbxState;
        private System.Windows.Forms.Label LblState;
        private System.Windows.Forms.TextBox TbxPurchaseValue;
        private System.Windows.Forms.Label LblPurchaseValue;
        private System.Windows.Forms.ComboBox CbxToSearch;
        private System.Windows.Forms.TextBox TbxDescription;
        private System.Windows.Forms.TextBox TbxQuantity;
        private System.Windows.Forms.TextBox TbxName;
        private System.Windows.Forms.TextBox TbxId;
        private System.Windows.Forms.Label LblDescription;
        private System.Windows.Forms.Label LblQuantity;
        private System.Windows.Forms.Label LblName;
        private System.Windows.Forms.Label LblId;
        private System.Windows.Forms.Label LblProduct;
        private System.Windows.Forms.TextBox TbProductnq;
        private System.Windows.Forms.Button BtnSearchSpecificProduct;
        private System.Windows.Forms.Button BtnSearchProduct;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelNotificationCount;
        private System.Windows.Forms.Label labelActiveNotif;
        private System.Windows.Forms.Button btnNotificationReq;
        private System.Windows.Forms.Panel panelInfoRequestNotification;
        private System.Windows.Forms.Button btnCloseNotificationPop;
        private System.Windows.Forms.Label labelNotificationPopText;
        private System.Windows.Forms.Panel panelProductRequests;
        private System.Windows.Forms.Button btnViewNewProductReq;
        private System.Windows.Forms.Button btnCloseProductReq;
        private System.Windows.Forms.ListBox listBoxProductReq;
        private System.Windows.Forms.Panel panelAddProduct;
        private System.Windows.Forms.Button btnAddProductClearFields;
        private System.Windows.Forms.Button btnBackFromAddProduct;
        private System.Windows.Forms.Button btnAddProductToDb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxAddProductValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxAddProductDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxAddProductQuantity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAddProductName;
        private System.Windows.Forms.Panel panelModifyProduct;
        private System.Windows.Forms.RadioButton radioButtonInactiveProduct;
        private System.Windows.Forms.RadioButton radioButtonActiveProduct;
        private System.Windows.Forms.Button btnClearModifyProductFields;
        private System.Windows.Forms.Button btnBackFromModifyProduct;
        private System.Windows.Forms.Button btnModifyProductToDb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxModifyProductValue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxModifyProductDescription;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxModifyProductQuantity;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxModifyProductName;
        private System.Windows.Forms.Button btnNavRemoveProduct;
        private System.Windows.Forms.Button btnNavModifyProduct;
        private System.Windows.Forms.Button btnNavAddProduct;
        private System.Windows.Forms.Button btnViewAvailableProducts;
        private System.Windows.Forms.Button btnViewUnavailableProducts;
        private System.Windows.Forms.Button btnViewAllProducts;
        private System.Windows.Forms.ListBox listBoxProducts;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Timer timerCheckNotif;
        private System.Windows.Forms.DateTimePicker dateTimePickerModStock;
        private System.Windows.Forms.Button btnViewStockDescription;
        private System.Windows.Forms.Button btnViewClosedReq;
        private System.Windows.Forms.Button btnViewPendingReq;
        private System.Windows.Forms.Button btnMarkPending;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxAddProductType;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxModifyProductType;
    }
}