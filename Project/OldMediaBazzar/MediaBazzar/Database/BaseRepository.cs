﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace MediaBazzar.Database
{
    public abstract class BaseRepository<T> where T : class
    {
        private static MySqlConnection _connection;
        public BaseRepository(string connectionString)
        {
            _connection = new MySqlConnection(connectionString);
        }

        public abstract T Populate(MySqlDataReader reader);
        public abstract void Fill(MySqlCommand command, T record);
        public abstract IEnumerable<T> GetAll();
        public abstract T GetById(string id);
        public abstract void Add(T record);
        public abstract void Update(T record);
        public abstract void Delete(string id);
        protected IEnumerable<T> GetAll(MySqlCommand command)
        {
            var list = new List<T>();
            command.Connection = _connection;
            _connection.Open();
            try
            {
                var reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                        list.Add(Populate(reader));
                }
                finally
                {
                    reader.Close();
                }
            }
            finally
            {
                _connection.Close();
            }
            return list;
        }
        protected T GetById(MySqlCommand command)
        {
            T record = null;
            command.Connection = _connection;
            _connection.Open();
            try
            {
                var reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        record = Populate(reader);
                        break;
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            finally
            {
                _connection.Close();
            }
            return record;
        }

        protected void Delete(string table, string id)
        {
            using (var command = new MySqlCommand("DELETE FROM " + table + " WHERE Id=@id"))
            {
                command.Parameters.AddWithValue("@id", id);
                Save(command);
            }
        }
        protected void Save(MySqlCommand command)
        {
            command.Connection = _connection;
            _connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                _connection.Close();
            }
        }

    }
}
