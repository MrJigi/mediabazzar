﻿using MediaBazzar.Database;
using MediaBazzar.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MediaBazzar.ViewModels
{
    class FLMViewModel : ViewModelBase
    {
        public UserListViewModel UserList { get; set; }
        public ICommand TerminateCommand { get; }
        public ICommand DeleteCommand { get; }
        public ICommand SaveStockCommand { get; }
        public ICommand RefreshCommand { get; }
        public ICommand RefillCommand { get; }
        public ICommand AcceptCommand { get; }
        public ICommand DeclineCommand { get; }
        public Stock NewStock { get; set; }
        public Stock SelectedStock { get; set; }
        private BaseRepository<User> _userRepo;
        private BaseRepository<Stock> _stockRepo;
        private BaseRepository<StockRefill> _stockRefillRepo;
        private ObservableCollection<Stock> _stocks;
        public ObservableCollection<Stock> Stocks
        {
            get => _stocks;
            set
            {
                _stocks = value;
                NotifyPropertyChanged("Stocks");
            }
        }
        public FLMViewModel()
        {
            UserList = new UserListViewModel();
            TerminateCommand = new RelayCommand(param => TerminateRequest());
            SaveStockCommand = new RelayCommand(param => CreateStock());
            RefreshCommand = new RelayCommand(param => RefreshList());
            DeleteCommand = new RelayCommand(param => DeleteStock());
            RefillCommand = new RelayCommand(param => RefreshRefillList());
            AcceptCommand = new RelayCommand(param => AcceptRefill());
            DeclineCommand = new RelayCommand(param => DeclineRefill());
            _userRepo = new UserRepository(ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
            _stockRepo = new StockRepository(ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
            _stockRefillRepo = new StockRefillRepository(ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
            Stocks = new ObservableCollection<Stock>();
            SelectedStock = new Stock();
            NewStock = new Stock();
        }
        private void AcceptRefill()
        {
            foreach(StockRefill st in _stockRefillRepo.GetAll())
            {
                if(st.ItemId == SelectedStock.Id)
                {
                    Stocks.Remove(SelectedStock);
                    st.Accepted = true;
                    _stockRefillRepo.Update(st);
                    break;
                }
            }
        }
        private void DeclineRefill()
        {
            foreach (StockRefill st in _stockRefillRepo.GetAll())
            {
                if (st.ItemId == SelectedStock.Id)
                {
                    Stocks.Remove(SelectedStock);
                    _stockRefillRepo.Delete(st.Id.ToString());
                    break;
                }
            }
        }
        private void RefreshRefillList()
        {
            Stocks.Clear();
            foreach(StockRefill st in _stockRefillRepo.GetAll())
            {
                Stocks.Add(_stockRepo.GetById(st.ItemId.ToString()));
            }
        }
        private void DeleteStock()
        {
            _stockRepo.Delete(SelectedStock.Id.ToString());
            Stocks.Remove(SelectedStock);
        }
        private void RefreshList()
        {
            Stocks = new ObservableCollection<Stock>(_stockRepo.GetAll());
        }

        private void CreateStock()
        {
            _stockRepo.Add(NewStock);
            NewStock = new Stock();
        }
        private void TerminateRequest()
        {
            UserList.SelectedUser.ToBeFired = true;
            _userRepo.Update(UserList.SelectedUser);
        }
    }
}
