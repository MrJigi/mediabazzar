﻿namespace Media_Bazaar
{
    partial class manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(manager));
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.BtnEmployeeStatistics = new System.Windows.Forms.Button();
            this.btnStockStats = new System.Windows.Forms.Button();
            this.btnProductReq = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelLogo = new System.Windows.Forms.Label();
            this.panelMainContent = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panelProductDash = new System.Windows.Forms.Panel();
            this.panelModifyProduct = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxModifyProductType = new System.Windows.Forms.TextBox();
            this.radioButtonInactiveProduct = new System.Windows.Forms.RadioButton();
            this.radioButtonActiveProduct = new System.Windows.Forms.RadioButton();
            this.btnClearModifyProductFields = new System.Windows.Forms.Button();
            this.btnBackFromModifyProduct = new System.Windows.Forms.Button();
            this.btnModifyProductToDb = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxModifyProductValue = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxModifyProductDescription = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxModifyProductName = new System.Windows.Forms.TextBox();
            this.panelStockRefillRequest = new System.Windows.Forms.Panel();
            this.btnCloseStockRefill = new System.Windows.Forms.Button();
            this.btnSendStockRefill = new System.Windows.Forms.Button();
            this.textBoxStockRefill = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnViewStockDescription = new System.Windows.Forms.Button();
            this.panelStockRequestSelectView = new System.Windows.Forms.Panel();
            this.btnViewOpenRequests = new System.Windows.Forms.Button();
            this.btnViewPendingRequests = new System.Windows.Forms.Button();
            this.btnViewClosedRequests = new System.Windows.Forms.Button();
            this.panelStockSelectView = new System.Windows.Forms.Panel();
            this.btnViewAllProducts = new System.Windows.Forms.Button();
            this.btnViewAvailableProducts = new System.Windows.Forms.Button();
            this.btnViewUnavailableProducts = new System.Windows.Forms.Button();
            this.btnViewStockRequests = new System.Windows.Forms.Button();
            this.btnViewStock = new System.Windows.Forms.Button();
            this.btnStockModRequest = new System.Windows.Forms.Button();
            this.btnStockRefillRequest = new System.Windows.Forms.Button();
            this.btnAddProductRequest = new System.Windows.Forms.Button();
            this.listBoxProducts = new System.Windows.Forms.ListBox();
            this.panelAddProduct = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxAddProductType = new System.Windows.Forms.TextBox();
            this.btnAddProductClearFields = new System.Windows.Forms.Button();
            this.btnBackFromAddProduct = new System.Windows.Forms.Button();
            this.btnAddProductToDb = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxAddProductValue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxAddProductDescription = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxAddProductQuantity = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAddProductName = new System.Windows.Forms.TextBox();
            this.panelStockStatistics = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textBoxSortType = new System.Windows.Forms.TextBox();
            this.btnSortStockByType = new System.Windows.Forms.Button();
            this.panelMonthlyStock = new System.Windows.Forms.Panel();
            this.listBoxRefillLog = new System.Windows.Forms.ListBox();
            this.labelTotalRefillRequests = new System.Windows.Forms.Label();
            this.btnRefreshMonthly = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.btnCloseMonthly = new System.Windows.Forms.Button();
            this.panelInactiveItemsInstock = new System.Windows.Forms.Panel();
            this.labelTotalInactiveInStock = new System.Windows.Forms.Label();
            this.btnTotalnactiveStockRefresh = new System.Windows.Forms.Button();
            this.panelActiveItemsInStock = new System.Windows.Forms.Panel();
            this.labelActiveItemsInStock = new System.Windows.Forms.Label();
            this.btnTotalActiveStockRefresh = new System.Windows.Forms.Button();
            this.panelInactiveItemsPerc = new System.Windows.Forms.Panel();
            this.labelTotalInactiveStockPerc = new System.Windows.Forms.Label();
            this.btnRefreshStockInactivePerc = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxPreviewStockPrice = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxPreviewStockQty = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxPreviewStockType = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxPreviewStockName = new System.Windows.Forms.TextBox();
            this.labelStockName = new System.Windows.Forms.Label();
            this.btnShowMonthly = new System.Windows.Forms.Button();
            this.btnPreviewSelectedStock = new System.Windows.Forms.Button();
            this.panelActiveItemsPerc = new System.Windows.Forms.Panel();
            this.labelTotalActiveStockPerc = new System.Windows.Forms.Label();
            this.btnRefreshStockActivePerc = new System.Windows.Forms.Button();
            this.panelTotalStock = new System.Windows.Forms.Panel();
            this.labelTotalStock = new System.Windows.Forms.Label();
            this.btnRefreshTotalStock = new System.Windows.Forms.Button();
            this.btnSortStockQty = new System.Windows.Forms.Button();
            this.btnViewInactiveStockStats = new System.Windows.Forms.Button();
            this.btnViewActiveStockStats = new System.Windows.Forms.Button();
            this.listBoxStockStats = new System.Windows.Forms.ListBox();
            this.PanelAllEmployeeStatistics = new System.Windows.Forms.Panel();
            this.LblAbsentPercentageAllEmployees = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.BtnStatisticsAllEmployees = new System.Windows.Forms.Button();
            this.LblTotalShiftsAllEmployees = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.LblSickAllEmployees = new System.Windows.Forms.Label();
            this.LblAbsentAllEmployees = new System.Windows.Forms.Label();
            this.LblPresentAllEmployees = new System.Windows.Forms.Label();
            this.LblSickPercentageAllEmployees = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.LblPresentPercentageAllEmployees = new System.Windows.Forms.Label();
            this.PanelSpecificEmployeeStatistics = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.BtnViewSpecificEmployeeStatistics = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.LblLatestShiftDate = new System.Windows.Forms.Label();
            this.LblFirstShiftDate = new System.Windows.Forms.Label();
            this.LblTotalShifts = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.LblSick = new System.Windows.Forms.Label();
            this.LblAbsent = new System.Windows.Forms.Label();
            this.LblPresent = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.LblSickPercentage = new System.Windows.Forms.Label();
            this.LblAbsentPercentage = new System.Windows.Forms.Label();
            this.LblPresentPercentage = new System.Windows.Forms.Label();
            this.PanelChoosSpecificOrAllEmployees = new System.Windows.Forms.Panel();
            this.BtnAllEmployees = new System.Windows.Forms.Button();
            this.BtnSpecificEmployee = new System.Windows.Forms.Button();
            this.bt_employeesStatistics = new System.Windows.Forms.Button();
            this.pnl_employeesStatistics = new System.Windows.Forms.Panel();
            this.lb_info = new System.Windows.Forms.ListBox();
            this.bt_allDepo = new System.Windows.Forms.Button();
            this.bt_allManager = new System.Windows.Forms.Button();
            this.bt_allAdministration = new System.Windows.Forms.Button();
            this.bt_totalInactive = new System.Windows.Forms.Button();
            this.bt_totalActive = new System.Windows.Forms.Button();
            this.bt_totalEmployees = new System.Windows.Forms.Button();
            this.panelSideMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelMainContent.SuspendLayout();
            this.panelProductDash.SuspendLayout();
            this.panelModifyProduct.SuspendLayout();
            this.panelStockRefillRequest.SuspendLayout();
            this.panelStockRequestSelectView.SuspendLayout();
            this.panelStockSelectView.SuspendLayout();
            this.panelAddProduct.SuspendLayout();
            this.panelStockStatistics.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panelMonthlyStock.SuspendLayout();
            this.panelInactiveItemsInstock.SuspendLayout();
            this.panelActiveItemsInStock.SuspendLayout();
            this.panelInactiveItemsPerc.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelActiveItemsPerc.SuspendLayout();
            this.panelTotalStock.SuspendLayout();
            this.PanelAllEmployeeStatistics.SuspendLayout();
            this.PanelSpecificEmployeeStatistics.SuspendLayout();
            this.PanelChoosSpecificOrAllEmployees.SuspendLayout();
            this.pnl_employeesStatistics.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panelSideMenu.Controls.Add(this.bt_employeesStatistics);
            this.panelSideMenu.Controls.Add(this.BtnEmployeeStatistics);
            this.panelSideMenu.Controls.Add(this.btnStockStats);
            this.panelSideMenu.Controls.Add(this.btnProductReq);
            this.panelSideMenu.Controls.Add(this.btnLogout);
            this.panelSideMenu.Controls.Add(this.panel1);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(228, 561);
            this.panelSideMenu.TabIndex = 12;
            // 
            // BtnEmployeeStatistics
            // 
            this.BtnEmployeeStatistics.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnEmployeeStatistics.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnEmployeeStatistics.FlatAppearance.BorderSize = 0;
            this.BtnEmployeeStatistics.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnEmployeeStatistics.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnEmployeeStatistics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEmployeeStatistics.ForeColor = System.Drawing.Color.Gainsboro;
            this.BtnEmployeeStatistics.Location = new System.Drawing.Point(0, 180);
            this.BtnEmployeeStatistics.Name = "BtnEmployeeStatistics";
            this.BtnEmployeeStatistics.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.BtnEmployeeStatistics.Size = new System.Drawing.Size(228, 40);
            this.BtnEmployeeStatistics.TabIndex = 7;
            this.BtnEmployeeStatistics.Text = "Employee Statistics";
            this.BtnEmployeeStatistics.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnEmployeeStatistics.UseVisualStyleBackColor = true;
            this.BtnEmployeeStatistics.Click += new System.EventHandler(this.BtnEmployeeStatistics_Click);
            // 
            // btnStockStats
            // 
            this.btnStockStats.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStockStats.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnStockStats.FlatAppearance.BorderSize = 0;
            this.btnStockStats.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnStockStats.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnStockStats.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockStats.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnStockStats.Location = new System.Drawing.Point(0, 140);
            this.btnStockStats.Name = "btnStockStats";
            this.btnStockStats.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnStockStats.Size = new System.Drawing.Size(228, 40);
            this.btnStockStats.TabIndex = 5;
            this.btnStockStats.Text = "Product Statistics";
            this.btnStockStats.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStockStats.UseVisualStyleBackColor = true;
            this.btnStockStats.Click += new System.EventHandler(this.btnStockStats_Click);
            // 
            // btnProductReq
            // 
            this.btnProductReq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProductReq.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProductReq.FlatAppearance.BorderSize = 0;
            this.btnProductReq.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnProductReq.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnProductReq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductReq.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnProductReq.Location = new System.Drawing.Point(0, 100);
            this.btnProductReq.Name = "btnProductReq";
            this.btnProductReq.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnProductReq.Size = new System.Drawing.Size(228, 40);
            this.btnProductReq.TabIndex = 4;
            this.btnProductReq.Text = "Product Requests";
            this.btnProductReq.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProductReq.UseVisualStyleBackColor = true;
            this.btnProductReq.Click += new System.EventHandler(this.btnProductReq_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnLogout.Location = new System.Drawing.Point(0, 521);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnLogout.Size = new System.Drawing.Size(228, 40);
            this.btnLogout.TabIndex = 3;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelLogo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 100);
            this.panel1.TabIndex = 0;
            // 
            // labelLogo
            // 
            this.labelLogo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelLogo.AutoSize = true;
            this.labelLogo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelLogo.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLogo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelLogo.Location = new System.Drawing.Point(23, 13);
            this.labelLogo.Name = "labelLogo";
            this.labelLogo.Size = new System.Drawing.Size(126, 76);
            this.labelLogo.TabIndex = 4;
            this.labelLogo.Text = "MB";
            this.labelLogo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelLogo.Click += new System.EventHandler(this.labelLogo_Click);
            // 
            // panelMainContent
            // 
            this.panelMainContent.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelMainContent.Controls.Add(this.label3);
            this.panelMainContent.Controls.Add(this.label11);
            this.panelMainContent.Location = new System.Drawing.Point(966, 0);
            this.panelMainContent.Name = "panelMainContent";
            this.panelMainContent.Size = new System.Drawing.Size(707, 460);
            this.panelMainContent.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Stencil", 48F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.label3.Location = new System.Drawing.Point(92, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(486, 76);
            this.label3.TabIndex = 8;
            this.label3.Text = "Media Bazaar";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Stencil", 36F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.label11.Location = new System.Drawing.Point(204, 242);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(340, 57);
            this.label11.TabIndex = 11;
            this.label11.Text = "management";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelProductDash
            // 
            this.panelProductDash.Controls.Add(this.panelModifyProduct);
            this.panelProductDash.Controls.Add(this.panelStockRefillRequest);
            this.panelProductDash.Controls.Add(this.btnViewStockDescription);
            this.panelProductDash.Controls.Add(this.panelStockRequestSelectView);
            this.panelProductDash.Controls.Add(this.panelStockSelectView);
            this.panelProductDash.Controls.Add(this.btnViewStockRequests);
            this.panelProductDash.Controls.Add(this.btnViewStock);
            this.panelProductDash.Controls.Add(this.btnStockModRequest);
            this.panelProductDash.Controls.Add(this.btnStockRefillRequest);
            this.panelProductDash.Controls.Add(this.btnAddProductRequest);
            this.panelProductDash.Controls.Add(this.listBoxProducts);
            this.panelProductDash.Location = new System.Drawing.Point(234, 551);
            this.panelProductDash.Name = "panelProductDash";
            this.panelProductDash.Size = new System.Drawing.Size(757, 572);
            this.panelProductDash.TabIndex = 15;
            this.panelProductDash.Visible = false;
            // 
            // panelModifyProduct
            // 
            this.panelModifyProduct.Controls.Add(this.label12);
            this.panelModifyProduct.Controls.Add(this.textBoxModifyProductType);
            this.panelModifyProduct.Controls.Add(this.radioButtonInactiveProduct);
            this.panelModifyProduct.Controls.Add(this.radioButtonActiveProduct);
            this.panelModifyProduct.Controls.Add(this.btnClearModifyProductFields);
            this.panelModifyProduct.Controls.Add(this.btnBackFromModifyProduct);
            this.panelModifyProduct.Controls.Add(this.btnModifyProductToDb);
            this.panelModifyProduct.Controls.Add(this.label7);
            this.panelModifyProduct.Controls.Add(this.textBoxModifyProductValue);
            this.panelModifyProduct.Controls.Add(this.label8);
            this.panelModifyProduct.Controls.Add(this.textBoxModifyProductDescription);
            this.panelModifyProduct.Controls.Add(this.label10);
            this.panelModifyProduct.Controls.Add(this.textBoxModifyProductName);
            this.panelModifyProduct.Cursor = System.Windows.Forms.Cursors.Default;
            this.panelModifyProduct.Location = new System.Drawing.Point(63, 84);
            this.panelModifyProduct.Name = "panelModifyProduct";
            this.panelModifyProduct.Size = new System.Drawing.Size(547, 340);
            this.panelModifyProduct.TabIndex = 17;
            this.panelModifyProduct.Visible = false;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(108, 76);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 16);
            this.label12.TabIndex = 27;
            this.label12.Text = "Type";
            // 
            // textBoxModifyProductType
            // 
            this.textBoxModifyProductType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxModifyProductType.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxModifyProductType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxModifyProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxModifyProductType.Location = new System.Drawing.Point(159, 71);
            this.textBoxModifyProductType.Name = "textBoxModifyProductType";
            this.textBoxModifyProductType.Size = new System.Drawing.Size(288, 26);
            this.textBoxModifyProductType.TabIndex = 26;
            // 
            // radioButtonInactiveProduct
            // 
            this.radioButtonInactiveProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioButtonInactiveProduct.AutoSize = true;
            this.radioButtonInactiveProduct.Location = new System.Drawing.Point(228, 252);
            this.radioButtonInactiveProduct.Name = "radioButtonInactiveProduct";
            this.radioButtonInactiveProduct.Size = new System.Drawing.Size(121, 20);
            this.radioButtonInactiveProduct.TabIndex = 22;
            this.radioButtonInactiveProduct.TabStop = true;
            this.radioButtonInactiveProduct.Text = "Prodcut Inactive";
            this.radioButtonInactiveProduct.UseVisualStyleBackColor = true;
            // 
            // radioButtonActiveProduct
            // 
            this.radioButtonActiveProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioButtonActiveProduct.AutoSize = true;
            this.radioButtonActiveProduct.Location = new System.Drawing.Point(228, 229);
            this.radioButtonActiveProduct.Name = "radioButtonActiveProduct";
            this.radioButtonActiveProduct.Size = new System.Drawing.Size(112, 20);
            this.radioButtonActiveProduct.TabIndex = 21;
            this.radioButtonActiveProduct.TabStop = true;
            this.radioButtonActiveProduct.Text = "Product Active";
            this.radioButtonActiveProduct.UseVisualStyleBackColor = true;
            // 
            // btnClearModifyProductFields
            // 
            this.btnClearModifyProductFields.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClearModifyProductFields.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnClearModifyProductFields.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearModifyProductFields.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnClearModifyProductFields.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnClearModifyProductFields.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearModifyProductFields.ForeColor = System.Drawing.Color.LightGray;
            this.btnClearModifyProductFields.Location = new System.Drawing.Point(467, 0);
            this.btnClearModifyProductFields.Margin = new System.Windows.Forms.Padding(4);
            this.btnClearModifyProductFields.Name = "btnClearModifyProductFields";
            this.btnClearModifyProductFields.Size = new System.Drawing.Size(80, 31);
            this.btnClearModifyProductFields.TabIndex = 20;
            this.btnClearModifyProductFields.Text = "Clear";
            this.btnClearModifyProductFields.UseVisualStyleBackColor = false;
            this.btnClearModifyProductFields.Click += new System.EventHandler(this.btnClearModifyProductFields_Click);
            // 
            // btnBackFromModifyProduct
            // 
            this.btnBackFromModifyProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBackFromModifyProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnBackFromModifyProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBackFromModifyProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnBackFromModifyProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnBackFromModifyProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackFromModifyProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackFromModifyProduct.ForeColor = System.Drawing.Color.LightGray;
            this.btnBackFromModifyProduct.Location = new System.Drawing.Point(0, 0);
            this.btnBackFromModifyProduct.Margin = new System.Windows.Forms.Padding(4);
            this.btnBackFromModifyProduct.Name = "btnBackFromModifyProduct";
            this.btnBackFromModifyProduct.Size = new System.Drawing.Size(80, 31);
            this.btnBackFromModifyProduct.TabIndex = 19;
            this.btnBackFromModifyProduct.Text = "<<";
            this.btnBackFromModifyProduct.UseVisualStyleBackColor = false;
            this.btnBackFromModifyProduct.Click += new System.EventHandler(this.btnBackFromModifyProduct_Click);
            // 
            // btnModifyProductToDb
            // 
            this.btnModifyProductToDb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnModifyProductToDb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnModifyProductToDb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModifyProductToDb.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnModifyProductToDb.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnModifyProductToDb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModifyProductToDb.ForeColor = System.Drawing.Color.LightGray;
            this.btnModifyProductToDb.Location = new System.Drawing.Point(220, 282);
            this.btnModifyProductToDb.Margin = new System.Windows.Forms.Padding(4);
            this.btnModifyProductToDb.Name = "btnModifyProductToDb";
            this.btnModifyProductToDb.Size = new System.Drawing.Size(128, 51);
            this.btnModifyProductToDb.TabIndex = 18;
            this.btnModifyProductToDb.Text = "Send Modification Request";
            this.btnModifyProductToDb.UseVisualStyleBackColor = false;
            this.btnModifyProductToDb.Click += new System.EventHandler(this.btnModifyProductToDb_Click);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 16);
            this.label7.TabIndex = 17;
            this.label7.Text = "Purchase Value";
            // 
            // textBoxModifyProductValue
            // 
            this.textBoxModifyProductValue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxModifyProductValue.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxModifyProductValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxModifyProductValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxModifyProductValue.Location = new System.Drawing.Point(159, 200);
            this.textBoxModifyProductValue.Name = "textBoxModifyProductValue";
            this.textBoxModifyProductValue.Size = new System.Drawing.Size(288, 26);
            this.textBoxModifyProductValue.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(74, 135);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 16);
            this.label8.TabIndex = 15;
            this.label8.Text = "Description";
            // 
            // textBoxModifyProductDescription
            // 
            this.textBoxModifyProductDescription.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxModifyProductDescription.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxModifyProductDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxModifyProductDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxModifyProductDescription.Location = new System.Drawing.Point(159, 103);
            this.textBoxModifyProductDescription.Multiline = true;
            this.textBoxModifyProductDescription.Name = "textBoxModifyProductDescription";
            this.textBoxModifyProductDescription.Size = new System.Drawing.Size(288, 91);
            this.textBoxModifyProductDescription.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(108, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 16);
            this.label10.TabIndex = 11;
            this.label10.Text = "Name";
            // 
            // textBoxModifyProductName
            // 
            this.textBoxModifyProductName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxModifyProductName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxModifyProductName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxModifyProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxModifyProductName.Location = new System.Drawing.Point(159, 37);
            this.textBoxModifyProductName.Name = "textBoxModifyProductName";
            this.textBoxModifyProductName.Size = new System.Drawing.Size(288, 26);
            this.textBoxModifyProductName.TabIndex = 10;
            // 
            // panelStockRefillRequest
            // 
            this.panelStockRefillRequest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelStockRefillRequest.Controls.Add(this.btnCloseStockRefill);
            this.panelStockRefillRequest.Controls.Add(this.btnSendStockRefill);
            this.panelStockRefillRequest.Controls.Add(this.textBoxStockRefill);
            this.panelStockRefillRequest.Controls.Add(this.label2);
            this.panelStockRefillRequest.Location = new System.Drawing.Point(63, 387);
            this.panelStockRefillRequest.Name = "panelStockRefillRequest";
            this.panelStockRefillRequest.Size = new System.Drawing.Size(547, 37);
            this.panelStockRefillRequest.TabIndex = 21;
            this.panelStockRefillRequest.Visible = false;
            // 
            // btnCloseStockRefill
            // 
            this.btnCloseStockRefill.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCloseStockRefill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnCloseStockRefill.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCloseStockRefill.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnCloseStockRefill.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnCloseStockRefill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseStockRefill.ForeColor = System.Drawing.Color.LightGray;
            this.btnCloseStockRefill.Location = new System.Drawing.Point(3, 5);
            this.btnCloseStockRefill.Margin = new System.Windows.Forms.Padding(4);
            this.btnCloseStockRefill.Name = "btnCloseStockRefill";
            this.btnCloseStockRefill.Size = new System.Drawing.Size(23, 25);
            this.btnCloseStockRefill.TabIndex = 22;
            this.btnCloseStockRefill.Text = "X";
            this.btnCloseStockRefill.UseVisualStyleBackColor = false;
            this.btnCloseStockRefill.Click += new System.EventHandler(this.btnCloseStockRefill_Click);
            // 
            // btnSendStockRefill
            // 
            this.btnSendStockRefill.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSendStockRefill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnSendStockRefill.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSendStockRefill.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnSendStockRefill.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnSendStockRefill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendStockRefill.ForeColor = System.Drawing.Color.LightGray;
            this.btnSendStockRefill.Location = new System.Drawing.Point(421, 1);
            this.btnSendStockRefill.Margin = new System.Windows.Forms.Padding(4);
            this.btnSendStockRefill.Name = "btnSendStockRefill";
            this.btnSendStockRefill.Size = new System.Drawing.Size(118, 32);
            this.btnSendStockRefill.TabIndex = 4;
            this.btnSendStockRefill.Text = "Send Request";
            this.btnSendStockRefill.UseVisualStyleBackColor = false;
            this.btnSendStockRefill.Click += new System.EventHandler(this.btnSendStockRefill_Click);
            // 
            // textBoxStockRefill
            // 
            this.textBoxStockRefill.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxStockRefill.Location = new System.Drawing.Point(175, 6);
            this.textBoxStockRefill.Name = "textBoxStockRefill";
            this.textBoxStockRefill.Size = new System.Drawing.Size(207, 22);
            this.textBoxStockRefill.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(76, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Enter Quantity";
            // 
            // btnViewStockDescription
            // 
            this.btnViewStockDescription.BackColor = System.Drawing.Color.Transparent;
            this.btnViewStockDescription.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnViewStockDescription.BackgroundImage")));
            this.btnViewStockDescription.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnViewStockDescription.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewStockDescription.FlatAppearance.BorderSize = 0;
            this.btnViewStockDescription.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnViewStockDescription.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnViewStockDescription.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewStockDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewStockDescription.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnViewStockDescription.Location = new System.Drawing.Point(651, 44);
            this.btnViewStockDescription.Name = "btnViewStockDescription";
            this.btnViewStockDescription.Size = new System.Drawing.Size(72, 38);
            this.btnViewStockDescription.TabIndex = 20;
            this.btnViewStockDescription.Text = "Description";
            this.btnViewStockDescription.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnViewStockDescription.UseVisualStyleBackColor = false;
            this.btnViewStockDescription.Visible = false;
            this.btnViewStockDescription.Click += new System.EventHandler(this.btnViewStockDescription_Click);
            // 
            // panelStockRequestSelectView
            // 
            this.panelStockRequestSelectView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelStockRequestSelectView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.panelStockRequestSelectView.Controls.Add(this.btnViewOpenRequests);
            this.panelStockRequestSelectView.Controls.Add(this.btnViewPendingRequests);
            this.panelStockRequestSelectView.Controls.Add(this.btnViewClosedRequests);
            this.panelStockRequestSelectView.Location = new System.Drawing.Point(630, 334);
            this.panelStockRequestSelectView.Name = "panelStockRequestSelectView";
            this.panelStockRequestSelectView.Size = new System.Drawing.Size(109, 135);
            this.panelStockRequestSelectView.TabIndex = 10;
            this.panelStockRequestSelectView.Visible = false;
            // 
            // btnViewOpenRequests
            // 
            this.btnViewOpenRequests.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewOpenRequests.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewOpenRequests.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewOpenRequests.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewOpenRequests.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewOpenRequests.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewOpenRequests.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewOpenRequests.Location = new System.Drawing.Point(6, 5);
            this.btnViewOpenRequests.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewOpenRequests.Name = "btnViewOpenRequests";
            this.btnViewOpenRequests.Size = new System.Drawing.Size(98, 42);
            this.btnViewOpenRequests.TabIndex = 1;
            this.btnViewOpenRequests.Text = "View Open";
            this.btnViewOpenRequests.UseVisualStyleBackColor = false;
            this.btnViewOpenRequests.Click += new System.EventHandler(this.btnViewOpenRequests_Click);
            // 
            // btnViewPendingRequests
            // 
            this.btnViewPendingRequests.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewPendingRequests.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewPendingRequests.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewPendingRequests.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewPendingRequests.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewPendingRequests.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewPendingRequests.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewPendingRequests.Location = new System.Drawing.Point(6, 47);
            this.btnViewPendingRequests.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewPendingRequests.Name = "btnViewPendingRequests";
            this.btnViewPendingRequests.Size = new System.Drawing.Size(98, 42);
            this.btnViewPendingRequests.TabIndex = 3;
            this.btnViewPendingRequests.Text = "View Pending";
            this.btnViewPendingRequests.UseVisualStyleBackColor = false;
            this.btnViewPendingRequests.Click += new System.EventHandler(this.btnViewPendingRequests_Click);
            // 
            // btnViewClosedRequests
            // 
            this.btnViewClosedRequests.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewClosedRequests.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewClosedRequests.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewClosedRequests.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewClosedRequests.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewClosedRequests.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewClosedRequests.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewClosedRequests.Location = new System.Drawing.Point(6, 89);
            this.btnViewClosedRequests.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewClosedRequests.Name = "btnViewClosedRequests";
            this.btnViewClosedRequests.Size = new System.Drawing.Size(98, 42);
            this.btnViewClosedRequests.TabIndex = 2;
            this.btnViewClosedRequests.Text = "View Closed";
            this.btnViewClosedRequests.UseVisualStyleBackColor = false;
            this.btnViewClosedRequests.Click += new System.EventHandler(this.btnViewClosedRequests_Click);
            // 
            // panelStockSelectView
            // 
            this.panelStockSelectView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelStockSelectView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.panelStockSelectView.Controls.Add(this.btnViewAllProducts);
            this.panelStockSelectView.Controls.Add(this.btnViewAvailableProducts);
            this.panelStockSelectView.Controls.Add(this.btnViewUnavailableProducts);
            this.panelStockSelectView.Location = new System.Drawing.Point(630, 141);
            this.panelStockSelectView.Name = "panelStockSelectView";
            this.panelStockSelectView.Size = new System.Drawing.Size(109, 135);
            this.panelStockSelectView.TabIndex = 9;
            this.panelStockSelectView.Visible = false;
            // 
            // btnViewAllProducts
            // 
            this.btnViewAllProducts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewAllProducts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewAllProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewAllProducts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewAllProducts.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewAllProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewAllProducts.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewAllProducts.Location = new System.Drawing.Point(6, 5);
            this.btnViewAllProducts.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewAllProducts.Name = "btnViewAllProducts";
            this.btnViewAllProducts.Size = new System.Drawing.Size(98, 42);
            this.btnViewAllProducts.TabIndex = 1;
            this.btnViewAllProducts.Text = "View All";
            this.btnViewAllProducts.UseVisualStyleBackColor = false;
            this.btnViewAllProducts.Click += new System.EventHandler(this.btnViewAllProducts_Click);
            // 
            // btnViewAvailableProducts
            // 
            this.btnViewAvailableProducts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewAvailableProducts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewAvailableProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewAvailableProducts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewAvailableProducts.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewAvailableProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewAvailableProducts.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewAvailableProducts.Location = new System.Drawing.Point(6, 47);
            this.btnViewAvailableProducts.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewAvailableProducts.Name = "btnViewAvailableProducts";
            this.btnViewAvailableProducts.Size = new System.Drawing.Size(98, 42);
            this.btnViewAvailableProducts.TabIndex = 3;
            this.btnViewAvailableProducts.Text = "View Available";
            this.btnViewAvailableProducts.UseVisualStyleBackColor = false;
            this.btnViewAvailableProducts.Click += new System.EventHandler(this.btnViewAvailableProducts_Click);
            // 
            // btnViewUnavailableProducts
            // 
            this.btnViewUnavailableProducts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewUnavailableProducts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewUnavailableProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewUnavailableProducts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewUnavailableProducts.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewUnavailableProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewUnavailableProducts.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewUnavailableProducts.Location = new System.Drawing.Point(6, 89);
            this.btnViewUnavailableProducts.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewUnavailableProducts.Name = "btnViewUnavailableProducts";
            this.btnViewUnavailableProducts.Size = new System.Drawing.Size(98, 42);
            this.btnViewUnavailableProducts.TabIndex = 2;
            this.btnViewUnavailableProducts.Text = "View Unavailable";
            this.btnViewUnavailableProducts.UseVisualStyleBackColor = false;
            this.btnViewUnavailableProducts.Click += new System.EventHandler(this.btnViewUnavailableProducts_Click);
            // 
            // btnViewStockRequests
            // 
            this.btnViewStockRequests.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewStockRequests.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewStockRequests.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewStockRequests.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewStockRequests.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewStockRequests.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewStockRequests.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewStockRequests.Location = new System.Drawing.Point(624, 283);
            this.btnViewStockRequests.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewStockRequests.Name = "btnViewStockRequests";
            this.btnViewStockRequests.Size = new System.Drawing.Size(119, 51);
            this.btnViewStockRequests.TabIndex = 8;
            this.btnViewStockRequests.Text = "View Stock Requests";
            this.btnViewStockRequests.UseVisualStyleBackColor = false;
            this.btnViewStockRequests.Click += new System.EventHandler(this.btnViewStockRequests_Click);
            // 
            // btnViewStock
            // 
            this.btnViewStock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewStock.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewStock.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewStock.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewStock.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewStock.Location = new System.Drawing.Point(624, 90);
            this.btnViewStock.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewStock.Name = "btnViewStock";
            this.btnViewStock.Size = new System.Drawing.Size(119, 51);
            this.btnViewStock.TabIndex = 7;
            this.btnViewStock.Text = "View Stock";
            this.btnViewStock.UseVisualStyleBackColor = false;
            this.btnViewStock.Click += new System.EventHandler(this.btnViewStock_Click);
            // 
            // btnStockModRequest
            // 
            this.btnStockModRequest.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStockModRequest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnStockModRequest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStockModRequest.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnStockModRequest.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnStockModRequest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockModRequest.ForeColor = System.Drawing.Color.LightGray;
            this.btnStockModRequest.Location = new System.Drawing.Point(189, 451);
            this.btnStockModRequest.Margin = new System.Windows.Forms.Padding(4);
            this.btnStockModRequest.Name = "btnStockModRequest";
            this.btnStockModRequest.Size = new System.Drawing.Size(118, 51);
            this.btnStockModRequest.TabIndex = 6;
            this.btnStockModRequest.Text = "Request Stock Modification";
            this.btnStockModRequest.UseVisualStyleBackColor = false;
            this.btnStockModRequest.Visible = false;
            this.btnStockModRequest.Click += new System.EventHandler(this.btnStockModRequest_Click);
            // 
            // btnStockRefillRequest
            // 
            this.btnStockRefillRequest.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStockRefillRequest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnStockRefillRequest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStockRefillRequest.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnStockRefillRequest.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnStockRefillRequest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockRefillRequest.ForeColor = System.Drawing.Color.LightGray;
            this.btnStockRefillRequest.Location = new System.Drawing.Point(315, 451);
            this.btnStockRefillRequest.Margin = new System.Windows.Forms.Padding(4);
            this.btnStockRefillRequest.Name = "btnStockRefillRequest";
            this.btnStockRefillRequest.Size = new System.Drawing.Size(118, 51);
            this.btnStockRefillRequest.TabIndex = 5;
            this.btnStockRefillRequest.Text = "Request Stock Refill";
            this.btnStockRefillRequest.UseVisualStyleBackColor = false;
            this.btnStockRefillRequest.Visible = false;
            this.btnStockRefillRequest.Click += new System.EventHandler(this.btnStockRefillRequest_Click);
            // 
            // btnAddProductRequest
            // 
            this.btnAddProductRequest.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddProductRequest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnAddProductRequest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddProductRequest.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnAddProductRequest.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnAddProductRequest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddProductRequest.ForeColor = System.Drawing.Color.LightGray;
            this.btnAddProductRequest.Location = new System.Drawing.Point(63, 451);
            this.btnAddProductRequest.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddProductRequest.Name = "btnAddProductRequest";
            this.btnAddProductRequest.Size = new System.Drawing.Size(118, 51);
            this.btnAddProductRequest.TabIndex = 4;
            this.btnAddProductRequest.Text = "Request New Stock Item";
            this.btnAddProductRequest.UseVisualStyleBackColor = false;
            this.btnAddProductRequest.Visible = false;
            this.btnAddProductRequest.Click += new System.EventHandler(this.btnAddProductRequest_Click);
            // 
            // listBoxProducts
            // 
            this.listBoxProducts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listBoxProducts.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listBoxProducts.FormattingEnabled = true;
            this.listBoxProducts.ItemHeight = 16;
            this.listBoxProducts.Location = new System.Drawing.Point(63, 90);
            this.listBoxProducts.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxProducts.Name = "listBoxProducts";
            this.listBoxProducts.Size = new System.Drawing.Size(547, 324);
            this.listBoxProducts.TabIndex = 0;
            // 
            // panelAddProduct
            // 
            this.panelAddProduct.Controls.Add(this.label13);
            this.panelAddProduct.Controls.Add(this.textBoxAddProductType);
            this.panelAddProduct.Controls.Add(this.btnAddProductClearFields);
            this.panelAddProduct.Controls.Add(this.btnBackFromAddProduct);
            this.panelAddProduct.Controls.Add(this.btnAddProductToDb);
            this.panelAddProduct.Controls.Add(this.label6);
            this.panelAddProduct.Controls.Add(this.textBoxAddProductValue);
            this.panelAddProduct.Controls.Add(this.label5);
            this.panelAddProduct.Controls.Add(this.textBoxAddProductDescription);
            this.panelAddProduct.Controls.Add(this.label4);
            this.panelAddProduct.Controls.Add(this.textBoxAddProductQuantity);
            this.panelAddProduct.Controls.Add(this.label1);
            this.panelAddProduct.Controls.Add(this.textBoxAddProductName);
            this.panelAddProduct.Location = new System.Drawing.Point(963, 471);
            this.panelAddProduct.Name = "panelAddProduct";
            this.panelAddProduct.Size = new System.Drawing.Size(694, 514);
            this.panelAddProduct.TabIndex = 16;
            this.panelAddProduct.Visible = false;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(334, 121);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 16);
            this.label13.TabIndex = 29;
            this.label13.Text = "Type";
            // 
            // textBoxAddProductType
            // 
            this.textBoxAddProductType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAddProductType.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxAddProductType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAddProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddProductType.Location = new System.Drawing.Point(255, 140);
            this.textBoxAddProductType.Name = "textBoxAddProductType";
            this.textBoxAddProductType.Size = new System.Drawing.Size(207, 26);
            this.textBoxAddProductType.TabIndex = 28;
            // 
            // btnAddProductClearFields
            // 
            this.btnAddProductClearFields.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddProductClearFields.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnAddProductClearFields.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddProductClearFields.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnAddProductClearFields.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnAddProductClearFields.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddProductClearFields.ForeColor = System.Drawing.Color.LightGray;
            this.btnAddProductClearFields.Location = new System.Drawing.Point(599, 26);
            this.btnAddProductClearFields.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddProductClearFields.Name = "btnAddProductClearFields";
            this.btnAddProductClearFields.Size = new System.Drawing.Size(80, 31);
            this.btnAddProductClearFields.TabIndex = 21;
            this.btnAddProductClearFields.Text = "Clear";
            this.btnAddProductClearFields.UseVisualStyleBackColor = false;
            this.btnAddProductClearFields.Click += new System.EventHandler(this.btnAddProductClearFields_Click);
            // 
            // btnBackFromAddProduct
            // 
            this.btnBackFromAddProduct.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBackFromAddProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnBackFromAddProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBackFromAddProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnBackFromAddProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnBackFromAddProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackFromAddProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackFromAddProduct.ForeColor = System.Drawing.Color.LightGray;
            this.btnBackFromAddProduct.Location = new System.Drawing.Point(17, 26);
            this.btnBackFromAddProduct.Margin = new System.Windows.Forms.Padding(4);
            this.btnBackFromAddProduct.Name = "btnBackFromAddProduct";
            this.btnBackFromAddProduct.Size = new System.Drawing.Size(80, 31);
            this.btnBackFromAddProduct.TabIndex = 9;
            this.btnBackFromAddProduct.Text = "<<";
            this.btnBackFromAddProduct.UseVisualStyleBackColor = false;
            this.btnBackFromAddProduct.Click += new System.EventHandler(this.btnBackFromAddProduct_Click);
            // 
            // btnAddProductToDb
            // 
            this.btnAddProductToDb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddProductToDb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnAddProductToDb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddProductToDb.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnAddProductToDb.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnAddProductToDb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddProductToDb.ForeColor = System.Drawing.Color.LightGray;
            this.btnAddProductToDb.Location = new System.Drawing.Point(307, 445);
            this.btnAddProductToDb.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddProductToDb.Name = "btnAddProductToDb";
            this.btnAddProductToDb.Size = new System.Drawing.Size(109, 51);
            this.btnAddProductToDb.TabIndex = 8;
            this.btnAddProductToDb.Text = "Add Product";
            this.btnAddProductToDb.UseVisualStyleBackColor = false;
            this.btnAddProductToDb.Click += new System.EventHandler(this.btnAddProductToDb_Click);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(303, 375);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "Purchase Value";
            // 
            // textBoxAddProductValue
            // 
            this.textBoxAddProductValue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAddProductValue.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxAddProductValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAddProductValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddProductValue.Location = new System.Drawing.Point(255, 398);
            this.textBoxAddProductValue.Name = "textBoxAddProductValue";
            this.textBoxAddProductValue.Size = new System.Drawing.Size(207, 26);
            this.textBoxAddProductValue.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(320, 249);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Description";
            // 
            // textBoxAddProductDescription
            // 
            this.textBoxAddProductDescription.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAddProductDescription.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxAddProductDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAddProductDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddProductDescription.Location = new System.Drawing.Point(183, 272);
            this.textBoxAddProductDescription.Multiline = true;
            this.textBoxAddProductDescription.Name = "textBoxAddProductDescription";
            this.textBoxAddProductDescription.Size = new System.Drawing.Size(355, 91);
            this.textBoxAddProductDescription.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(329, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Quantity";
            // 
            // textBoxAddProductQuantity
            // 
            this.textBoxAddProductQuantity.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAddProductQuantity.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxAddProductQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAddProductQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddProductQuantity.Location = new System.Drawing.Point(255, 209);
            this.textBoxAddProductQuantity.Name = "textBoxAddProductQuantity";
            this.textBoxAddProductQuantity.Size = new System.Drawing.Size(207, 26);
            this.textBoxAddProductQuantity.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(334, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            // 
            // textBoxAddProductName
            // 
            this.textBoxAddProductName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAddProductName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxAddProductName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAddProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddProductName.Location = new System.Drawing.Point(255, 88);
            this.textBoxAddProductName.Name = "textBoxAddProductName";
            this.textBoxAddProductName.Size = new System.Drawing.Size(207, 26);
            this.textBoxAddProductName.TabIndex = 0;
            // 
            // panelStockStatistics
            // 
            this.panelStockStatistics.Controls.Add(this.panel6);
            this.panelStockStatistics.Controls.Add(this.panelMonthlyStock);
            this.panelStockStatistics.Controls.Add(this.panelInactiveItemsInstock);
            this.panelStockStatistics.Controls.Add(this.panelActiveItemsInStock);
            this.panelStockStatistics.Controls.Add(this.panelInactiveItemsPerc);
            this.panelStockStatistics.Controls.Add(this.panel2);
            this.panelStockStatistics.Controls.Add(this.panelActiveItemsPerc);
            this.panelStockStatistics.Controls.Add(this.panelTotalStock);
            this.panelStockStatistics.Controls.Add(this.btnSortStockQty);
            this.panelStockStatistics.Controls.Add(this.btnViewInactiveStockStats);
            this.panelStockStatistics.Controls.Add(this.btnViewActiveStockStats);
            this.panelStockStatistics.Controls.Add(this.listBoxStockStats);
            this.panelStockStatistics.Location = new System.Drawing.Point(746, 438);
            this.panelStockStatistics.Name = "panelStockStatistics";
            this.panelStockStatistics.Size = new System.Drawing.Size(173, 104);
            this.panelStockStatistics.TabIndex = 18;
            this.panelStockStatistics.Visible = false;
            // 
            // panel6
            // 
            this.panel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panel6.Controls.Add(this.textBoxSortType);
            this.panel6.Controls.Add(this.btnSortStockByType);
            this.panel6.Location = new System.Drawing.Point(-274, 83);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(270, 33);
            this.panel6.TabIndex = 18;
            // 
            // textBoxSortType
            // 
            this.textBoxSortType.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxSortType.Location = new System.Drawing.Point(3, 6);
            this.textBoxSortType.Name = "textBoxSortType";
            this.textBoxSortType.Size = new System.Drawing.Size(108, 22);
            this.textBoxSortType.TabIndex = 27;
            // 
            // btnSortStockByType
            // 
            this.btnSortStockByType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSortStockByType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnSortStockByType.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSortStockByType.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnSortStockByType.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnSortStockByType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSortStockByType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSortStockByType.ForeColor = System.Drawing.Color.LightGray;
            this.btnSortStockByType.Location = new System.Drawing.Point(187, 2);
            this.btnSortStockByType.Margin = new System.Windows.Forms.Padding(4);
            this.btnSortStockByType.Name = "btnSortStockByType";
            this.btnSortStockByType.Size = new System.Drawing.Size(79, 29);
            this.btnSortStockByType.TabIndex = 20;
            this.btnSortStockByType.Text = "Sort Type";
            this.btnSortStockByType.UseVisualStyleBackColor = false;
            this.btnSortStockByType.Click += new System.EventHandler(this.btnSortStockByType_Click);
            // 
            // panelMonthlyStock
            // 
            this.panelMonthlyStock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelMonthlyStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panelMonthlyStock.Controls.Add(this.listBoxRefillLog);
            this.panelMonthlyStock.Controls.Add(this.labelTotalRefillRequests);
            this.panelMonthlyStock.Controls.Add(this.btnRefreshMonthly);
            this.panelMonthlyStock.Controls.Add(this.label15);
            this.panelMonthlyStock.Controls.Add(this.btnCloseMonthly);
            this.panelMonthlyStock.Location = new System.Drawing.Point(247, 47);
            this.panelMonthlyStock.Name = "panelMonthlyStock";
            this.panelMonthlyStock.Size = new System.Drawing.Size(199, 240);
            this.panelMonthlyStock.TabIndex = 18;
            this.panelMonthlyStock.Visible = false;
            // 
            // listBoxRefillLog
            // 
            this.listBoxRefillLog.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listBoxRefillLog.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listBoxRefillLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxRefillLog.FormattingEnabled = true;
            this.listBoxRefillLog.ItemHeight = 15;
            this.listBoxRefillLog.Location = new System.Drawing.Point(4, 67);
            this.listBoxRefillLog.Name = "listBoxRefillLog";
            this.listBoxRefillLog.Size = new System.Drawing.Size(191, 154);
            this.listBoxRefillLog.TabIndex = 20;
            // 
            // labelTotalRefillRequests
            // 
            this.labelTotalRefillRequests.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelTotalRefillRequests.AutoSize = true;
            this.labelTotalRefillRequests.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalRefillRequests.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelTotalRefillRequests.Location = new System.Drawing.Point(19, 51);
            this.labelTotalRefillRequests.Name = "labelTotalRefillRequests";
            this.labelTotalRefillRequests.Size = new System.Drawing.Size(120, 13);
            this.labelTotalRefillRequests.TabIndex = 20;
            this.labelTotalRefillRequests.Text = "             Refresh to view";
            this.labelTotalRefillRequests.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRefreshMonthly
            // 
            this.btnRefreshMonthly.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRefreshMonthly.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnRefreshMonthly.BackgroundImage = global::Media_Bazaar.Properties.Resources.refresh;
            this.btnRefreshMonthly.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefreshMonthly.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefreshMonthly.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnRefreshMonthly.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnRefreshMonthly.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefreshMonthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefreshMonthly.ForeColor = System.Drawing.Color.LightGray;
            this.btnRefreshMonthly.Location = new System.Drawing.Point(163, 10);
            this.btnRefreshMonthly.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefreshMonthly.Name = "btnRefreshMonthly";
            this.btnRefreshMonthly.Size = new System.Drawing.Size(32, 24);
            this.btnRefreshMonthly.TabIndex = 18;
            this.btnRefreshMonthly.UseVisualStyleBackColor = false;
            this.btnRefreshMonthly.Click += new System.EventHandler(this.btnRefreshMonthly_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label15.Location = new System.Drawing.Point(42, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Monthly Stock Statistics";
            // 
            // btnCloseMonthly
            // 
            this.btnCloseMonthly.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCloseMonthly.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnCloseMonthly.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCloseMonthly.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCloseMonthly.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnCloseMonthly.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnCloseMonthly.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseMonthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseMonthly.ForeColor = System.Drawing.Color.LightGray;
            this.btnCloseMonthly.Location = new System.Drawing.Point(4, 10);
            this.btnCloseMonthly.Margin = new System.Windows.Forms.Padding(4);
            this.btnCloseMonthly.Name = "btnCloseMonthly";
            this.btnCloseMonthly.Size = new System.Drawing.Size(35, 24);
            this.btnCloseMonthly.TabIndex = 17;
            this.btnCloseMonthly.Text = "X";
            this.btnCloseMonthly.UseVisualStyleBackColor = false;
            this.btnCloseMonthly.Click += new System.EventHandler(this.btnCloseMonthly_Click);
            // 
            // panelInactiveItemsInstock
            // 
            this.panelInactiveItemsInstock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelInactiveItemsInstock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panelInactiveItemsInstock.Controls.Add(this.labelTotalInactiveInStock);
            this.panelInactiveItemsInstock.Controls.Add(this.btnTotalnactiveStockRefresh);
            this.panelInactiveItemsInstock.Location = new System.Drawing.Point(-274, -179);
            this.panelInactiveItemsInstock.Name = "panelInactiveItemsInstock";
            this.panelInactiveItemsInstock.Size = new System.Drawing.Size(244, 33);
            this.panelInactiveItemsInstock.TabIndex = 19;
            // 
            // labelTotalInactiveInStock
            // 
            this.labelTotalInactiveInStock.AutoSize = true;
            this.labelTotalInactiveInStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalInactiveInStock.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelTotalInactiveInStock.Location = new System.Drawing.Point(3, 10);
            this.labelTotalInactiveInStock.Name = "labelTotalInactiveInStock";
            this.labelTotalInactiveInStock.Size = new System.Drawing.Size(90, 15);
            this.labelTotalInactiveInStock.TabIndex = 17;
            this.labelTotalInactiveInStock.Text = "Refresh to view";
            // 
            // btnTotalnactiveStockRefresh
            // 
            this.btnTotalnactiveStockRefresh.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnTotalnactiveStockRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnTotalnactiveStockRefresh.BackgroundImage = global::Media_Bazaar.Properties.Resources.refresh;
            this.btnTotalnactiveStockRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTotalnactiveStockRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTotalnactiveStockRefresh.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnTotalnactiveStockRefresh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnTotalnactiveStockRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTotalnactiveStockRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotalnactiveStockRefresh.ForeColor = System.Drawing.Color.LightGray;
            this.btnTotalnactiveStockRefresh.Location = new System.Drawing.Point(213, 6);
            this.btnTotalnactiveStockRefresh.Margin = new System.Windows.Forms.Padding(4);
            this.btnTotalnactiveStockRefresh.Name = "btnTotalnactiveStockRefresh";
            this.btnTotalnactiveStockRefresh.Size = new System.Drawing.Size(27, 22);
            this.btnTotalnactiveStockRefresh.TabIndex = 16;
            this.btnTotalnactiveStockRefresh.UseVisualStyleBackColor = false;
            this.btnTotalnactiveStockRefresh.Click += new System.EventHandler(this.btnTotalnactiveStockRefresh_Click);
            // 
            // panelActiveItemsInStock
            // 
            this.panelActiveItemsInStock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelActiveItemsInStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panelActiveItemsInStock.Controls.Add(this.labelActiveItemsInStock);
            this.panelActiveItemsInStock.Controls.Add(this.btnTotalActiveStockRefresh);
            this.panelActiveItemsInStock.Location = new System.Drawing.Point(-274, -140);
            this.panelActiveItemsInStock.Name = "panelActiveItemsInStock";
            this.panelActiveItemsInStock.Size = new System.Drawing.Size(244, 33);
            this.panelActiveItemsInStock.TabIndex = 18;
            // 
            // labelActiveItemsInStock
            // 
            this.labelActiveItemsInStock.AutoSize = true;
            this.labelActiveItemsInStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActiveItemsInStock.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelActiveItemsInStock.Location = new System.Drawing.Point(3, 10);
            this.labelActiveItemsInStock.Name = "labelActiveItemsInStock";
            this.labelActiveItemsInStock.Size = new System.Drawing.Size(90, 15);
            this.labelActiveItemsInStock.TabIndex = 17;
            this.labelActiveItemsInStock.Text = "Refresh to view";
            // 
            // btnTotalActiveStockRefresh
            // 
            this.btnTotalActiveStockRefresh.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnTotalActiveStockRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnTotalActiveStockRefresh.BackgroundImage = global::Media_Bazaar.Properties.Resources.refresh;
            this.btnTotalActiveStockRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTotalActiveStockRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTotalActiveStockRefresh.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnTotalActiveStockRefresh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnTotalActiveStockRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTotalActiveStockRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotalActiveStockRefresh.ForeColor = System.Drawing.Color.LightGray;
            this.btnTotalActiveStockRefresh.Location = new System.Drawing.Point(213, 6);
            this.btnTotalActiveStockRefresh.Margin = new System.Windows.Forms.Padding(4);
            this.btnTotalActiveStockRefresh.Name = "btnTotalActiveStockRefresh";
            this.btnTotalActiveStockRefresh.Size = new System.Drawing.Size(27, 22);
            this.btnTotalActiveStockRefresh.TabIndex = 16;
            this.btnTotalActiveStockRefresh.UseVisualStyleBackColor = false;
            this.btnTotalActiveStockRefresh.Click += new System.EventHandler(this.btnTotalActiveStockRefresh_Click);
            // 
            // panelInactiveItemsPerc
            // 
            this.panelInactiveItemsPerc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelInactiveItemsPerc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panelInactiveItemsPerc.Controls.Add(this.labelTotalInactiveStockPerc);
            this.panelInactiveItemsPerc.Controls.Add(this.btnRefreshStockInactivePerc);
            this.panelInactiveItemsPerc.Location = new System.Drawing.Point(-274, -18);
            this.panelInactiveItemsPerc.Name = "panelInactiveItemsPerc";
            this.panelInactiveItemsPerc.Size = new System.Drawing.Size(181, 33);
            this.panelInactiveItemsPerc.TabIndex = 19;
            // 
            // labelTotalInactiveStockPerc
            // 
            this.labelTotalInactiveStockPerc.AutoSize = true;
            this.labelTotalInactiveStockPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalInactiveStockPerc.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelTotalInactiveStockPerc.Location = new System.Drawing.Point(3, 9);
            this.labelTotalInactiveStockPerc.Name = "labelTotalInactiveStockPerc";
            this.labelTotalInactiveStockPerc.Size = new System.Drawing.Size(90, 15);
            this.labelTotalInactiveStockPerc.TabIndex = 18;
            this.labelTotalInactiveStockPerc.Text = "Refresh to view";
            // 
            // btnRefreshStockInactivePerc
            // 
            this.btnRefreshStockInactivePerc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRefreshStockInactivePerc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnRefreshStockInactivePerc.BackgroundImage = global::Media_Bazaar.Properties.Resources.refresh;
            this.btnRefreshStockInactivePerc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefreshStockInactivePerc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefreshStockInactivePerc.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnRefreshStockInactivePerc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnRefreshStockInactivePerc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefreshStockInactivePerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefreshStockInactivePerc.ForeColor = System.Drawing.Color.LightGray;
            this.btnRefreshStockInactivePerc.Location = new System.Drawing.Point(148, 6);
            this.btnRefreshStockInactivePerc.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefreshStockInactivePerc.Name = "btnRefreshStockInactivePerc";
            this.btnRefreshStockInactivePerc.Size = new System.Drawing.Size(27, 23);
            this.btnRefreshStockInactivePerc.TabIndex = 17;
            this.btnRefreshStockInactivePerc.UseVisualStyleBackColor = false;
            this.btnRefreshStockInactivePerc.Click += new System.EventHandler(this.btnRefreshStockInactivePerc_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panel2.Controls.Add(this.textBoxPreviewStockPrice);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.textBoxPreviewStockQty);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.textBoxPreviewStockType);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.textBoxPreviewStockName);
            this.panel2.Controls.Add(this.labelStockName);
            this.panel2.Controls.Add(this.btnShowMonthly);
            this.panel2.Controls.Add(this.btnPreviewSelectedStock);
            this.panel2.Location = new System.Drawing.Point(19, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(199, 240);
            this.panel2.TabIndex = 17;
            // 
            // textBoxPreviewStockPrice
            // 
            this.textBoxPreviewStockPrice.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxPreviewStockPrice.Location = new System.Drawing.Point(15, 199);
            this.textBoxPreviewStockPrice.Name = "textBoxPreviewStockPrice";
            this.textBoxPreviewStockPrice.Size = new System.Drawing.Size(171, 22);
            this.textBoxPreviewStockPrice.TabIndex = 26;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label18.Location = new System.Drawing.Point(60, 181);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 15);
            this.label18.TabIndex = 25;
            this.label18.Text = "Stock Price";
            // 
            // textBoxPreviewStockQty
            // 
            this.textBoxPreviewStockQty.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxPreviewStockQty.Location = new System.Drawing.Point(15, 156);
            this.textBoxPreviewStockQty.Name = "textBoxPreviewStockQty";
            this.textBoxPreviewStockQty.Size = new System.Drawing.Size(171, 22);
            this.textBoxPreviewStockQty.TabIndex = 24;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label17.Location = new System.Drawing.Point(60, 138);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 15);
            this.label17.TabIndex = 23;
            this.label17.Text = "Stock Quantity";
            // 
            // textBoxPreviewStockType
            // 
            this.textBoxPreviewStockType.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxPreviewStockType.Location = new System.Drawing.Point(14, 112);
            this.textBoxPreviewStockType.Name = "textBoxPreviewStockType";
            this.textBoxPreviewStockType.Size = new System.Drawing.Size(171, 22);
            this.textBoxPreviewStockType.TabIndex = 22;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label16.Location = new System.Drawing.Point(59, 94);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 15);
            this.label16.TabIndex = 21;
            this.label16.Text = "Stock Type";
            // 
            // textBoxPreviewStockName
            // 
            this.textBoxPreviewStockName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBoxPreviewStockName.Location = new System.Drawing.Point(15, 69);
            this.textBoxPreviewStockName.Name = "textBoxPreviewStockName";
            this.textBoxPreviewStockName.Size = new System.Drawing.Size(171, 22);
            this.textBoxPreviewStockName.TabIndex = 20;
            // 
            // labelStockName
            // 
            this.labelStockName.AutoSize = true;
            this.labelStockName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStockName.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelStockName.Location = new System.Drawing.Point(60, 51);
            this.labelStockName.Name = "labelStockName";
            this.labelStockName.Size = new System.Drawing.Size(74, 15);
            this.labelStockName.TabIndex = 18;
            this.labelStockName.Text = "Stock Name";
            // 
            // btnShowMonthly
            // 
            this.btnShowMonthly.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnShowMonthly.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnShowMonthly.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnShowMonthly.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnShowMonthly.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnShowMonthly.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnShowMonthly.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShowMonthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowMonthly.ForeColor = System.Drawing.Color.LightGray;
            this.btnShowMonthly.Location = new System.Drawing.Point(160, 10);
            this.btnShowMonthly.Margin = new System.Windows.Forms.Padding(4);
            this.btnShowMonthly.Name = "btnShowMonthly";
            this.btnShowMonthly.Size = new System.Drawing.Size(35, 24);
            this.btnShowMonthly.TabIndex = 17;
            this.btnShowMonthly.Text = ">>";
            this.btnShowMonthly.UseVisualStyleBackColor = false;
            this.btnShowMonthly.Click += new System.EventHandler(this.btnShowMonthly_Click);
            // 
            // btnPreviewSelectedStock
            // 
            this.btnPreviewSelectedStock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPreviewSelectedStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnPreviewSelectedStock.BackgroundImage = global::Media_Bazaar.Properties.Resources.rounded_rectangle_desc;
            this.btnPreviewSelectedStock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPreviewSelectedStock.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPreviewSelectedStock.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnPreviewSelectedStock.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnPreviewSelectedStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreviewSelectedStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreviewSelectedStock.ForeColor = System.Drawing.Color.LightGray;
            this.btnPreviewSelectedStock.Location = new System.Drawing.Point(47, 4);
            this.btnPreviewSelectedStock.Margin = new System.Windows.Forms.Padding(4);
            this.btnPreviewSelectedStock.Name = "btnPreviewSelectedStock";
            this.btnPreviewSelectedStock.Size = new System.Drawing.Size(101, 34);
            this.btnPreviewSelectedStock.TabIndex = 16;
            this.btnPreviewSelectedStock.Text = "Preview Selected";
            this.btnPreviewSelectedStock.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPreviewSelectedStock.UseVisualStyleBackColor = false;
            this.btnPreviewSelectedStock.Click += new System.EventHandler(this.btnPreviewSelectedStock_Click);
            // 
            // panelActiveItemsPerc
            // 
            this.panelActiveItemsPerc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelActiveItemsPerc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panelActiveItemsPerc.Controls.Add(this.labelTotalActiveStockPerc);
            this.panelActiveItemsPerc.Controls.Add(this.btnRefreshStockActivePerc);
            this.panelActiveItemsPerc.Location = new System.Drawing.Point(-274, -61);
            this.panelActiveItemsPerc.Name = "panelActiveItemsPerc";
            this.panelActiveItemsPerc.Size = new System.Drawing.Size(181, 33);
            this.panelActiveItemsPerc.TabIndex = 15;
            // 
            // labelTotalActiveStockPerc
            // 
            this.labelTotalActiveStockPerc.AutoSize = true;
            this.labelTotalActiveStockPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalActiveStockPerc.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelTotalActiveStockPerc.Location = new System.Drawing.Point(3, 9);
            this.labelTotalActiveStockPerc.Name = "labelTotalActiveStockPerc";
            this.labelTotalActiveStockPerc.Size = new System.Drawing.Size(90, 15);
            this.labelTotalActiveStockPerc.TabIndex = 18;
            this.labelTotalActiveStockPerc.Text = "Refresh to view";
            // 
            // btnRefreshStockActivePerc
            // 
            this.btnRefreshStockActivePerc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRefreshStockActivePerc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnRefreshStockActivePerc.BackgroundImage = global::Media_Bazaar.Properties.Resources.refresh;
            this.btnRefreshStockActivePerc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefreshStockActivePerc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefreshStockActivePerc.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnRefreshStockActivePerc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnRefreshStockActivePerc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefreshStockActivePerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefreshStockActivePerc.ForeColor = System.Drawing.Color.LightGray;
            this.btnRefreshStockActivePerc.Location = new System.Drawing.Point(148, 6);
            this.btnRefreshStockActivePerc.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefreshStockActivePerc.Name = "btnRefreshStockActivePerc";
            this.btnRefreshStockActivePerc.Size = new System.Drawing.Size(27, 23);
            this.btnRefreshStockActivePerc.TabIndex = 17;
            this.btnRefreshStockActivePerc.UseVisualStyleBackColor = false;
            this.btnRefreshStockActivePerc.Click += new System.EventHandler(this.btnRefreshStockActivePerc_Click);
            // 
            // panelTotalStock
            // 
            this.panelTotalStock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelTotalStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.panelTotalStock.Controls.Add(this.labelTotalStock);
            this.panelTotalStock.Controls.Add(this.btnRefreshTotalStock);
            this.panelTotalStock.Location = new System.Drawing.Point(-274, -101);
            this.panelTotalStock.Name = "panelTotalStock";
            this.panelTotalStock.Size = new System.Drawing.Size(211, 33);
            this.panelTotalStock.TabIndex = 14;
            // 
            // labelTotalStock
            // 
            this.labelTotalStock.AutoSize = true;
            this.labelTotalStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalStock.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelTotalStock.Location = new System.Drawing.Point(3, 10);
            this.labelTotalStock.Name = "labelTotalStock";
            this.labelTotalStock.Size = new System.Drawing.Size(90, 15);
            this.labelTotalStock.TabIndex = 17;
            this.labelTotalStock.Text = "Refresh to view";
            // 
            // btnRefreshTotalStock
            // 
            this.btnRefreshTotalStock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRefreshTotalStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnRefreshTotalStock.BackgroundImage = global::Media_Bazaar.Properties.Resources.refresh;
            this.btnRefreshTotalStock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefreshTotalStock.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefreshTotalStock.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnRefreshTotalStock.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnRefreshTotalStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefreshTotalStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefreshTotalStock.ForeColor = System.Drawing.Color.LightGray;
            this.btnRefreshTotalStock.Location = new System.Drawing.Point(179, 6);
            this.btnRefreshTotalStock.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefreshTotalStock.Name = "btnRefreshTotalStock";
            this.btnRefreshTotalStock.Size = new System.Drawing.Size(27, 22);
            this.btnRefreshTotalStock.TabIndex = 16;
            this.btnRefreshTotalStock.UseVisualStyleBackColor = false;
            this.btnRefreshTotalStock.Click += new System.EventHandler(this.btnRefreshTotalStock_Click);
            // 
            // btnSortStockQty
            // 
            this.btnSortStockQty.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSortStockQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnSortStockQty.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSortStockQty.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnSortStockQty.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnSortStockQty.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSortStockQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSortStockQty.ForeColor = System.Drawing.Color.LightGray;
            this.btnSortStockQty.Location = new System.Drawing.Point(-191, 51);
            this.btnSortStockQty.Margin = new System.Windows.Forms.Padding(4);
            this.btnSortStockQty.Name = "btnSortStockQty";
            this.btnSortStockQty.Size = new System.Drawing.Size(96, 29);
            this.btnSortStockQty.TabIndex = 12;
            this.btnSortStockQty.Text = "Sort Quantity";
            this.btnSortStockQty.UseVisualStyleBackColor = false;
            this.btnSortStockQty.Click += new System.EventHandler(this.btnSortStockQty_Click);
            // 
            // btnViewInactiveStockStats
            // 
            this.btnViewInactiveStockStats.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewInactiveStockStats.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewInactiveStockStats.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewInactiveStockStats.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewInactiveStockStats.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewInactiveStockStats.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewInactiveStockStats.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewInactiveStockStats.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewInactiveStockStats.Location = new System.Drawing.Point(-145, 256);
            this.btnViewInactiveStockStats.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewInactiveStockStats.Name = "btnViewInactiveStockStats";
            this.btnViewInactiveStockStats.Size = new System.Drawing.Size(96, 31);
            this.btnViewInactiveStockStats.TabIndex = 11;
            this.btnViewInactiveStockStats.Text = "Inactive";
            this.btnViewInactiveStockStats.UseVisualStyleBackColor = false;
            this.btnViewInactiveStockStats.Click += new System.EventHandler(this.btnViewInactiveStockStats_Click);
            // 
            // btnViewActiveStockStats
            // 
            this.btnViewActiveStockStats.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewActiveStockStats.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewActiveStockStats.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewActiveStockStats.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.btnViewActiveStockStats.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.btnViewActiveStockStats.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewActiveStockStats.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewActiveStockStats.ForeColor = System.Drawing.Color.LightGray;
            this.btnViewActiveStockStats.Location = new System.Drawing.Point(-249, 256);
            this.btnViewActiveStockStats.Margin = new System.Windows.Forms.Padding(4);
            this.btnViewActiveStockStats.Name = "btnViewActiveStockStats";
            this.btnViewActiveStockStats.Size = new System.Drawing.Size(96, 31);
            this.btnViewActiveStockStats.TabIndex = 10;
            this.btnViewActiveStockStats.Text = "Active";
            this.btnViewActiveStockStats.UseVisualStyleBackColor = false;
            this.btnViewActiveStockStats.Click += new System.EventHandler(this.btnViewActiveStockStats_Click);
            // 
            // listBoxStockStats
            // 
            this.listBoxStockStats.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.listBoxStockStats.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listBoxStockStats.FormattingEnabled = true;
            this.listBoxStockStats.ItemHeight = 16;
            this.listBoxStockStats.Location = new System.Drawing.Point(-274, 116);
            this.listBoxStockStats.Name = "listBoxStockStats";
            this.listBoxStockStats.Size = new System.Drawing.Size(270, 116);
            this.listBoxStockStats.TabIndex = 0;
            // 
            // PanelAllEmployeeStatistics
            // 
            this.PanelAllEmployeeStatistics.Controls.Add(this.LblAbsentPercentageAllEmployees);
            this.PanelAllEmployeeStatistics.Controls.Add(this.label26);
            this.PanelAllEmployeeStatistics.Controls.Add(this.label27);
            this.PanelAllEmployeeStatistics.Controls.Add(this.label28);
            this.PanelAllEmployeeStatistics.Controls.Add(this.label29);
            this.PanelAllEmployeeStatistics.Controls.Add(this.label30);
            this.PanelAllEmployeeStatistics.Controls.Add(this.label31);
            this.PanelAllEmployeeStatistics.Controls.Add(this.BtnStatisticsAllEmployees);
            this.PanelAllEmployeeStatistics.Controls.Add(this.LblTotalShiftsAllEmployees);
            this.PanelAllEmployeeStatistics.Controls.Add(this.label37);
            this.PanelAllEmployeeStatistics.Controls.Add(this.LblSickAllEmployees);
            this.PanelAllEmployeeStatistics.Controls.Add(this.LblAbsentAllEmployees);
            this.PanelAllEmployeeStatistics.Controls.Add(this.LblPresentAllEmployees);
            this.PanelAllEmployeeStatistics.Controls.Add(this.LblSickPercentageAllEmployees);
            this.PanelAllEmployeeStatistics.Controls.Add(this.label42);
            this.PanelAllEmployeeStatistics.Controls.Add(this.LblPresentPercentageAllEmployees);
            this.PanelAllEmployeeStatistics.Location = new System.Drawing.Point(685, 333);
            this.PanelAllEmployeeStatistics.Name = "PanelAllEmployeeStatistics";
            this.PanelAllEmployeeStatistics.Size = new System.Drawing.Size(264, 91);
            this.PanelAllEmployeeStatistics.TabIndex = 19;
            this.PanelAllEmployeeStatistics.Visible = false;
            // 
            // LblAbsentPercentageAllEmployees
            // 
            this.LblAbsentPercentageAllEmployees.AutoSize = true;
            this.LblAbsentPercentageAllEmployees.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAbsentPercentageAllEmployees.Location = new System.Drawing.Point(529, 147);
            this.LblAbsentPercentageAllEmployees.Name = "LblAbsentPercentageAllEmployees";
            this.LblAbsentPercentageAllEmployees.Size = new System.Drawing.Size(0, 20);
            this.LblAbsentPercentageAllEmployees.TabIndex = 99;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(607, 192);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 20);
            this.label26.TabIndex = 97;
            this.label26.Text = "Sick:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(443, 192);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 20);
            this.label27.TabIndex = 96;
            this.label27.Text = "Absent:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(266, 190);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(68, 20);
            this.label28.TabIndex = 95;
            this.label28.Text = "Present:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(589, 145);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(57, 20);
            this.label29.TabIndex = 94;
            this.label29.Text = "%Sick:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(425, 143);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(78, 20);
            this.label30.TabIndex = 93;
            this.label30.Text = "%Absent:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(248, 143);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(82, 20);
            this.label31.TabIndex = 92;
            this.label31.Text = "%Present:";
            // 
            // BtnStatisticsAllEmployees
            // 
            this.BtnStatisticsAllEmployees.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnStatisticsAllEmployees.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnStatisticsAllEmployees.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnStatisticsAllEmployees.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnStatisticsAllEmployees.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnStatisticsAllEmployees.ForeColor = System.Drawing.Color.LightGray;
            this.BtnStatisticsAllEmployees.Location = new System.Drawing.Point(81, 141);
            this.BtnStatisticsAllEmployees.Margin = new System.Windows.Forms.Padding(4);
            this.BtnStatisticsAllEmployees.Name = "BtnStatisticsAllEmployees";
            this.BtnStatisticsAllEmployees.Size = new System.Drawing.Size(109, 50);
            this.BtnStatisticsAllEmployees.TabIndex = 91;
            this.BtnStatisticsAllEmployees.Text = "View Statistics";
            this.BtnStatisticsAllEmployees.UseVisualStyleBackColor = false;
            this.BtnStatisticsAllEmployees.Click += new System.EventHandler(this.BtnStatisticsAllEmployees_Click);
            // 
            // LblTotalShiftsAllEmployees
            // 
            this.LblTotalShiftsAllEmployees.AutoSize = true;
            this.LblTotalShiftsAllEmployees.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalShiftsAllEmployees.Location = new System.Drawing.Point(201, 77);
            this.LblTotalShiftsAllEmployees.Name = "LblTotalShiftsAllEmployees";
            this.LblTotalShiftsAllEmployees.Size = new System.Drawing.Size(0, 20);
            this.LblTotalShiftsAllEmployees.TabIndex = 86;
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(76, 77);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(119, 28);
            this.label37.TabIndex = 78;
            this.label37.Text = "Total Shifts:";
            // 
            // LblSickAllEmployees
            // 
            this.LblSickAllEmployees.AutoSize = true;
            this.LblSickAllEmployees.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSickAllEmployees.Location = new System.Drawing.Point(669, 192);
            this.LblSickAllEmployees.Name = "LblSickAllEmployees";
            this.LblSickAllEmployees.Size = new System.Drawing.Size(0, 20);
            this.LblSickAllEmployees.TabIndex = 85;
            // 
            // LblAbsentAllEmployees
            // 
            this.LblAbsentAllEmployees.AutoSize = true;
            this.LblAbsentAllEmployees.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAbsentAllEmployees.Location = new System.Drawing.Point(529, 192);
            this.LblAbsentAllEmployees.Name = "LblAbsentAllEmployees";
            this.LblAbsentAllEmployees.Size = new System.Drawing.Size(0, 20);
            this.LblAbsentAllEmployees.TabIndex = 84;
            // 
            // LblPresentAllEmployees
            // 
            this.LblPresentAllEmployees.AutoSize = true;
            this.LblPresentAllEmployees.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPresentAllEmployees.Location = new System.Drawing.Point(354, 192);
            this.LblPresentAllEmployees.Name = "LblPresentAllEmployees";
            this.LblPresentAllEmployees.Size = new System.Drawing.Size(0, 20);
            this.LblPresentAllEmployees.TabIndex = 83;
            // 
            // LblSickPercentageAllEmployees
            // 
            this.LblSickPercentageAllEmployees.AutoSize = true;
            this.LblSickPercentageAllEmployees.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSickPercentageAllEmployees.Location = new System.Drawing.Point(669, 143);
            this.LblSickPercentageAllEmployees.Name = "LblSickPercentageAllEmployees";
            this.LblSickPercentageAllEmployees.Size = new System.Drawing.Size(0, 20);
            this.LblSickPercentageAllEmployees.TabIndex = 81;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(512, 143);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(0, 20);
            this.label42.TabIndex = 80;
            // 
            // LblPresentPercentageAllEmployees
            // 
            this.LblPresentPercentageAllEmployees.AutoSize = true;
            this.LblPresentPercentageAllEmployees.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPresentPercentageAllEmployees.Location = new System.Drawing.Point(357, 143);
            this.LblPresentPercentageAllEmployees.Name = "LblPresentPercentageAllEmployees";
            this.LblPresentPercentageAllEmployees.Size = new System.Drawing.Size(0, 20);
            this.LblPresentPercentageAllEmployees.TabIndex = 79;
            // 
            // PanelSpecificEmployeeStatistics
            // 
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.label23);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.label24);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.label25);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.label20);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.label21);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.label22);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.BtnViewSpecificEmployeeStatistics);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.label9);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.label14);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.LblLatestShiftDate);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.LblFirstShiftDate);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.LblTotalShifts);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.label19);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.LblSick);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.LblAbsent);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.LblPresent);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.textBox1);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.LblSickPercentage);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.LblAbsentPercentage);
            this.PanelSpecificEmployeeStatistics.Controls.Add(this.LblPresentPercentage);
            this.PanelSpecificEmployeeStatistics.Location = new System.Drawing.Point(885, 242);
            this.PanelSpecificEmployeeStatistics.Name = "PanelSpecificEmployeeStatistics";
            this.PanelSpecificEmployeeStatistics.Size = new System.Drawing.Size(64, 46);
            this.PanelSpecificEmployeeStatistics.TabIndex = 20;
            this.PanelSpecificEmployeeStatistics.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(540, 205);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(43, 20);
            this.label23.TabIndex = 77;
            this.label23.Text = "Sick:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(359, 205);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 20);
            this.label24.TabIndex = 76;
            this.label24.Text = "Absent:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(184, 203);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(68, 20);
            this.label25.TabIndex = 75;
            this.label25.Text = "Present:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(540, 129);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 20);
            this.label20.TabIndex = 74;
            this.label20.Text = "%Sick:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(359, 127);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 20);
            this.label21.TabIndex = 73;
            this.label21.Text = "%Absent:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(180, 127);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 20);
            this.label22.TabIndex = 72;
            this.label22.Text = "%Present:";
            // 
            // BtnViewSpecificEmployeeStatistics
            // 
            this.BtnViewSpecificEmployeeStatistics.AutoSize = true;
            this.BtnViewSpecificEmployeeStatistics.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnViewSpecificEmployeeStatistics.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnViewSpecificEmployeeStatistics.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnViewSpecificEmployeeStatistics.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnViewSpecificEmployeeStatistics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnViewSpecificEmployeeStatistics.ForeColor = System.Drawing.Color.LightGray;
            this.BtnViewSpecificEmployeeStatistics.Location = new System.Drawing.Point(41, 116);
            this.BtnViewSpecificEmployeeStatistics.Margin = new System.Windows.Forms.Padding(4);
            this.BtnViewSpecificEmployeeStatistics.Name = "BtnViewSpecificEmployeeStatistics";
            this.BtnViewSpecificEmployeeStatistics.Size = new System.Drawing.Size(132, 50);
            this.BtnViewSpecificEmployeeStatistics.TabIndex = 71;
            this.BtnViewSpecificEmployeeStatistics.Text = "View Statistics";
            this.BtnViewSpecificEmployeeStatistics.UseVisualStyleBackColor = false;
            this.BtnViewSpecificEmployeeStatistics.Click += new System.EventHandler(this.BtnViewSpecificEmployeeStatistics_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(78, 290);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 20);
            this.label9.TabIndex = 26;
            this.label9.Text = "Latest shift date:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(94, 265);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(114, 20);
            this.label14.TabIndex = 25;
            this.label14.Text = "First shift date:";
            // 
            // LblLatestShiftDate
            // 
            this.LblLatestShiftDate.AutoSize = true;
            this.LblLatestShiftDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLatestShiftDate.Location = new System.Drawing.Point(253, 290);
            this.LblLatestShiftDate.Name = "LblLatestShiftDate";
            this.LblLatestShiftDate.Size = new System.Drawing.Size(0, 20);
            this.LblLatestShiftDate.TabIndex = 24;
            // 
            // LblFirstShiftDate
            // 
            this.LblFirstShiftDate.AutoSize = true;
            this.LblFirstShiftDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFirstShiftDate.Location = new System.Drawing.Point(253, 265);
            this.LblFirstShiftDate.Name = "LblFirstShiftDate";
            this.LblFirstShiftDate.Size = new System.Drawing.Size(0, 20);
            this.LblFirstShiftDate.TabIndex = 23;
            // 
            // LblTotalShifts
            // 
            this.LblTotalShifts.AutoSize = true;
            this.LblTotalShifts.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalShifts.Location = new System.Drawing.Point(184, 62);
            this.LblTotalShifts.Name = "LblTotalShifts";
            this.LblTotalShifts.Size = new System.Drawing.Size(0, 20);
            this.LblTotalShifts.TabIndex = 22;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(59, 62);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(119, 28);
            this.label19.TabIndex = 13;
            this.label19.Text = "Total Shifts:";
            // 
            // LblSick
            // 
            this.LblSick.AutoSize = true;
            this.LblSick.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSick.Location = new System.Drawing.Point(602, 205);
            this.LblSick.Name = "LblSick";
            this.LblSick.Size = new System.Drawing.Size(0, 20);
            this.LblSick.TabIndex = 21;
            // 
            // LblAbsent
            // 
            this.LblAbsent.AutoSize = true;
            this.LblAbsent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAbsent.Location = new System.Drawing.Point(445, 205);
            this.LblAbsent.Name = "LblAbsent";
            this.LblAbsent.Size = new System.Drawing.Size(0, 20);
            this.LblAbsent.TabIndex = 20;
            // 
            // LblPresent
            // 
            this.LblPresent.AutoSize = true;
            this.LblPresent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPresent.Location = new System.Drawing.Point(272, 205);
            this.LblPresent.Name = "LblPresent";
            this.LblPresent.Size = new System.Drawing.Size(0, 20);
            this.LblPresent.TabIndex = 19;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(44, 188);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 18;
            // 
            // LblSickPercentage
            // 
            this.LblSickPercentage.AutoSize = true;
            this.LblSickPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSickPercentage.Location = new System.Drawing.Point(620, 127);
            this.LblSickPercentage.Name = "LblSickPercentage";
            this.LblSickPercentage.Size = new System.Drawing.Size(0, 20);
            this.LblSickPercentage.TabIndex = 16;
            // 
            // LblAbsentPercentage
            // 
            this.LblAbsentPercentage.AutoSize = true;
            this.LblAbsentPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAbsentPercentage.Location = new System.Drawing.Point(463, 127);
            this.LblAbsentPercentage.Name = "LblAbsentPercentage";
            this.LblAbsentPercentage.Size = new System.Drawing.Size(0, 20);
            this.LblAbsentPercentage.TabIndex = 15;
            // 
            // LblPresentPercentage
            // 
            this.LblPresentPercentage.AutoSize = true;
            this.LblPresentPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPresentPercentage.Location = new System.Drawing.Point(289, 127);
            this.LblPresentPercentage.Name = "LblPresentPercentage";
            this.LblPresentPercentage.Size = new System.Drawing.Size(0, 20);
            this.LblPresentPercentage.TabIndex = 14;
            // 
            // PanelChoosSpecificOrAllEmployees
            // 
            this.PanelChoosSpecificOrAllEmployees.Controls.Add(this.BtnAllEmployees);
            this.PanelChoosSpecificOrAllEmployees.Controls.Add(this.BtnSpecificEmployee);
            this.PanelChoosSpecificOrAllEmployees.Location = new System.Drawing.Point(570, 471);
            this.PanelChoosSpecificOrAllEmployees.Name = "PanelChoosSpecificOrAllEmployees";
            this.PanelChoosSpecificOrAllEmployees.Size = new System.Drawing.Size(120, 71);
            this.PanelChoosSpecificOrAllEmployees.TabIndex = 21;
            this.PanelChoosSpecificOrAllEmployees.Visible = false;
            // 
            // BtnAllEmployees
            // 
            this.BtnAllEmployees.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnAllEmployees.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnAllEmployees.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnAllEmployees.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnAllEmployees.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnAllEmployees.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAllEmployees.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAllEmployees.ForeColor = System.Drawing.Color.LightGray;
            this.BtnAllEmployees.Location = new System.Drawing.Point(73, -92);
            this.BtnAllEmployees.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAllEmployees.Name = "BtnAllEmployees";
            this.BtnAllEmployees.Size = new System.Drawing.Size(276, 252);
            this.BtnAllEmployees.TabIndex = 72;
            this.BtnAllEmployees.Text = "All Employees";
            this.BtnAllEmployees.UseVisualStyleBackColor = false;
            this.BtnAllEmployees.Click += new System.EventHandler(this.BtnAllEmployees_Click);
            // 
            // BtnSpecificEmployee
            // 
            this.BtnSpecificEmployee.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnSpecificEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSpecificEmployee.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSpecificEmployee.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.BtnSpecificEmployee.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.BtnSpecificEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSpecificEmployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSpecificEmployee.ForeColor = System.Drawing.Color.LightGray;
            this.BtnSpecificEmployee.Location = new System.Drawing.Point(-238, -92);
            this.BtnSpecificEmployee.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSpecificEmployee.Name = "BtnSpecificEmployee";
            this.BtnSpecificEmployee.Size = new System.Drawing.Size(276, 252);
            this.BtnSpecificEmployee.TabIndex = 71;
            this.BtnSpecificEmployee.Text = "Specific Employee";
            this.BtnSpecificEmployee.UseVisualStyleBackColor = false;
            this.BtnSpecificEmployee.Click += new System.EventHandler(this.BtnSpecificEmployee_Click);
            // 
            // bt_employeesStatistics
            // 
            this.bt_employeesStatistics.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_employeesStatistics.Dock = System.Windows.Forms.DockStyle.Top;
            this.bt_employeesStatistics.FlatAppearance.BorderSize = 0;
            this.bt_employeesStatistics.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(30)))));
            this.bt_employeesStatistics.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(60)))));
            this.bt_employeesStatistics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_employeesStatistics.ForeColor = System.Drawing.Color.Gainsboro;
            this.bt_employeesStatistics.Location = new System.Drawing.Point(0, 220);
            this.bt_employeesStatistics.Name = "bt_employeesStatistics";
            this.bt_employeesStatistics.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.bt_employeesStatistics.Size = new System.Drawing.Size(228, 40);
            this.bt_employeesStatistics.TabIndex = 8;
            this.bt_employeesStatistics.Text = "Employees";
            this.bt_employeesStatistics.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_employeesStatistics.UseVisualStyleBackColor = true;
            this.bt_employeesStatistics.Click += new System.EventHandler(this.bt_employeesStatistics_Click);
            // 
            // pnl_employeesStatistics
            // 
            this.pnl_employeesStatistics.Controls.Add(this.lb_info);
            this.pnl_employeesStatistics.Controls.Add(this.bt_allDepo);
            this.pnl_employeesStatistics.Controls.Add(this.bt_allManager);
            this.pnl_employeesStatistics.Controls.Add(this.bt_allAdministration);
            this.pnl_employeesStatistics.Controls.Add(this.bt_totalInactive);
            this.pnl_employeesStatistics.Controls.Add(this.bt_totalActive);
            this.pnl_employeesStatistics.Controls.Add(this.bt_totalEmployees);
            this.pnl_employeesStatistics.Location = new System.Drawing.Point(277, 44);
            this.pnl_employeesStatistics.Name = "pnl_employeesStatistics";
            this.pnl_employeesStatistics.Size = new System.Drawing.Size(514, 420);
            this.pnl_employeesStatistics.TabIndex = 22;
            this.pnl_employeesStatistics.Visible = false;
            // 
            // lb_info
            // 
            this.lb_info.FormattingEnabled = true;
            this.lb_info.ItemHeight = 16;
            this.lb_info.Location = new System.Drawing.Point(88, 193);
            this.lb_info.Name = "lb_info";
            this.lb_info.Size = new System.Drawing.Size(341, 212);
            this.lb_info.TabIndex = 13;
            // 
            // bt_allDepo
            // 
            this.bt_allDepo.Location = new System.Drawing.Point(268, 136);
            this.bt_allDepo.Name = "bt_allDepo";
            this.bt_allDepo.Size = new System.Drawing.Size(161, 46);
            this.bt_allDepo.TabIndex = 12;
            this.bt_allDepo.Text = "All depo";
            this.bt_allDepo.UseVisualStyleBackColor = true;
            this.bt_allDepo.Click += new System.EventHandler(this.bt_allDepo_Click);
            // 
            // bt_allManager
            // 
            this.bt_allManager.Location = new System.Drawing.Point(268, 80);
            this.bt_allManager.Name = "bt_allManager";
            this.bt_allManager.Size = new System.Drawing.Size(161, 46);
            this.bt_allManager.TabIndex = 11;
            this.bt_allManager.Text = "All manager";
            this.bt_allManager.UseVisualStyleBackColor = true;
            this.bt_allManager.Click += new System.EventHandler(this.bt_allManager_Click);
            // 
            // bt_allAdministration
            // 
            this.bt_allAdministration.Location = new System.Drawing.Point(268, 22);
            this.bt_allAdministration.Name = "bt_allAdministration";
            this.bt_allAdministration.Size = new System.Drawing.Size(161, 46);
            this.bt_allAdministration.TabIndex = 10;
            this.bt_allAdministration.Text = "All administration";
            this.bt_allAdministration.UseVisualStyleBackColor = true;
            this.bt_allAdministration.Click += new System.EventHandler(this.bt_allAdministration_Click);
            // 
            // bt_totalInactive
            // 
            this.bt_totalInactive.Location = new System.Drawing.Point(88, 136);
            this.bt_totalInactive.Name = "bt_totalInactive";
            this.bt_totalInactive.Size = new System.Drawing.Size(161, 46);
            this.bt_totalInactive.TabIndex = 9;
            this.bt_totalInactive.Text = "Total inactive";
            this.bt_totalInactive.UseVisualStyleBackColor = true;
            this.bt_totalInactive.Click += new System.EventHandler(this.bt_totalInactive_Click);
            // 
            // bt_totalActive
            // 
            this.bt_totalActive.Location = new System.Drawing.Point(88, 80);
            this.bt_totalActive.Name = "bt_totalActive";
            this.bt_totalActive.Size = new System.Drawing.Size(161, 46);
            this.bt_totalActive.TabIndex = 8;
            this.bt_totalActive.Text = "Total active";
            this.bt_totalActive.UseVisualStyleBackColor = true;
            this.bt_totalActive.Click += new System.EventHandler(this.bt_totalActive_Click);
            // 
            // bt_totalEmployees
            // 
            this.bt_totalEmployees.Location = new System.Drawing.Point(88, 22);
            this.bt_totalEmployees.Name = "bt_totalEmployees";
            this.bt_totalEmployees.Size = new System.Drawing.Size(161, 46);
            this.bt_totalEmployees.TabIndex = 7;
            this.bt_totalEmployees.Text = "Total Employees";
            this.bt_totalEmployees.UseVisualStyleBackColor = true;
            this.bt_totalEmployees.Click += new System.EventHandler(this.bt_totalEmployees_Click);
            // 
            // manager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.pnl_employeesStatistics);
            this.Controls.Add(this.PanelSpecificEmployeeStatistics);
            this.Controls.Add(this.PanelAllEmployeeStatistics);
            this.Controls.Add(this.PanelChoosSpecificOrAllEmployees);
            this.Controls.Add(this.panelStockStatistics);
            this.Controls.Add(this.panelAddProduct);
            this.Controls.Add(this.panelProductDash);
            this.Controls.Add(this.panelMainContent);
            this.Controls.Add(this.panelSideMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.MinimumSize = new System.Drawing.Size(900, 600);
            this.Name = "manager";
            this.Text = "manager";
            this.panelSideMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelMainContent.ResumeLayout(false);
            this.panelMainContent.PerformLayout();
            this.panelProductDash.ResumeLayout(false);
            this.panelModifyProduct.ResumeLayout(false);
            this.panelModifyProduct.PerformLayout();
            this.panelStockRefillRequest.ResumeLayout(false);
            this.panelStockRefillRequest.PerformLayout();
            this.panelStockRequestSelectView.ResumeLayout(false);
            this.panelStockSelectView.ResumeLayout(false);
            this.panelAddProduct.ResumeLayout(false);
            this.panelAddProduct.PerformLayout();
            this.panelStockStatistics.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panelMonthlyStock.ResumeLayout(false);
            this.panelMonthlyStock.PerformLayout();
            this.panelInactiveItemsInstock.ResumeLayout(false);
            this.panelInactiveItemsInstock.PerformLayout();
            this.panelActiveItemsInStock.ResumeLayout(false);
            this.panelActiveItemsInStock.PerformLayout();
            this.panelInactiveItemsPerc.ResumeLayout(false);
            this.panelInactiveItemsPerc.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelActiveItemsPerc.ResumeLayout(false);
            this.panelActiveItemsPerc.PerformLayout();
            this.panelTotalStock.ResumeLayout(false);
            this.panelTotalStock.PerformLayout();
            this.PanelAllEmployeeStatistics.ResumeLayout(false);
            this.PanelAllEmployeeStatistics.PerformLayout();
            this.PanelSpecificEmployeeStatistics.ResumeLayout(false);
            this.PanelSpecificEmployeeStatistics.PerformLayout();
            this.PanelChoosSpecificOrAllEmployees.ResumeLayout(false);
            this.pnl_employeesStatistics.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Button btnProductReq;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelLogo;
        private System.Windows.Forms.Panel panelMainContent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panelProductDash;
        private System.Windows.Forms.Button btnAddProductRequest;
        private System.Windows.Forms.Button btnViewAvailableProducts;
        private System.Windows.Forms.Button btnViewUnavailableProducts;
        private System.Windows.Forms.Button btnViewAllProducts;
        private System.Windows.Forms.ListBox listBoxProducts;
        private System.Windows.Forms.Panel panelAddProduct;
        private System.Windows.Forms.Button btnAddProductClearFields;
        private System.Windows.Forms.Button btnBackFromAddProduct;
        private System.Windows.Forms.Button btnAddProductToDb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxAddProductValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxAddProductDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxAddProductQuantity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAddProductName;
        private System.Windows.Forms.Panel panelStockRequestSelectView;
        private System.Windows.Forms.Button btnViewOpenRequests;
        private System.Windows.Forms.Button btnViewPendingRequests;
        private System.Windows.Forms.Button btnViewClosedRequests;
        private System.Windows.Forms.Panel panelStockSelectView;
        private System.Windows.Forms.Button btnViewStockRequests;
        private System.Windows.Forms.Button btnViewStock;
        private System.Windows.Forms.Button btnStockModRequest;
        private System.Windows.Forms.Button btnStockRefillRequest;
        private System.Windows.Forms.Button btnViewStockDescription;
        private System.Windows.Forms.Panel panelStockRefillRequest;
        private System.Windows.Forms.Button btnCloseStockRefill;
        private System.Windows.Forms.Button btnSendStockRefill;
        private System.Windows.Forms.TextBox textBoxStockRefill;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelModifyProduct;
        private System.Windows.Forms.RadioButton radioButtonInactiveProduct;
        private System.Windows.Forms.RadioButton radioButtonActiveProduct;
        private System.Windows.Forms.Button btnClearModifyProductFields;
        private System.Windows.Forms.Button btnBackFromModifyProduct;
        private System.Windows.Forms.Button btnModifyProductToDb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxModifyProductValue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxModifyProductDescription;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxModifyProductName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxModifyProductType;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxAddProductType;
        private System.Windows.Forms.Button btnStockStats;
        private System.Windows.Forms.Panel panelStockStatistics;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox textBoxSortType;
        private System.Windows.Forms.Button btnSortStockByType;
        private System.Windows.Forms.Panel panelMonthlyStock;
        private System.Windows.Forms.ListBox listBoxRefillLog;
        private System.Windows.Forms.Label labelTotalRefillRequests;
        private System.Windows.Forms.Button btnRefreshMonthly;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnCloseMonthly;
        private System.Windows.Forms.Panel panelInactiveItemsInstock;
        private System.Windows.Forms.Label labelTotalInactiveInStock;
        private System.Windows.Forms.Button btnTotalnactiveStockRefresh;
        private System.Windows.Forms.Panel panelActiveItemsInStock;
        private System.Windows.Forms.Label labelActiveItemsInStock;
        private System.Windows.Forms.Button btnTotalActiveStockRefresh;
        private System.Windows.Forms.Panel panelInactiveItemsPerc;
        private System.Windows.Forms.Label labelTotalInactiveStockPerc;
        private System.Windows.Forms.Button btnRefreshStockInactivePerc;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxPreviewStockPrice;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxPreviewStockQty;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxPreviewStockType;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxPreviewStockName;
        private System.Windows.Forms.Label labelStockName;
        private System.Windows.Forms.Button btnShowMonthly;
        private System.Windows.Forms.Button btnPreviewSelectedStock;
        private System.Windows.Forms.Panel panelActiveItemsPerc;
        private System.Windows.Forms.Label labelTotalActiveStockPerc;
        private System.Windows.Forms.Button btnRefreshStockActivePerc;
        private System.Windows.Forms.Panel panelTotalStock;
        private System.Windows.Forms.Label labelTotalStock;
        private System.Windows.Forms.Button btnRefreshTotalStock;
        private System.Windows.Forms.Button btnSortStockQty;
        private System.Windows.Forms.Button btnViewInactiveStockStats;
        private System.Windows.Forms.Button btnViewActiveStockStats;
        private System.Windows.Forms.ListBox listBoxStockStats;
        private System.Windows.Forms.Button BtnEmployeeStatistics;
        private System.Windows.Forms.Panel PanelAllEmployeeStatistics;
        private System.Windows.Forms.Label LblAbsentPercentageAllEmployees;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button BtnStatisticsAllEmployees;
        private System.Windows.Forms.Label LblTotalShiftsAllEmployees;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label LblSickAllEmployees;
        private System.Windows.Forms.Label LblAbsentAllEmployees;
        private System.Windows.Forms.Label LblPresentAllEmployees;
        private System.Windows.Forms.Label LblSickPercentageAllEmployees;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label LblPresentPercentageAllEmployees;
        private System.Windows.Forms.Panel PanelSpecificEmployeeStatistics;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button BtnViewSpecificEmployeeStatistics;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label LblLatestShiftDate;
        private System.Windows.Forms.Label LblFirstShiftDate;
        private System.Windows.Forms.Label LblTotalShifts;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label LblSick;
        private System.Windows.Forms.Label LblAbsent;
        private System.Windows.Forms.Label LblPresent;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label LblSickPercentage;
        private System.Windows.Forms.Label LblAbsentPercentage;
        private System.Windows.Forms.Label LblPresentPercentage;
        private System.Windows.Forms.Panel PanelChoosSpecificOrAllEmployees;
        private System.Windows.Forms.Button BtnAllEmployees;
        private System.Windows.Forms.Button BtnSpecificEmployee;
        private System.Windows.Forms.Button bt_employeesStatistics;
        private System.Windows.Forms.Panel pnl_employeesStatistics;
        private System.Windows.Forms.ListBox lb_info;
        private System.Windows.Forms.Button bt_allDepo;
        private System.Windows.Forms.Button bt_allManager;
        private System.Windows.Forms.Button bt_allAdministration;
        private System.Windows.Forms.Button bt_totalInactive;
        private System.Windows.Forms.Button bt_totalActive;
        private System.Windows.Forms.Button bt_totalEmployees;
    }
}